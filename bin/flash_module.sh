#!/usr/bin/bash

# writes usage hints then exits
usage() {
  echo "usage: ${##*/} [-c|--config config_file]"
  echo "       if no config file is specified, default.cfg is used."
  exit $1
}

# deletes the temporary files
cleanup() {
  rm -f ${temp_dir}/${sketch_name}.spiffs.bin >${temp_dir}/read_mac.log ${temp_dir}/write_sketch.log ${temp_dir}/write_spiffs.log
}

# checks the first parameter (numeric) if 0,
# otherwise writes the second parameter (message string) to screen and log and exits
check_rc_and_exit_if_error() {
  if [ ${1} -gt 0 ]
  then
    echo "${2}"
    echo "${2}" >>${log_file}
    cleanup
    exit ${1}
  fi
}

# checks file (first parameter) existence,
# otherwise writes a message containing the second parameter to screen NOT LOG and exits
check_file_existence() {
  if [ ! -f "${1}" ]
  then
    echo "ERROR: ${2} \"${1}\" not found."
    exit 1
  else
    echo "${2}: ${1}"
  fi
}

# get local config from command line
local_config_file=default.cfg
param_index=0
while [ "${1}" != "" ]
do
  case "${1}" in
    -h | --help)
      usage 0
      ;;
    -c | --config)
      shift
      local_config_file="${1}"
      ;;
   -*) # Any other option
      echo "Error: Unknown option: ${1}" >&2
      usage 1
      ;;
    *) # no option
      break
      ;;
  esac
  shift
done

global_config_file="${0%/*}/flash_module_global.${USER}.cfg"

# check global config file's existence
check_file_existence "${global_config_file}" "Global config file"

# check specifed config file's existence
check_file_existence "${local_config_file}" "Local config file"

# read global config from same path where this script is
. "${global_config_file}"

# read specifed config file
. "${local_config_file}"

# if sketch name is missing in local config, assume it's the same as the current directory name
if [ "${sketch_name}" == "" ]
then
  sketch_name=${PWD##*/}
fi

# determine USB port
port=`ls -1 ls -1 /dev/ttyUSB*`

echo "Global Config:"
echo "esptool_path   = ${esptool_path}"
echo "mkspiffs_path  = ${mkspiffs_path}"
echo "mkspiffs_bin   = ${mkspiffs_bin}"
echo "boot_app_bin   = ${boot_app_bin}"
echo "bootloader_bin = ${bootloader_bin}"
echo "baud           = ${baud}"
echo "flash_freq     = ${flash_freq}"
echo
echo "Sketch Config:"
echo "PWD                = ${PWD}"
echo "sketch_name        = ${sketch_name}"
echo "data_dir           = ${data_dir}"
echo "chip               = ${chip}"
echo "board              = ${board}"
echo "partitions_address = ${partitions_address}"
echo "sketch_address     = ${sketch_address}"
echo "boot_app_address   = ${boot_app_address}"
echo "bootloader_address = ${bootloader_address}"
echo "mkspiffs_params    = ${mkspiffs_params}"
echo "spiffs_address     = ${spiffs_address}"
echo "${delimiter}"
echo

sketch_file="${sketch_name}.ino.${board}.bin"
partitions_file="${sketch_name}.ino.partitions.bin"
check_file_existence "${sketch_file}" "Sketch file"
check_file_existence  "${partitions_file}" "Partitions file"

python ${esptool_path}/esptool.py --chip ${chip} --port ${port} --no-stub read_mac >${temp_dir}/read_mac.log
mac_addr="`grep -F 'MAC:' ${temp_dir}/read_mac.log | head -n 1`"
mac_addr="${mac_addr#MAC: }"
echo "MAC address: ${mac_addr}"

echo "= `date "${dateformat}"` ${local_config_file} ${sketch_name} ${mac_addr} ${delimiter}" >>${log_file}
echo >>${log_file}

# check if data dir exists, exit if not
if [ ! -d "${data_dir}" ]
then
  echo "Data dir not found."
  echo "Data dir not found." >>${log_file}
  echo >>${log_file}
  cleanup
  exit 1
fi

# make sure we don't have a SPIFFS image left from last time ...
rm -f ${sketch_name}.spiffs.bin

# create new SPIFFS image
${mkspiffs_path}/mkspiffs -c ${data_dir} ${mkspiffs_params} ${sketch_name}.spiffs.bin
mkspiffs_rc=$?

# check result of mkspiffs, exit if not OK
check_rc_and_exit_if_error ${mkspiffs_rc} "Error creating SPIFFS."
echo "SPIFFS created: ${sketch_name}.spiffs.bin"

echo
# write sketch and SPIFFS.
# the " | tee ..." writes the output to screen and the given file.
python ${esptool_path}/esptool.py \
  --chip ${chip} --port ${port} --baud ${baud} \
  --before default_reset --after hard_reset \
  write_flash -z --flash_mode dio --flash_freq ${flash_freq} --flash_size detect \
  ${boot_app_address} ${boot_app_bin} \
  ${bootloader_address} ${bootloader_bin} \
  ${sketch_address} ${sketch_file} \
  ${partitions_address} ${partitions_file} \
  ${spiffs_address} ${sketch_name}.spiffs.bin \
| tee ${temp_dir}/write_sketch.log
# ${PIPESTATUS[0]} gets the exit status of the first command in a pipe (here: | tee)
write_sketch_rc=${PIPESTATUS[0]}

# add ouput of flash process to log
cat ${temp_dir}/write_sketch.log >>${log_file}
echo >>${log_file}

# check result of write process, exit if not OK
check_rc_and_exit_if_error ${write_sketch_rc} "Error writing sketch and SPIFFS."

cleanup
