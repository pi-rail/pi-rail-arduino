# esptool:

ggf. muss `esptool.py` noch im Pfad eingetragen werden:
`PATH="$PATH:~/.arduino15/packages/esp32/tools/esptool_py/4.6"`  
(die genaue location/Versionsnummer entsprechend anpassen)

### MAC-Adresse auslesen:
`esptool.py --chip esp32 --port /dev/ttyUSB0 --no-stub read_mac`

### Flash-Größe ermitteln:
`esptool.py --chip esp32 --port /dev/ttyUSB0 --no-stub flash_id`

# Arduino-IDE Know-How
* Definitionen aller bekannten Boards:
  ~/.arduino15/packages/esp32/hardware/esp32/1.0.6/boards.txt
* Definitionen aller erlaubten Partitions-Layouts:
  ~/.arduino15/packages/esp32/hardware/esp32/1.0.6/tools/partitions/*.csv
* Verbindung der beiden:  
  Ausschnitt aus boards.txt:
  ```
  esp32.menu.PartitionScheme.default=Default 4MB with spiffs (1.2MB APP/1.5MB SPIFFS)
  esp32.menu.PartitionScheme.default.build.partitions=default
  esp32.menu.PartitionScheme.defaultffat=Default 4MB with ffat (1.2MB APP/1.5MB FATFS)
  esp32.menu.PartitionScheme.defaultffat.build.partitions=default_ffat
  ```
  Die erste Zeile ist jeweils der angezeigte Titel, die zweite der Dateiname der .csv-Datei

# Modul flashen:
* Terminal öffnen
* In den Pfad des sketches wechseln
* Z.B. `../../bin/flash_module.sh -c default.cfg` aufrufen  
  Hierbei wird ein Log aller Aufrufe geschrieben: `flash_module.log`

# Vorbereitungen:

### Einmalig
* Dieses Verzeichnis sollte im PATH eingetragen sein, andernfalls muss man das Skript immer mit vollem Pfad aufrufen (geht zu Not auch).
* Es gibt eine "globale" (d.h. für alle Sketches/Module) config, die muss user-spezifisch dupliziert und angepasst werden (Pfade/Versionsnummern).
  Die liegt hier im Verzeichnis: `flash_module_global.{user}.cfg`  
  Es gibt für jeden User eine, damit man sie einchecken kann, ohne sich gegenseitig immer die Werte zu überschreiben. :-)

### Einmalig pro Sketch
* Es gibt pro Sketch (mindestens) eine sketch-spezifische config, die Daten dafür muss man ermitteln, indem man der Arduino-IDE beim flashen auf die Finger guckt (s.u.).
* Ein mal mit der Arduino-IDE flashen
* Das .ino.partitions.bin aus dem passenden /tmp/arduino_build_* holen und im Verzeichnis des sketches ablegen
* Das .ino.esp32.bin in der Arduino IDE bauen (Menü Sketch/Kompilierte Binärdatei exportieren) 
* Die Daten in die sketch-config (z.B. default.cfg) eintragen, z.B.:
```
    sketch_name=Loko_G_ESP32
    data_dir=data
    chip=esp32
    board=esp32
    partitions_address=0x8000
    sketch_address=0x10000
    boot_app_address=0xe000
    bootloader_address=0x1000
    mkspiffs_params="-p 256 -b 4096 -s 1507328"
    spiffs_address=0x290000
    bootloader_bin=/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/tools/sdk/bin/bootloader_qio_80m.bin
```

Falls man mehrere Versionen des sketches (z.B. wenn es eine Nachfolgeversion der Hardware mit anderen Optionen geben sollte), kann man hier auch mehrere configs ablegen.
Dann muss dort ein anderer `sketch_name` angegeben und die bin-Dateien aus dem `/tmp/arduino_build_*` entsprechend benannt werden.

### Arduino-IDE ausspionieren:
* Am einfachsten Verbose-Mode (Ausführliche Ausgabe) in den Voreinstellungen der Arduino-IDE aktivieren.
  Dann erscheint als eine der ersten Zeilen die Kommandozeile mit esptool.py beim Flashen
* Alternativ während des Flashens `psgrep esptool.py` im Terminal aufrufen  
  psgrep liegt auch hier, d.h. wenn man das vorhin im PATH eingetragen hat, findet er das auch. :-)
* Für mkspiffs klappt das nicht, weil das so schnell geht, dass man den Moment verpasst.  
  Daher:
  * Das executable `mkspiffs` finden, bei mir liegt das unter `~/.arduino15/packages/esp32/tools/mkspiffs/0.2.3`
  * Nach `mkspiffs.ORIG` umbenennen
  * Das bash-Skript `mkspiffs` genau dort ablegen und ausführbar machen: `chmod 755 mkspiffs`
  * Das Skript schreibt die Parameter dann nach `/tmp/mkspiffs.txt` und ruft das original auf, d.h. man kann so auch weiter mit der Arduino-IDE weiterarbeiten.
      
### Beispiel Loko-G-ESP32 Flash Firmware
```
python /home/pru/.arduino15/packages/esp32/tools/esptool_py/3.0.0/esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 921600 --before default_reset --after hard_reset write_flash -z --flash_mode dio --flash_freq 80m --flash_size detect 0xe000 /home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/tools/partitions/boot_app0.bin 0x1000 /home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/tools/sdk/bin/bootloader_qio_80m.bin 0x10000 /tmp/arduino_build_85496/Loko_G_ESP32.ino.bin 0x8000 /tmp/arduino_build_85496/Loko_G_ESP32.ino.partitions.bin 
```
              
### Beispiel Loko-G-ESP32 Flash SPIFFS
```
[SPIFFS] data   : /home/pru/projects/Eisenbahn/gitlab/pi-rail-arduino/sketchbook/Loko_G_ESP32/data
[SPIFFS] start  : 2686976
[SPIFFS] size   : 1472
[SPIFFS] page   : 256
[SPIFFS] block  : 4096
/netCfg.xml
/icon.jpg
/cfg.xml
/ioCfg.xml
[SPIFFS] upload : /tmp/arduino_build_85496/Loko_G_ESP32.spiffs.bin
[SPIFFS] address: 2686976
[SPIFFS] port   : /dev/ttyUSB0
[SPIFFS] speed  : 921600
[SPIFFS] mode   : dio
[SPIFFS] freq   : 80m
```
Output in /tmp/mkspiffs:
```
-c /home/pru/projects/Eisenbahn/gitlab/pi-rail-arduino/sketchbook/Loko_G_ESP32/data -p 256 -b 4096 -s 1507328 /tmp/arduino_build_85496/Loko_G_ESP32.spiffs.bin
```