/*
 * This file is part of PI-Rail-Arduino (https://gitlab.com/pi-rail/pi-rail-arduino).
 * Copyright (c) 2013-2020 PI-Data AG, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *------------
 * FlashSize 442209 Bytes (max. is about 442250) esp8266 v3.02 with 128k SPIFFS,
 * IwIP Variant: v2 lower Memory (no features),
 * SSL Support: Basic SSL ciphers (lower ROM use)
 * Logging disabled in PiOTA and PiHttpServer
 */
#include <FS.h>

#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

#define LOCO_ESP8285 1
#include <XmlPiRail.h>
#include <PiHttpServer.h>
#include <PiOTA.h>

int INFO_LED = 10; // vorne

int LED_ON = HIGH;
int LED_OFF = LOW;
int ledState = LED_ON;
int ledSpeedDefault = 250;
int ledSpeed = ledSpeedDefault;
unsigned long lastToggle = 0;

//----- Constants and variable for WiFi
const char *wifiModuleType = __FILE__;
int wifiStatus = WL_IDLE_STATUS;

//----- Constants and variable for IO pins
#define MAX_IO_PIN 16
enum iopin_usage_t { PIN_UNUSED, PIN_INTERNAL, PIN_LED, PIN_OUTPUT, PIN_PWM,
    PIN_INPUT, PIN_SERVO, PIN_SERIAL, PIN_MOTOR, PIN_MOTORSENSOR,
    PIN_IR_SENDER, PIN_DCC, PIN_SUSI };
iopin_usage_t ioPinUsage[MAX_IO_PIN+1] =
    {PIN_UNUSED,PIN_SERIAL,PIN_UNUSED,PIN_SERIAL,PIN_UNUSED,
     PIN_UNUSED,PIN_INTERNAL,PIN_INTERNAL,PIN_INTERNAL,PIN_UNUSED,
     PIN_UNUSED,PIN_UNUSED,PIN_UNUSED,PIN_UNUSED,PIN_UNUSED,
     PIN_UNUSED,PIN_UNUSED };

//----- Constants for Logging
Data* data = NULL;
unsigned long lastDataMsg = 0;
bool logSerial = true;
bool logUDP = true;

//----- Constants and variable for Motor
const int LOKO_MODE_DIRECT = 0;
const int LOKO_MODE_DCC = 1;
const int LOKO_MODE_MM2 = 2;
int lokoMode = LOKO_MODE_DIRECT;
unsigned long lastMotorAdjustTime = 0;
int sensorPin = A0;

//----- Constants and variables for ID-Reader
bool serialChangedState = false;
int idReaderPos = -1;
char idReaderData[MSG_MAX_LEN + 1];
long idReadMillis = 0;

#define HOSTNAME_SIZE 32
#define ERROR_MSG_SIZE 64
const int UDP_BUF_SIZE = 1500;
char myHostname[HOSTNAME_SIZE + 1];
WiFiUDP Udp;
char udpOut[UDP_BUF_SIZE+1];
bool udpConnected = false;
unsigned long wifiBeginTime = 0;
bool wifiScanning = false;
char apMAC[18];
unsigned long resetMillisStart = 0L;
unsigned long resetAfterMillis = 0L;

const PROGMEM char *sketchFileNameAndCompileDate = __FILE__ " " __DATE__ " " __TIME__;
const char *sketchNameAndCompileDate;

bool initialConfigMode = false;
bool hasWifiCfg = false;
char lastErrorMsg[ERROR_MSG_SIZE + 1];

static PROGMEM const char *PI_SETUP = "Pi-Setup";

XmlPiRail xmlPiRail( wifiModuleType, 1024, UDP_BUF_SIZE );

//----- Variables for web page and firmware update
PiHttpServer piHttpServer;
bool httpServerWasAccessed = false;
PiOTA piOta;

//-------------------------------------------------
void fatalError( const char* msg1, const char* msg2 ) {
  if (logSerial) {
    Serial.print( msg1 );
    Serial.print( msg2 );
    Serial.println( "Stopped - fix config" );
  }
  xmlPiRail.setErrorMode( true );
  xmlPiRail.setNextSyncTime( millis() );
  int count = strlen_P( msg1 );
  if (count > ERROR_MSG_SIZE) {
    count = ERROR_MSG_SIZE;
  }
  memcpy_P( lastErrorMsg, msg1, count );
  int count2 = strlen_P( msg2 );
  if (count2 > (ERROR_MSG_SIZE - count)) {
    count2 = (ERROR_MSG_SIZE - count);
  }
  if (count2 > 0) {
    memcpy_P( lastErrorMsg, msg1, count );
  }
  lastErrorMsg[count] = 0;
}

void debugStr( const char* msg, const char* msg2, const char* msg3, const char* msg4 ) {
  if (logSerial) {
    Serial.print( msg );
    if (msg2 != NULL) {
      Serial.print( msg2 );
    }
    if (msg3 != NULL) {
      Serial.print( msg3 );
    }
    if (msg4 != NULL) {
      Serial.print( msg4 );
    }
    Serial.println();
  }
/*  if (logUDP && udpConnected) {
    Csv *csv = data->addCsv( CSVTYPE_DEBUGDATA, 1 );
    if (csv == NULL) { // buffer is full --> send
      sendUdpMessage( xmlPiRail.writeData( data )); // prints csv data to udpOut
      data->clear();
      csv = data->addCsv( CSVTYPE_DEBUGDATA, 1 );
    }
    csv->append( millis() );
    csv->appendStr( msg.c_str() );
    if (msg2 != emptyString) {
      csv->appendStr( msg2.c_str() );
    }
    if (msg3 != emptyString) {
      csv->appendStr( msg3.c_str() );
    }
    if (msg4 != emptyString) {
      csv->appendStr( msg4.c_str() );
    }
  }*/
}

void debugVal( const char* msg, long val, const char* msg2, long val2 ) {
  if (logSerial) {
    Serial.print( msg );
    Serial.print( val );
    if (msg2 != NULL) {
      Serial.print( ", " );
      Serial.print( msg2 );
      Serial.print( val2 );
    }
    Serial.println();
  }
/*  if (logUDP && udpConnected) {
    Csv *csv = data->addCsv( CSVTYPE_DEBUGDATA, 1 );
    if (csv == NULL) { // buffer is full --> send
      sendUdpMessage( xmlPiRail.writeData( data )); // prints csv data to udpOut
      data->clear();
      csv = data->addCsv( CSVTYPE_DEBUGDATA, 1 );
    }
    csv->append( millis() );
    csv->appendStr( msg.c_str() );
    csv->append( val );
    if (msg2 != NULL) {
      csv->appendStr( msg2.c_str() );
      csv->append( val2 );
    }
  }*/
}

void debugIDVal( const char* msg, const QName* id, const char* msg2, int intVal, char charVal ) {
  if (logSerial) {
    Serial.print( msg );
    if (id == NULL) {
      Serial.print( "NULL" );
    }
    else {
      Namespace* ns = id->getNamespace();
      if (ns != piRailFactory->NS_PIRAILFACTORY) {
        Serial.print( id->getNamespace()->getUri());
        Serial.print( ":" );
      }
      Serial.print( id->getName() );
    }
    if (msg2 != NULL) {
      Serial.print( msg2 );
      Serial.print( intVal );
      Serial.print( "," );
      Serial.print( (char) charVal );
    }
    Serial.println();
  }
/*  if (logUDP && udpConnected) {
    Csv *csv = data->addCsv( CSVTYPE_DEBUGDATA, 1 );
    if (csv == NULL) { // buffer is full --> send
      sendUdpMessage( xmlPiRail.writeData( data )); // prints csv data to udpOut
      data->clear();
      csv = data->addCsv( CSVTYPE_DEBUGDATA, 1 );
    }
    csv->append( millis() );
    csv->appendStr( msg.c_str() );
    if (id == NULL) {
      csv->appendStr( "null" );
      csv->appendStr( "null" );
    }
    else {
      Namespace* ns = id->getNamespace();
      if (ns == piRailFactory->NS_PIRAILFACTORY) {
        csv->appendStr( "" );
      }
      else {
        csv->appendStr( id->getNamespace()->getUri());
      }
      csv->appendStr( id->getName());
    }
    if (msg2 != NULL) {
      csv->appendStr( msg2.c_str() );
      csv->append( intVal );
      csv->appendChar( charVal );
    }
  }*/
}

//-------------------------------------------------
void printHeap( const char* msg1, const char* msg2 ) {
  if (logSerial) {
    Serial.print(F( "ms=" ));
    Serial.print( millis());
    Serial.print( " " );
    Serial.print( msg1 );
    Serial.print( " " );
    Serial.print( msg2 );
    Serial.print(F( ", heap=" ));
    Serial.println( ESP.getFreeHeap());
  }
}

//-------------------------------------------------
const char* getVersion() {
  return __DATE__;
}

//-------------------------------------------------
int getHeapSize() {
  int heap = ESP.getFreeHeap();
  return heap;
}

//-------------------------------------------------
void doReset( long afterMillis ) {
  resetMillisStart = millis();
  resetAfterMillis = afterMillis > 0 ? afterMillis : 250; // have some minimum value
  debugVal( "Reset after ms=", afterMillis );
}

//-------------------------------------------------
void httpServerAccessed() {
  httpServerWasAccessed = true;
}

//-------------------------------------------------
bool toggleLed() {
  unsigned long now = millis();
  if ((now - lastToggle) > ledSpeed) {
    if (ledState == LED_ON) {
      ledState = LED_OFF;
    }
    else {
      ledState = LED_ON;
    }
    digitalWrite( INFO_LED, ledState );
    lastToggle = now;
    return true;
  }
  else {
    return false;
  }
}

//-------------------------------------------------
/*void printWifiStatus() {
  // print the SSID of the network you're attached to:
  debugStr( "SSID: ", WiFi.SSID().c_str() );

  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  debugStr( "IP: ", ip.toString().c_str() );

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  debugVal( " RSSI (dBm) ", rssi );
}*/

//-------------------------------------------------
boolean connectToWifi() {
  WifiCfg* wifiCfg = xmlPiRail.getWifiCfg();
  if (wifiCfg == NULL) {
    hasWifiCfg = false;
    return false;
  }
  const char* ssid = wifiCfg->getSsid();
  debugStr( "ssid=", ssid );
  if ((ssid == NULL) || (strlen(ssid) <= 0)) {
    hasWifiCfg = false;
    return false;
  }
  hasWifiCfg = true;
  const char* pass = wifiCfg->getPass();

  //--- connect to Wifi
  WiFi.setHostname( myHostname );

  // Delay based on MAC to avoid all devices connecting same time
  uint8_t mac[6];
  WiFi.macAddress( mac );
  int waitConn = mac[5] * 4;
  debugVal( "Connecting, delay=", waitConn );
  delay( waitConn );

  // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
  WiFi.begin( ssid, pass );

  ledSpeed = 200;
  unsigned long startMillis = millis();

  int count = 50;
  while (WiFi.status() != WL_CONNECTED && millis() - startMillis < 60 * 1000) {
    if (logSerial) {
      Serial.print( "." );
      count--;
      if (count == 0) {
        Serial.println();
        count = 50;
      }
    }
    delay(200);
    toggleLed();
  }

  bool success = WiFi.status() == WL_CONNECTED;

  if (success) {
    debugStr( "WiFi connect" );
  }
  else {
    debugStr( "WiFi failure" );
    //WiFi.disconnect( true, false );
  }
  digitalWrite(INFO_LED, success ? LED_OFF : LED_ON);

  return success;
}

boolean isValidNameChar( char ch ) {
  return ( ((ch == '-') || (ch == '_'))
           || ((ch >= '0') && (ch <= '9'))
           || ((ch >= 'A') && (ch <= 'Z'))
           || ((ch >= 'a') && (ch <= 'z'))
         );
}

void setUpHostname( const char * deviceId, boolean macPrefix ) {
  memset( myHostname, 0, HOSTNAME_SIZE + 1 );
  int pos = 0;
  int i = 0;
  if (macPrefix) {
    String mac = WiFi.macAddress();
    while (pos < mac.length()) {
      char ch = mac.charAt(pos);
      if (isValidNameChar( mac[pos] )) {
        myHostname[i++] = ch;
      }
      pos++;
    }
    myHostname[i++] = '-';
  }
  if (deviceId != NULL) {
    pos = 0;
    while ((i < HOSTNAME_SIZE - 1) && (deviceId[pos] > 0)) {
      char ch = deviceId[pos];
      if (isValidNameChar( ch )) {
        myHostname[i++] = ch;
      }
      else {
        myHostname[i++] = '-';
      }
      pos++;
    }
  }
}

void setUpConfigWifi() {
  digitalWrite( INFO_LED, HIGH );
  if (logSerial) {
    Serial.print(F( "Setup AP: " ));
    Serial.println( myHostname );
  }

  WiFi.softAP( myHostname );

  IPAddress wifiLocalIp = WiFi.softAPIP();
  if (logSerial) {
    Serial.print(F( "IP=" ));
    Serial.println( wifiLocalIp );
  }
}

bool usePin( uint8_t pin, uint8_t mode, iopin_usage_t usage ) {
  if ((pin < 0) || (pin > MAX_IO_PIN)) {
    return false;
  }
  iopin_usage_t pinUsage = ioPinUsage[pin];
  if (pinUsage == PIN_UNUSED) {
    if (mode > 0) {
      pinMode( pin, mode );
    }
    ioPinUsage[pin] = usage;
    return true;
  }
  else if (pinUsage == usage) {
    // allow use of same pin in multiple itemConn
    return true;
  }
  else {
    debugVal( " Pin already in use: ", pin );
    return false;
  }
}


bool attachOutPin( OutCfg* outCfg, outMode_t outMode ) {
  if (outCfg->attached == OUTMODE_NONE) {
    int pin = outCfg->getPin();
    if (!usePin( pin, OUTPUT, PIN_PWM )) {
      return false;
    }
    outCfg->attached = outMode;
    return true;
  }
  else {
    return (outMode == outCfg->attached);
  }
}

bool attachInPin( InCfg* inCfg, inModeType_t inMode, fireType_t fireType, bool useIterrupt ) {
  return true;
}

bool attachPort( PortCfg* portCfg, portMode_t portMode ) {
  ExtCfg *extCfg = portCfg->getExtCfg();
  if (extCfg == NULL) {
    if (portCfg->attached == PORTMODE_NONE) {
      if (portMode == PORTMODE_MOTORDRIVER) {
        PortPin* motorDirPortPin = portCfg->getPortPinByType( PORTPINTYPE_DIRPIN );
        if (motorDirPortPin == NULL) {
          debugStr( "Motor dir pin missing" );
          return false;
        }
        int motorDirPin = motorDirPortPin->getPin();
        if (!usePin( motorDirPin, OUTPUT, PIN_MOTOR )) {
          return false;
        }
        debugVal( "Init motor #dir=", motorDirPin );

        PortPin* motorPWMPortPin = portCfg->getPortPinByType( PORTPINTYPE_PWM_PIN );
        if (motorPWMPortPin == NULL) {
          debugStr( "Motor PWM pin missing" );
          return false;
        }
        int motorPWMPin = motorPWMPortPin->getPin();
        if (!usePin( motorPWMPin, OUTPUT, PIN_MOTOR )) {
          return false;
        }
        debugVal( "  #PWM=", motorPWMPin );
        analogWriteFreq( 24000 );
        analogWriteRange( 1023 );
        analogWrite( motorPWMPin, 0 );
        portCfg->attached = portMode;
        return true;
      }
      else if ((portMode == PORTMODE_IR_READER) || (portMode == PORTMODE_IDREADER)) {
        // always using normal serial port
        portCfg->attached = portMode;
        return true;
      }
      else {
        debugStr( "Unsupported port mode ", piRailFactory->portMode_names[portMode] );
        return false;
      }
    }
    else {
      return (portCfg->attached == portMode);
    }
  }
  else {
    PortCfg* extPort = xmlPiRail.getCfg()->getPortCfg( extCfg->getPort() );
    if (extPort == NULL) {
      return false;
    }
    else if (extPort->attached == PORTMODE_NONE) {
      portMode_t extPortMode = extCfg->getType();
      if (attachPort( extPort, extPortMode )) {
        portCfg->attached = portMode;
        return true;
      }
      else {
        return false;
      }
    }
    else {
      if (portCfg->attached == PORTMODE_NONE) {
        portCfg->attached = portMode;
        return true;
      }
      else {
        return (portCfg->attached == portMode);
      }
    }
  }
}

//--------------------------
void setup() {
  sketchNameAndCompileDate = strrchr( sketchFileNameAndCompileDate, '/' );
  if (sketchNameAndCompileDate) {
    sketchNameAndCompileDate ++;
  }
  else {
    sketchNameAndCompileDate = sketchFileNameAndCompileDate;
  }

  //-- Initialize console logging
  Serial.begin( 2400 ); // 74880 ); // 2400 );
  logSerial = true;
  Serial.println();
  Serial.println( sketchNameAndCompileDate );
  Serial.println();
  printHeap( "Begin ", "setup" );

  //-- Initialize the INFO_LED pin as an output
  // special case: INFO_LED is later used as front light, so do not call usePin( INFO_LED, OUTPUT, PIN_LED );
  pinMode(INFO_LED, OUTPUT);     // Initialize the INFO_LED pin as an output
  digitalWrite(INFO_LED, HIGH);  // Turn the LED off by making the voltage HIGH
  lastErrorMsg[0] = 0;

  piRailFactory->max_csv_count = 6;
  piRailFactory->max_ev_count = 4;
  piRailFactory->max_fileDef_count = 10;

  data = new Data();
  xmlPiRail.setup( WiFi.macAddress().c_str(), DEVICETYPE_LOCOMOTIVE );
  ESP.wdtFeed();

  // Ensure we have a hostname before connectToWifi()
  Cfg* cfg = xmlPiRail.getCfg();
  const char *deviceName;
  if ((cfg != NULL) && (cfg->getId() != NULL) && (strlen(cfg->getId()->getName()) > 0)) {
    deviceName = cfg->getId()->getName();
    setUpHostname( deviceName, false );
  }
  else {
    deviceName = xmlPiRail.getDeviceID();
    if (deviceName == NULL) {
      deviceName = "INIT";
    }
    setUpHostname( deviceName, true );
  }

  if (!initialConfigMode) {
    initialConfigMode = !connectToWifi();
  }

  if (!initialConfigMode && !xmlPiRail.isErrorMode()) {
    debugVal( "Start listen UDP ", piRailReceivePort );
    // if you get a connection, report back via serial:
    Udp.begin( piRailReceivePort );
    xmlPiRail.begin( udpOut );
  }

  if (initialConfigMode) {
    // discard the hostname and rebuild
    setUpHostname( deviceName, true );
  }

  debugStr( "Hostname=", myHostname );

  if (initialConfigMode) {
    setUpConfigWifi();
  }

  ESP.wdtFeed();
  delay(250);
  ESP.wdtFeed();

  NetCfg* netCfg = xmlPiRail.getNetCfg();
  piHttpServer.init( sketchNameAndCompileDate, myHostname, netCfg );
  piHttpServer.begin();

  //--- Does not work on ESP8285: piOta.begin( myHostname, &ArduinoOTA );
  //    So do initialization inline
  ArduinoOTA.setHostname( myHostname );
  ArduinoOTA.onStart( PiOTA::startOTA );
  ArduinoOTA.onEnd( PiOTA::endOTA );
  ArduinoOTA.onProgress( PiOTA::progressOTA );
  ArduinoOTA.onError( PiOTA::errorOTA );
  ArduinoOTA.begin();

  printHeap( "Finished ", "setup" );
}

//------------------------------------------------------------------
// Hardware access functions
//------------------------------------------------------------------

void prepareMotorSensor( PortCfg* portCfg, char dir ) {
  if (portCfg == NULL) {
    return;
  }
  int pinPWM = portCfg->getPinByType( PORTPINTYPE_PWM_PIN );
  if (pinPWM < 0) {
    return;
  }
  // For ESP8285 there is only one (global sensorPin): int sensorPin = portCfg->getPinByType( PORTPINTYPE_SENSORPIN );
  if (sensorPin <= 0) {
    return;
  }
  digitalWrite( pinPWM, HIGH );
}

int readMotorSensor( PortCfg* portCfg, char dir, int currentSpeed ) {
  if (portCfg == NULL) {
    return 0;
  }
  int pinPWM = portCfg->getPinByType( PORTPINTYPE_PWM_PIN );
  if (pinPWM < 0) {
    return 0;
  }
  // For ESP8285 there is only one (global sensorPin): int sensorPin = portCfg->getPinByType( PORTPINTYPE_SENSORPIN );
  if (sensorPin <= 0) {
    return 0;
  }
  int val = 0;
  int count = 8;
  for (int i = 0; i < count; i++) {
    val += analogRead( sensorPin );
  }
  analogWrite( pinPWM, currentSpeed );

  int sensorValue = val / count;
  return sensorValue;
}

void processMsgState( MsgState* newMsgState ) {
  // not supported
}

bool checkIDReader( SensorAction* sensorAction ) {
  // not supported
  return false;
}

void doMotor( PortCfg* portCfg, char dir, int motorOutput ) {
  if (portCfg == NULL) {
    return;
  }
  ExtCfg* extCfg = portCfg->getExtCfg();
  if (extCfg == NULL) {
    if (portCfg->attached == PORTMODE_MOTORDRIVER) {
      int pinDir = portCfg->getPinByType( PORTPINTYPE_DIRPIN );
      if (pinDir < 0) {
        return;
      }
      int pinPWM = portCfg->getPinByType( PORTPINTYPE_PWM_PIN );
      if (pinPWM < 0) {
        return;
      }

      //--- Set PWM and Dir
      if (dir == DIR_FORWARD) {
        digitalWrite( pinDir, HIGH );
      }
      else {
        digitalWrite( pinDir, LOW );
      }
      analogWrite( pinPWM, motorOutput );
    }
  }
}

void doOutputPin( OutCfg* outCfg, int pinValue, int duration ) {
  if (outCfg == NULL) {
    return;
  }
  int pin = outCfg->getPin();
  if (pin < 0) {
    return;
  }
  outMode_t outMode = outCfg->attached;
  switch (outMode) {
    case OUTMODE_SERVO: {
      // not supported
      break;
    }
    case OUTMODE_PWM: {
      debugVal( "  Set PWM pin=", pin, "val=", pinValue );
      analogWrite( pin, pinValue );
      break;
    }
    case OUTMODE_SWITCH: {
      debugVal( "  Set port=", pin, "val=", pinValue );
      if (pinValue == 0) {
        digitalWrite( pin, LOW );
      }
      else if (pinValue == -1) {
        digitalWrite( pin, HIGH );
      }
      break;
    }
    default: { // PULSE or NONE
      if ((duration <= 0) || (duration > 500)) {
        duration = 500;
      }
      debugVal( "  Pulse pin=",pin,"duration=", duration );
      if (pinValue < 0) {
        digitalWrite( pin, HIGH );
      }
      else {
        debugVal( "doPulse NOT YET IMPLEMENTED for value=", pinValue );
        //analogWrite( swPort, pinValue );
      }
      delay( duration );
      digitalWrite( pin, LOW );
    }
  }
}

void doSendMsg( PortCfg* portCfg, const char* msg ) {
  // not supported - do nothing
}

//------------------------------------------------------------------
// Command Execution (doXXX, processXXX)
//------------------------------------------------------------------

void sendUdpMessage( const char *msg ) {
  if (!udpConnected) {
    return;
  }
  //IPAddress sendIP;
  //WiFi.hostByName(udpSendIP, sendIP);
  //Udp.beginPacketMulticast( WiFi.localIP(), sendIP, piRailSendPort ); // funzt nicht
  //Udp.beginPacketMulticast( sendIP, piRailSendPort, WiFi.localIP() ); // funzt nicht
  //Udp.beginPacket( "192.168.43.50", piRailSendPort ); // funzt
  //Udp.beginPacket( "255.255.255.255", piRailSendPort ); // funzt nicht
  //Udp.beginPacketMulticast(addr, port, WiFi.localIP())
  IPAddress sendIP = WiFi.localIP();
  sendIP[3] = 255;
  Udp.beginPacket( sendIP, piRailSendPort ); // funzt
  Udp.print( msg );
  Udp.endPacket();
  udpOut[0] = 0; // clear udpOut buffer
}

//------------------------------------------------------------------
// Serial Event Handling
//------------------------------------------------------------------
/*
 SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();

    //----- Add char to ID reader input
    if (inChar == '[') { // start flag
      idReaderPos = 0;
      idReadMillis = millis();
    }
    else if (idReaderPos >= 0) {
      if (inChar == ']') {
        if (idReaderPos == 7) {
          // checksum
          int cksum = 0;
          for (int i = 0; i < idReaderPos-1; i++) {
            cksum += (byte) idReaderData[i];
          }
          cksum = cksum & 0x7F;
          if (cksum < 32) {
            cksum += 32; // avoid using control characters
          }
          if ((cksum == '[') || (cksum==']')) {
            cksum++; // avoid using mesage start/end characters
          }
          char ckSumCh = (char) cksum;

          if (ckSumCh == idReaderData[6]) {
            // replace checksum byte with string end
            idReaderData[6] = 0;

            //-- Process speed, dir and command
            int dist_mm = (idReaderData[3] - 32) * 20; //mm
            const QName* posID = piRailFactory->NS_PIRAILFACTORY->getQName( idReaderData, 0, 3 );
            char cmd = idReaderData[4];
            int cmdDist_mm = (idReaderData[5] - 32) * 20;
            if (xmlPiRail.processTrackMsg( xmlPiRail.getIDSensorAction(), posID, dist_mm, cmd, cmdDist_mm, idReadMillis )) {
              serialChangedState = true;
            }
            idReaderPos = -1;
            return; // leave method to allow processing of received message
          }
        }
        idReaderPos = -1;
      }
      else {
        if (idReaderPos < MSG_MAX_LEN) {
          idReaderData[idReaderPos] = inChar;
          idReaderPos++;
        }
        else {
          idReaderPos = -1;
        }
      }
    }
  }
}

//==========================
// Main Loop
//==========================
void loop() {
  if ( resetMillisStart != 0L && (unsigned long)( millis() - resetMillisStart ) >= resetAfterMillis ) {
    debugVal( "Performing reset at ", millis() );
    ESP.restart();
  }

  ArduinoOTA.handle();

  if (initialConfigMode && hasWifiCfg) {
    if (!httpServerWasAccessed && millis() >= 3 * 60 * 1000) {
      // 3 minutes = 1 minute trying to connect to existing WiFi plus 2 minutes configuration window
      debugStr( "Config timeout. Restarting…" );
      ESP.restart();
    }
    return; // return even if it's not yet time to restart
  }

  //checkWiFi();
  if (WiFi.status() == WL_CONNECTED) {
    if (!udpConnected) {
      Udp.begin( piRailReceivePort );
      udpConnected = true;
      debugVal( "Start listen UDP ", piRailReceivePort );
    }
  }

  if (xmlPiRail.isErrorMode()) {
    unsigned long currentTime = millis();
    long nextSyncTime = xmlPiRail.getNextSyncTime();
    if (udpConnected && (nextSyncTime >= 0) && (currentTime >= nextSyncTime)) {
      IPAddress sendIP = WiFi.localIP();
      sendIP[3] = 255;
      Udp.beginPacket( sendIP, piRailSendPort ); // funzt
      const char* deviceID = xmlPiRail.getDeviceID();
      if ((deviceID != NULL) && (strlen(deviceID) > 0)) {
        Udp.print( deviceID );
      }
      else {
        Udp.print( myHostname );
      }
      Udp.print( ":\n" );
      Udp.print( sketchNameAndCompileDate );
      Udp.print( "\nERROR - " );
      Udp.print( lastErrorMsg );
      Udp.endPacket();
      udpOut[0] = 0; // clear udpOut buffer
      xmlPiRail.setNextSyncTime( currentTime + 5000 );
    }
  }
  else {
    bool sendState = false;
    if (udpConnected) {
      int packetSize = Udp.parsePacket();
      if (packetSize > 0) {
        IPAddress remoteIp = Udp.remoteIP();
        sendState = xmlPiRail.processXml( WiFi.localIP(), remoteIp, &Udp );
      }
      //--- Event processing and notification
      else if (Serial.available()) {
        // ESP8266 is missing implementation for calling serialEvent(), so do it manually
        // see https://github.com/esp8266/Arduino/issues/752
        serialEvent();
        if (serialChangedState) {
          sendState = true;
          serialChangedState = false;
        }
      }
    }
    ESP.wdtFeed();

    //--- Motor adjust
    Cfg* cfg = xmlPiRail.getCfg();
    MotorAction* motorAction = (MotorAction*) cfg->firstChild( piRailFactory->ID_MOTOR );
    unsigned long currentTime = millis();
    unsigned long deltaTime = currentTime - lastMotorAdjustTime;
    if ((motorAction != NULL) && (deltaTime > motorAction->getSample())) {
      if (deltaTime > 1000) {
        deltaTime = motorAction->getSample();
      }
      lastMotorAdjustTime = currentTime;

      //- For all motor sensors: set motorPWM to 100% for 1 ms then read motor sensors
      while (motorAction != NULL) {
        motorAction->prepareSensor();
        motorAction = (MotorAction *) motorAction->nextSibling( piRailFactory->ID_MOTOR );
      }
      delay( 1 );
      unsigned long sensorReadMillis = millis();
      motorAction = (MotorAction *) cfg->firstChild( piRailFactory->ID_MOTOR );
      while (motorAction != NULL) {
        motorAction->readSensor();
        motorAction = (MotorAction *) motorAction->nextSibling( piRailFactory->ID_MOTOR );
      }

      //- Finally calculate and apply new speed
      Ln *ln = xmlPiRail.getDeviceState()->getLn();
      motorAction = (MotorAction *) cfg->firstChild( piRailFactory->ID_MOTOR );
      while (motorAction != NULL) {
        int oldSpeed = motorAction->getState()->getCurI();
        motorAction->doLoop( data, ln, sensorReadMillis, deltaTime );
        int newSpeed = motorAction->getState()->getCurI();
        if (oldSpeed != newSpeed) {
          sendState = true;
          xmlPiRail.processState( WiFi.localIP(), WiFi.localIP(), xmlPiRail.getDeviceState() );
        }
        motorAction = (MotorAction *) motorAction->nextSibling( piRailFactory->ID_MOTOR );
      }
    }
    ESP.wdtFeed();

    IoCfg* ioCfg = xmlPiRail.getIoCfg();

    //--- Check timers and execute if time out
    TimerAction* timerAction = (TimerAction*) cfg->firstChild( piRailFactory->ID_TIMER );
    while (timerAction != NULL) {
      if (timerAction->doLoop( cfg, ioCfg )) {
        sendState = true;
      }
      timerAction = (TimerAction*) timerAction->nextSibling( piRailFactory->ID_TIMER );
    }

    //--- Send sync message
    currentTime = millis();
    unsigned long nextSyncTime = xmlPiRail.getNextSyncTime();
    if ((nextSyncTime >= 0) && (currentTime >= nextSyncTime)) {
      sendState = true;
      xmlPiRail.setNextSyncTime( -1 );
    }
    if (sendState) {
      //--- Save current BSSID to apMAC
      String bssidStr = WiFi.BSSIDstr();
      if (bssidStr == NULL) {
        apMAC[0] = 0;
      }
      else {
        for (int i = 0; i < bssidStr.length(); i++) {
          apMAC[i] = bssidStr.charAt(i);
          if (apMAC[i] == ':') {
            apMAC[i] = '-';
          }
        }
        apMAC[bssidStr.length()] = 0;
      }
      // print events and speed changes into udpOut
      xmlPiRail.sendDeviceState( currentTime, WiFi.RSSI(), apMAC );
    }

    //--- Check triggers
    xmlPiRail.processState( WiFi.localIP(), WiFi.localIP(), xmlPiRail.getDeviceState() );
    ESP.wdtFeed();

    //--- Send motor tuning data if buffer is full
    if (data->isFull() || ((data->csvCount() > 0) && ((currentTime - lastDataMsg) > 3000))) {
      sendUdpMessage( xmlPiRail.writeData( data ) ); // prints csv data to udpOut
      data->clear();
    }
  }
}
