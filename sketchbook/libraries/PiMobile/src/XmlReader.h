/*
 * This file is part of PI-Rail-Arduino (https://gitlab.com/pi-rail/pi-rail-arduino).
 * Copyright (c) 2013-2020 PI-Data AG, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PIRAIL_XMLREADER_H
#define PIRAIL_XMLREADER_H

#include "Arduino.h"

static const int INITIAL_STACK_SIZE = 8;
static const int INITIAL_NS_COUNT = 5;
static const int MAX_PREFIX_LENGTH = 5;
static const int MAX_DUMP_CHARS = 1024;
static const int ERROR_MAX_LEN = 120;
static const int ENCODING_MAX_LEN = 20;

static const PROGMEM char* NSURI_XMLSCHEMA = "http://www.w3.org/2001/XMLSchema";
static const PROGMEM char* NSURI_PI_MODELS = "de.pidata.models";

static const PROGMEM char* ENTITY_AMP = "amp";
static const PROGMEM char* ENTITY_APOS = "apos";
static const PROGMEM char* ENTITY_GT = "gt";
static const PROGMEM char* ENTITY_LT = "lt";
static const PROGMEM char* ENTITY_QUOT = "quot";
static const PROGMEM char* ENTITY_AUML = "auml";
static const PROGMEM char* ENTITY_OUML = "ouml";
static const PROGMEM char* ENTITY_UUML = "uuml";
static const PROGMEM char* ENTITY_AUML2 = "Auml";
static const PROGMEM char* ENTITY_OUML2 = "Ouml";
static const PROGMEM char* ENTITY_UUML2 = "Uuml";
static const PROGMEM char* ENTITY_SZLIG = "szlig";
static const PROGMEM char* VALUE_TRUE = "true";
static const PROGMEM char* VALUE_FALSE = "false";
static const PROGMEM char* CHARSET_UTF8 = "UTF-8";

static const PROGMEM char* MSG_ROW = " row=";
static const PROGMEM char* MSG_COL = ",col=";

static const PROGMEM char* TAG_XML = "?xml";
static const PROGMEM char* ATTR_ENCODING = "encoding";
static const PROGMEM char* ATTR_XMLNS = "xmlns";
static const PROGMEM char* ATTR_TYPE = "type";
static const PROGMEM char* ATTR_TARGETNAMESPACE = "targetNamespace";
static const PROGMEM char* UNDEFINED = "undefined";

static const PROGMEM char* ERR_DEFAULT_NS_UNDEFINDED = "Default namespace not defined";
static const PROGMEM char* ERR_UNKNOWN_NS_PREFIX = "Unknown namespace prefix: ";
static const PROGMEM char* ERR_UNKNOWN_NS = "Unknown namespace: ";
static const PROGMEM char* ERR_NUMBER_FORMAT = "NumberformatError";
static const PROGMEM char* ERR_ATTR_NOT_FOUND = "Attribute not found";
static const PROGMEM char* ERR_VALUE_NOT_FOUND = "Value not found";
static const PROGMEM char* ERR_EXPECTED_EQ = "Expected '=' but found ";
static const PROGMEM char* ERR_EXPECTED_DBLQUOTE = "Expected '\"' but found ";
static const PROGMEM char* ERR_HEADER_NOT_XML = "Header must start with '?xml'";
static const PROGMEM char* ERR_EXPECTED_MINUS_D_QUESTION = "Expected '-', 'D' or '?' but found ";
static const PROGMEM char* ERR_EXPECTED_MINUS = "Expected '-' but found ";
static const PROGMEM char* ERR_EXPECTED_GT = "Expected '>' but found ";
static const PROGMEM char* ERR_EXPECTED_SLASH = "Expected '/' but found ";
static const PROGMEM char* ERR_XMLREADER_EOF = "XMLReader: Unexpected end of input.";
static const PROGMEM char* ERR_XMLREADER_ENTITY = "EnitityFormatError";

class Namespace;
class QName;

class XmlReader {
public:
  XmlReader( int textBufSize );

  void initParser( char *xmlBuffer, int xmlBufferSize, Stream* dataStream );

  const QName* nextTag( const QName* parentRelation );

  void parseSimpleValue( const QName *relation, char *value, int valueMaxLen );

  bool hasChildren();

  const char* getStringAttr( const QName* attrName, char* valuePtr, int maxValueLen );
  const char* getQNameAttr( const QName* attrName, const QName** valuePtr );
  const char* getByteAttr( const QName* attrName, byte* valuePtr, char defaultValue );
  const char* getShortAttr( const QName* attrName, short* valuePtr, short defaultValue );
  const char* getIntAttr( const QName* attrName, int* valuePtr, int defaultValue );
  const char* getLongAttr( const QName* attrName, long* valuePtr, long defaultValue );
  const char* getBoolAttr( const QName* attrName, bool * valuePtr );
  const char* getEnumAttr( const QName* attrName, char *valueSet[], int valueCount, int *valuePtr );

private:
  char errorMsg[ERROR_MAX_LEN];
  Stream* dataStream;
  //bool continuous = false;
  //InputStreamReader reader;
  char encoding[ENCODING_MAX_LEN];

  char* srcBuf;  // is the first half of xmlBuffer given in initParser()
  int srcBufSize = 0;
  int srcPos;
  //int srcCount;

  int line;
  int column;
  int depth;
  int parseIndex;

  /** whenever a new tag is found in srcBuf that tag is copied to txtBuf while parsing. e tag always begins at txtBuf[0] */
  int txtBuf_length = 0;
  char* txtBuf = 0;  // is the second half of xmlBuffer given in initParser()

  /** Pointer to the next positiopn to be used inside txtBuf */
  int txtPos = 0;
  int attrStart[16];
  int attrNsPos[16];
  int attrEnd[16];
  int valueStart[16];
  int valueEnd[16];
  int attrCount;

  /** Array of declared namespace prefixes. Attention: first character contains prefix's length */
  char nsPrefixes[INITIAL_NS_COUNT][MAX_PREFIX_LENGTH+1];
  Namespace* namespaces[INITIAL_NS_COUNT];
  char* xsiPrefix;
  //char* xsiType;
  Namespace* targetNamespace;
  int nsCounts[INITIAL_STACK_SIZE];

  char *lastLine();
  bool compareString( const char *compStr, int startPos, int endPos );

  Namespace *findNamespace( int startPos, int endPos );
  Namespace *getDefaultNamespace();

  void parseError( const char *message, char *string, int i, int i1 );
  void parseError( const char *message, char errorChar );

  char read();
  char *readString();

  const QName *createQName( Namespace *defaultNS, int startPos, int nsPos, int endPos );
  const QName *createQName( Namespace *defaultNS, int startPos, int endPos );
  const char * createString( int startPos, int endPos, char *str, int maxLen );
  const char * createByte( int startPos, int endPos, byte *bytePtr );
  const char * createShort( int startPos, int endPos, short *shortPtr );
  const char * createInt( int startPos, int endPos, int *intPtr );
  const char * createLong( int startPos, int endPos, long* longPtr );
  const char * createLongHex( int startPos, int endPos, unsigned long* longPtr );
  const char * createBool( int startPos, int endPos, bool *str );
  const char * createDateTime( int startPos, int endPos, char *str, int maxLen );
  const char * createDecimal( int startPos, int endPos, char *str, int maxLen );
  const char * createBinary( int startPos, int endPos, char *str, int maxLen );
  const char * createDuration( int startPos, int endPos, char *str, int maxLen );

  int findNSPos( int startPos, int endPos );

  int parseQName();
  void parseValue( char closeChar );

  char readWhitespace();

  bool prcessXSI( int attrIndex );

  const char *readHeader( char *encodingPtr, int encodingMaxLen );

  void readDocType();

  void readProcessingInstruction();

  void readComment();

  void processCloseTag( const QName *parentRelation );

  char parseAttributes( char endChar );

  int indexOfAttr( const QName* attrName );

  byte getEntity( int startPos, int endPos );

  Namespace *processNsDecl( int attrIndex );
};


#endif //PIRAIL_XMLREADER_H
