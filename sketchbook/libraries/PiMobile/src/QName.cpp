//
// Created by pru on 21.04.19.
//

#include <string.h>
#include "QName.h"

QName::QName( Namespace *namespacePtr, const char *name ) {
  _namespace = namespacePtr;
  _name = name;
  _hashCode = calcHashCode( name );
}

Namespace *QName::getNamespace() const {
  return _namespace;
}

const char *QName::getName() const {
  return _name;
}

int QName::hashCode() const {
  return _hashCode;
}

bool QName::equals( const char *name ) const {
  return (strcmp( _name, name ) == 0);
}

bool QName::equals( char buffer[], int startIndex, int endIndex ) const {
  int pos = 0;
  while ((_name[pos] > 0) && (_name[pos] == buffer[startIndex+pos])) {
    pos++;
  }
  return ((_name[pos] == 0) && (startIndex+pos == endIndex));
}

