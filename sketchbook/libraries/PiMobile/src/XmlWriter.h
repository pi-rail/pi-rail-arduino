/*
 * This file is part of PI-Rail-Arduino (https://gitlab.com/pi-rail/pi-rail-arduino).
 * Copyright (c) 2013-2020 PI-Data AG, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TEST_ARDUINO_XMLWRITER_H
#define TEST_ARDUINO_XMLWRITER_H

#include "Arduino.h"
#include "Namespace.h"
#include "XmlReader.h"

static const PROGMEM char* HEADER_XML = "<?xml version=\"1.0\" encoding=\"ASCII\"?>";

class XmlWriter {
private:
  int  xmlBufSize = 0;
  char* _xmlBuf;
  int  _bufPos;
  int  _level;
  bool _prettyPrint = false;
  bool _convertGerman = true;

  char _nsPrefixes[INITIAL_NS_COUNT][MAX_PREFIX_LENGTH+1];
  Namespace* _namespaces[INITIAL_NS_COUNT];
  Namespace* _defaultNS;
  int _nsCount;
  int _nsIndex;

  void char2Buf( char ch );
  void int2Buf( int num );
  void long2Buf( long num );
  void hex2Buf( unsigned long num );
  void str2Buf( const char* str );
  void qName2Buf( const QName * qName );

  void appendChar( byte ch );
  void appendString( char* str );
  const char * getPrefix( Namespace* ns );

public:
  XmlWriter( int xmlBufSize );

  void setPrettyPrint( bool prettyPrint );
  void begin( Namespace* defaultNS );
  void addNamespace( Namespace* ns, char * prefix );

  void header();
  void startTag( const QName* tagName );
  void attribute( const QName* name, const QName * value );
  void attribute( const QName* name, char * value );
  void attribute( const QName* name, char value );
  void attribute( const QName* name, int value );
  void attribute( const QName* name, long value );
  void attribute( const QName* name, bool value );
  void endAttributes( bool endTag );
  void endTag( const QName* tagName  );
  const int getBufPos();
  const char * getXml();
};


#endif //TEST_ARDUINO_XMLWRITER_H
