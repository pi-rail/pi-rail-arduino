/*
 * This file is part of PI-Rail-Arduino (https://gitlab.com/pi-rail/pi-rail-arduino).
 * Copyright (c) 2013-2020 PI-Data AG, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PIRAIL_NAMESPACE_H
#define PIRAIL_NAMESPACE_H

#include "Namespace.h"
#include "QName.h"
#include "StringHash.h"

const int NS_MAX_COUNT = 10;

class Namespace {

private:
  static Namespace* nsTable[];
  static int nsCount;

  const char* _uri;
  StringHash* _stringHash;

public:
  Namespace( const char *string );
  static Namespace* getInstance( const char* nsUri );
  static Namespace *getInstance( char *buf, int startPos, int endPos );

  const QName * getQName( char * txtBuf, int startPos, int endPos );
  const QName * getQName( const char * name );

  const char *getUri();
};


#endif //PIRAIL_NAMESPACE_H
