/*
 * This file is part of PI-Rail-Arduino (https://gitlab.com/pi-rail/pi-rail-arduino).
 * Copyright (c) 2013-2020 PI-Data AG, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <cstring>
#include "XmlWriter.h"
#include "QName.h"

extern void fatalError( const char* msg1, const char* msg2 );

XmlWriter::XmlWriter( int xmlBufSize ) {
  this->xmlBufSize = xmlBufSize;
  _xmlBuf = new char[xmlBufSize+1];
  for (int i = 0; i < INITIAL_NS_COUNT; i++) {
    _nsPrefixes[i][0] = 0;
    _nsPrefixes[i][MAX_PREFIX_LENGTH] = 0;
  }
}

void XmlWriter::char2Buf( char ch ) {
  if (_bufPos < (this->xmlBufSize - 2)) {
    _xmlBuf[_bufPos++] = ch;
    _xmlBuf[_bufPos] = 0;
  }
  else {
    fatalError( "XmlBuffer ", "overflow" );
  }
}

void XmlWriter::hex2Buf( unsigned long num ) {
  char2Buf( '#' );
  char2Buf( 'x' );
  unsigned long factor = 1;
  while ((factor*16) <= num) {
    factor = factor * 16;
  }
  while (factor > 0) {
    unsigned long digit = num / factor;
    byte ch;
    if (digit >= 10) {
      ch = 'A' + digit - 10;
    }
    else {
      ch = '0' + digit;
    }
    char2Buf( ch );
    num = num % factor;
    factor = factor / 16;
  }
}

void XmlWriter::int2Buf( int num ) {
  if (num < 0) {
    char2Buf( '-' );
    num = -num;
  }
  int factor = 1;
  while ((num / factor) > 9) {
    factor = factor * 10;
  }
  while (factor > 0) {
    int digit = num / factor;
    char2Buf( '0' + digit );
    num = num % factor;
    factor = factor / 10;
  }
}

void XmlWriter::long2Buf( long num ) {
  if (num < 0) {
    char2Buf( '-' );
    num = -num;
  }
  long factor = 1;
  while ((num / factor) > 9) {
    factor = factor * 10;
  }
  while (factor > 0) {
    long digit = num / factor;
    char2Buf( '0' + digit );
    num = num % factor;
    factor = factor / 10;
  }
}

void XmlWriter::str2Buf( const char * str ) {
  int i = 0;
  while (str[i] != 0) {
    appendChar( str[i++] );
  }
}

void XmlWriter::qName2Buf( const QName * qName ) {
  Namespace * ns = qName->getNamespace();
  if (ns != _defaultNS) {
    const char * prefix = getPrefix(ns);
    if (prefix == NULL) {
      str2Buf( "xxx:" );
    }
    else {
      str2Buf( prefix );
      char2Buf( ':' );
    }
  }
  str2Buf( qName->getName() );
}



void XmlWriter::appendChar( byte aktChar ) {
  if (aktChar < ' ') {
    char2Buf( '&' );
    hex2Buf( aktChar );
    char2Buf( ';' );
  }
  else if (aktChar > 126) {
    char2Buf( '&' );
    if (_convertGerman) {
      switch (aktChar) {
        case (char)0xE4: str2Buf( ENTITY_AUML ); break;
        case (char)0xF6: str2Buf( ENTITY_OUML ); break;
        case (char)0xFC: str2Buf( ENTITY_UUML ); break;
        case (char)0xC4: str2Buf( ENTITY_AUML2 ); break;
        case (char)0xD6: str2Buf( ENTITY_OUML2 ); break;
        case (char)0xDC: str2Buf( ENTITY_UUML2 ); break;
        case (char)0x3B2: str2Buf( ENTITY_SZLIG ); break;
        default: {
          hex2Buf( aktChar );
        }
      }
      char2Buf( ';' );
    }
    else {
      hex2Buf( aktChar );
      char2Buf( ';' );
    }
  }
  else {
    switch (aktChar) {
      case '\"': {
        char2Buf( '&' );
        str2Buf( ENTITY_QUOT );
        char2Buf( ';' );
        break;
      }
      case '\'': {
        char2Buf( '&' );
        str2Buf( ENTITY_APOS );
        char2Buf( ';' );
        break;
      }
      case '<': {
        char2Buf( '&' );
        str2Buf( ENTITY_LT );
        char2Buf( ';' );
        break;
      }
      case '>': {
        char2Buf( '&' );
        str2Buf( ENTITY_GT );
        char2Buf( ';' );
        break;
      }
      case '&': {
        char2Buf( '&' );
        str2Buf( ENTITY_AMP );
        char2Buf( ';' );
        break;
      }
      default: {
        char2Buf( aktChar );
      }
    }
  }
}

void XmlWriter::appendString( char * str ) {
  int i = 0;
  while (str[i] != 0) {
    appendChar( str[i++] );
  }
}

const char * XmlWriter::getPrefix( Namespace * ns ) {
  char* prefix;
  int prefixNum = _nsCount-1;
  while (prefixNum >= 0) {
    if (ns == _namespaces[prefixNum]) {
      return _nsPrefixes[prefixNum];
    }
    prefixNum++;
  }
  return NULL;
}

void XmlWriter::setPrettyPrint( bool prettyPrint ) {
  _prettyPrint = prettyPrint;
}

void XmlWriter::begin( Namespace * defaultNS ) {
  _bufPos = 0;
  _xmlBuf[xmlBufSize-1] = 0;
  _level = 0;
  _defaultNS = defaultNS;
  _nsCount = 0;
  _nsIndex = -1;
}

void XmlWriter::addNamespace( Namespace* ns, char* prefix ) {
  if (_nsCount < NS_MAX_COUNT) {
    strcpy( _nsPrefixes[_nsCount], prefix );
    _namespaces[_nsCount] = ns;
    _nsCount++;
  }
}

void XmlWriter::header() {
  str2Buf( HEADER_XML );
  if (_prettyPrint) {
    char2Buf( '\n' );
  }
}

void XmlWriter::startTag( const QName * tagName ) {
  bool empty = _bufPos == 0;
  if (_prettyPrint) {
    for (int i = 0; i < (_level*2); i++) {
      char2Buf( ' ' );
    }
  }
  char2Buf( '<' );
  qName2Buf( tagName );
  if (empty && _defaultNS != NULL) {
    char2Buf( ' ' );
    str2Buf( ATTR_XMLNS );
    char2Buf(  '=' );
    char2Buf(  '"' );
    str2Buf( _defaultNS->getUri() );
    char2Buf(  '"' );
  }
  while ( _nsIndex < _nsCount - 1) {
    _nsIndex ++;
    char2Buf( ' ' );
    str2Buf( ATTR_XMLNS );
    str2Buf(  ":" );
    str2Buf( _nsPrefixes[_nsIndex] );
    char2Buf(  '=' );
    char2Buf(  '"' );
    str2Buf( _namespaces[_nsIndex]->getUri() );
    char2Buf(  '"' );
  }
  _level++;
}

void XmlWriter::attribute( const QName* name, const QName * value ) {
  if (value != NULL) {
    char2Buf( ' ' );
    qName2Buf( name );
    char2Buf( '=' );
    char2Buf( '"' );
    qName2Buf( value );
    char2Buf( '"' );
  }
}

void XmlWriter::attribute( const QName* name, char * value ) {
  char2Buf( ' ' );
  qName2Buf( name );
  char2Buf( '=' );
  char2Buf( '"' );
  appendString( value );
  char2Buf( '"' );
}

void XmlWriter::attribute( const QName* name, char value ) {
  char2Buf( ' ' );
  qName2Buf( name );
  char2Buf( '=' );
  char2Buf( '"' );
  appendChar( value );
  char2Buf( '"' );
}

void XmlWriter::attribute( const QName* name, int value ) {
  char2Buf( ' ' );
  qName2Buf( name );
  char2Buf( '=' );
  char2Buf( '"' );
  int2Buf( value );
  char2Buf( '"' );
}

void XmlWriter::attribute( const QName* name, long value ) {
  char2Buf( ' ' );
  qName2Buf( name );
  char2Buf( '=' );
  char2Buf( '"' );
  long2Buf( value );
  char2Buf( '"' );
}

void XmlWriter::attribute( const QName* name, bool value ) {
  char2Buf( ' ' );
  qName2Buf( name );
  char2Buf( '=' );
  char2Buf( '"' );
  if (value) {
    str2Buf( VALUE_TRUE );
  }
  else {
    str2Buf( VALUE_FALSE );
  }
  char2Buf( '"' );
}

void XmlWriter::endAttributes( bool endTag ) {
  if (endTag) {
    char2Buf( '/' );
    _level--;
  }
  char2Buf( '>' );
  if (_prettyPrint) {
    char2Buf( '\n' );
  }
}

void XmlWriter::endTag( const QName * tagName  ) {
  _level--;
  if (_prettyPrint) {
    for (int i = 0; i < (_level*2); i++) {
      char2Buf( ' ' );
    }
  }
  char2Buf( '<' );
  char2Buf( '/' );
  qName2Buf( tagName );
  char2Buf( '>' );
  if (_prettyPrint) {
    char2Buf( '\n' );
  }
}

const int XmlWriter::getBufPos() {
  return _bufPos;
}

const char * XmlWriter::getXml() {
  return _xmlBuf;
}

