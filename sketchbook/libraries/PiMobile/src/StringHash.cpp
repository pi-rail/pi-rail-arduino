/*
 * This file is part of PI-Rail-Arduino (https://gitlab.com/pi-rail/pi-rail-arduino).
 * Copyright (c) 2013-2020 PI-Data AG, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <stddef.h>
#include <string.h>
#include "StringHash.h"
#include "Namespace.h"
#include "QName.h"

int calcHashCode( const char value[] ) {
  int result = 0;
  int i = 0;
  while (value[i] != 0) {
    result += value[i];
    i++;
  }
  return result;
}

/**
 * Calculates the hash code for the characters between startIndex (included) and
 * endIndex (excluded) of buffer.
 *
 * @param buffer      the buffer containing the character sequence
 * @param startIndex index of the first character inside buffer
 * @param endIndex   1 + index of the last character inside buffer
 * @return the calculated hash code
 */
int calcHashCode( char buffer[], int startIndex, int endIndex) {
  int result = 0;
  for (int i = startIndex; i < endIndex; i++) {
    result += buffer[i];
  }
  return result;
}

StringHash::StringHash( Namespace* ns ) {
  _namespace = ns;
  loadFactor = 75;
  //table = new QName[initialCapacity];
  threshold = _capacity * loadFactor / 100;
  for (int i = 0; i < _capacity; i++) {
    table[i] = NULL;
  }
}

int StringHash::size() {
  return count;
}

const QName* StringHash::get( const char* value)
{
  QName* entry;
  int    hash = calcHashCode(value);
  int    index = (hash & 0x7FFFFFFF) % _capacity;

  entry = table[index];
  while (entry != NULL) {
    if ((entry->hashCode() == hash) && entry->equals( value )) {
      return entry;
    }
    entry = entry->_next;
  }
  return NULL;
}

const QName* StringHash::get( char buffer[], int startIndex, int endIndex ) {
  QName*      entry;
  int         hash = calcHashCode(buffer, startIndex, endIndex);
  int         index = (hash & 0x7FFFFFFF) % _capacity;

  entry = table[index];
  while (entry != NULL) {
    if ((entry->hashCode() == hash) && entry->equals( buffer, startIndex, endIndex )) {
      return entry;
    }
    entry = entry->_next;
  }
  return NULL;
}

const QName* StringHash::put( char buffer[], int startIndex, int endIndex) {
  int    hash, tableIndex;
  QName *entry;

  //do not add empty Strings
  if (startIndex >= endIndex) {
    return NULL;
  }

  // Makes sure the key is not already in the hashtable.
  entry = (QName*) get(buffer, startIndex, endIndex);

  if (entry == NULL) {
    // Creates the new entry.
    int len = endIndex - startIndex;
    char* name = new char[len+1];
    strncpy( name, &buffer[startIndex], len );
    name[len] = 0;
    entry = new QName( _namespace, name );
    tableIndex = (entry->hashCode() & 0x7FFFFFFF) % _capacity;
    entry->_next = table[tableIndex];
    table[tableIndex] = entry;
    count++;

    modCount++;
    if (count >= threshold) {
      // Rehash the binding if the threshold is exceeded
      //rehash();
    }
  }
  return entry;
}

const QName* StringHash::put( const char* value ) {
  int    tableIndex;
  QName* entry;

  //do not add empty Strings
  if ((value == NULL) || (strlen( value ) == 0)) {
    return NULL;
  }

  // Makes sure the key is not already in the hashtable.
  entry = (QName*) get(value);

  if (entry == NULL) {
    // Creates the new entry.
    entry = new QName( _namespace, value );
    tableIndex = (entry->hashCode() & 0x7FFFFFFF) % _capacity;
    entry->_next = table[tableIndex];
    table[tableIndex] = entry;
    count++;

    modCount++;
    if (count >= threshold) {
      // Rehash the binding if the threshold is exceeded
      //rehash();
    }
  }
  return entry;
}
