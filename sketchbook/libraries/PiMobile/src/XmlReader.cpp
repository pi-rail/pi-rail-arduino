/*
 * This file is part of PI-Rail-Arduino (https://gitlab.com/pi-rail/pi-rail-arduino).
 * Copyright (c) 2013-2020 PI-Data AG, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <string.h>
#include <stdlib.h>
#include "XmlReader.h"
#include "Namespace.h"

const bool DEBUG = false;

static const Namespace* NAMESPACE = Namespace::getInstance( NSURI_PI_MODELS );

/** This constant represents missing namespace */
static const Namespace* NAMESPACE_NONE = Namespace::getInstance("");
static const Namespace* NAMESPACE_XML = Namespace::getInstance( NSURI_XMLSCHEMA );

extern void fatalError( const char* msg1, const char* msg2 );

XmlReader::XmlReader( int textBufSize ) {
  //this->txtBuf = new char[textBufSize+1];
  this->txtBuf_length = textBufSize;
}

char* XmlReader::lastLine() {
  if (txtPos < 1) return 0;
  else return txtBuf;
}

bool XmlReader::compareString( const char* compStr, int startPos, int endPos ) {
  int len = endPos - startPos;
  if (strlen(compStr) != len) return false;
  for (int i = 0; i < len; i++) {
    if (compStr[i] != txtBuf[startPos+i]) return false;
  }
  return true;
}

byte XmlReader::getEntity( int startPos, int endPos ) {
  if (compareString( ENTITY_AMP, startPos, endPos )) return '&';
  if (compareString( ENTITY_APOS, startPos, endPos )) return '\'';
  if (compareString( ENTITY_GT, startPos, endPos )) return '>';
  if (compareString( ENTITY_LT, startPos, endPos )) return '<';
  if (compareString( ENTITY_QUOT, startPos, endPos )) return '"';
  if (compareString( ENTITY_AUML, startPos, endPos )) return 'a';
  if (compareString( ENTITY_OUML, startPos, endPos )) return 'ö';
  if (compareString( ENTITY_UUML, startPos, endPos )) return 'ü';
  if (compareString( ENTITY_AUML2, startPos, endPos )) return 'Ä';
  if (compareString( ENTITY_OUML2, startPos, endPos )) return 'Ö';
  if (compareString( ENTITY_UUML2, startPos, endPos )) return 'Ü';
  if (compareString( ENTITY_SZLIG, startPos, endPos )) return 'ß';
  if (txtBuf[startPos] == '#') {
    txtBuf[endPos+1] = 0;
    if (txtBuf[startPos+1] == 'x') {
      unsigned long value = 0;
      if (createLongHex( startPos+2, endPos, &value ) == 0) {
        if (value > 255) {
          if (value < 0xFFFF) {
            byte first = (value & 0xFF00);
            byte second = (value & 0xFF);
            switch (first) {     // conversion depending on first UTF8-character
              case 0xC2: {
                return (second);
                break;
              }
              case 0xC3: {
                return (second | 0xC0);
                break;
              }
            }
          }
          return -1;
        }
        else {
          return (byte) value;
        }
      }
      else {
        parseError(ERR_XMLREADER_ENTITY, txtBuf, startPos, endPos );
        return -1;
      }
    }
    else {
      long value;
      if (createLong( startPos+1, endPos, &value ) == 0) {
        if ((value > 255) || (value < 0)) {
          return -1;
        }
        else {
          return (byte) value;
        }
      }
      else {
        parseError(ERR_XMLREADER_ENTITY, txtBuf, startPos, endPos );
        return -1;
      }
    }
  }
  else {
    //throw new IOException("Unknown entity: "+str);
    return -1;
  }
}

/**
 * Looks for a namespace prefix matching the characters between startPos (included) and
 * endPos (excluded). nsPrefixes is searched from this.nsCounts[this.depth] backwards.
 *
 * @param startPos the position of first character of the prefix inside this.txtbuf
 * @param endPos   the position after the last character of the prefix inside this.txtbuf
 * @return the namespace identified by the prefix or null if not found
 */
Namespace* XmlReader::findNamespace( int startPos, int endPos ) {
  if (depth >= 0) {
    char *prefix;
    int prefixNum = nsCounts[depth] - 1;
    while (prefixNum >= 0) {
      prefix = nsPrefixes[prefixNum];
      if ((prefix != NULL) && compareString( prefix, startPos, endPos )) {
        return namespaces[prefixNum];
      }
      prefixNum--;
    }
  }
  return NULL;
}

Namespace* XmlReader::getDefaultNamespace() {
  if (depth >= 0) {
    int prefixNum = nsCounts[depth] - 1;
    while (prefixNum >= 0) {
      if (strlen( nsPrefixes[prefixNum] ) == 0) {
        return namespaces[prefixNum];
      }
      prefixNum--;
    }
  }
  return NULL;
}

/*void XmlReader::setInput(InputStreamReader reader) {
  this.reader = reader;

  line = 1;
  column = 0;
  targetNamespace = null;
  //defaultNamespace = NAMESPACE_NONE;

  if (reader == null)
    return;

  srcPos = 0;
  srcCount = 0;
  depth = 0;
}
*/
void XmlReader::parseError( const char *message, char* textBuf, int startPos, int endPos ) {
  int msgLen = strlen( message );
  if (msgLen > ERROR_MAX_LEN) {
    strncpy( errorMsg, message, ERROR_MAX_LEN-1 );
    errorMsg[ERROR_MAX_LEN-1] = 0;
    return;
  }
  strcpy( errorMsg, message );

  char intStr[6];
  if (msgLen + 10 < ERROR_MAX_LEN ) {
    strcat( errorMsg, MSG_ROW );
    //strcat( errorMsg, itoa( line, intStr, 10 ));
  }
  if (msgLen + 20 < ERROR_MAX_LEN ) {
    strcat( errorMsg, MSG_COL );
    //strcat( errorMsg, itoa( column, intStr, 10 ));
  }
  int i = 0;
  int pos = strlen(message);
  while ((i < (ERROR_MAX_LEN-1)) && ((startPos + i) < endPos)) {
    errorMsg[pos+i] = textBuf[startPos+i];
    i++;
  }
  errorMsg[pos+1] = 0;
  fatalError( "XML parse error", errorMsg );
}

void XmlReader::parseError( const char *message, char errorChar ) {
  parseError( message, txtBuf, txtPos-1, txtPos );
}

/**
 * Reads next character from srcBuf. If end of srcBuf is reached srcBuf is refilled from dataStream.
 * Copies character to txtBuf[txtPos] if not CR, LF or XML entity.
 * Converts XML entities and places matching character in txtBuf.
 * When writing character to txtBuf, txtPos is incremented .
 *
 * @return the character read or 0 if an error occurred.
 */
char XmlReader::read() {
  int character;
  char bufChar = 0;
  int entityStart = -1;

  do {
    character = srcBuf[srcPos++];
    if ((character == 0) && (dataStream != NULL)) {
      size_t readCount = dataStream->available();
      if (readCount > this->srcBufSize) {
        readCount = this->srcBufSize;
      }
      else if (readCount == 0) {
        srcPos--;
        parseError( ERR_XMLREADER_EOF, character );
        return 0;
      }
      readCount = dataStream->readBytes( srcBuf, readCount );
      srcBuf[readCount] = 0;
      srcPos = 0;
      character = srcBuf[srcPos++];
    }

    if (entityStart > 0) {
      if (character == ';') {
        char entityChar = getEntity( entityStart, txtPos );
        if (entityChar < 0) {
          //throw new IOException("Unknown entity: "+str);
          bufChar = '?';
        }
        else {
          bufChar = entityChar;
        }
        txtPos = entityStart;
      }
      else {
        if (txtPos <= txtBuf_length) {
          txtBuf[txtPos++] = character;
        }
        else {
          parseError( ERR_XMLREADER_EOF, character );
          return 0;
        }
      }
    }
    else {
      if (character == '&') {
        entityStart = txtPos;
      }
      else {
        bufChar = (char) character;
      }
    }
  } while (bufChar == 0);

  if (txtPos <= txtBuf_length) {
    txtBuf[txtPos++] = bufChar;
  }
  else {
    parseError( ERR_XMLREADER_EOF, character );
    return 0;
  }
  parseIndex++;
  return (char) character;
}

char* XmlReader::readString() {
  char c;
  int startPos = txtPos - 1;
  do {
    c = read();
    if (c == 0) {
      return NULL;
    }
  }
  while ((c >= 'a' && c <= 'z')
      || (c >= 'A' && c <= 'Z')
      || (c >= '0' && c <= '9')
      || c == '_'
      || c == '-'
      || c == ':'
      || c == '.');

  int len = txtPos - startPos - 1;
  char* str = new char[len];
  strncpy( str, &txtBuf[startPos], len );
  return str;
}

const QName* XmlReader::createQName( Namespace* defaultNS, int startPos, int nsPos, int endPos ) {
  int count = endPos - startPos;
  if (count <= 0) {
    return NULL;
  }
  else {
    Namespace *ns;
    if (nsPos < 0) {
      ns = defaultNS;
      if (ns == NULL) {
        parseError( ERR_DEFAULT_NS_UNDEFINDED, NULL, 0, 0 );
        return NULL;
      }
    }
    else {
      ns = findNamespace( startPos, nsPos );
      if (ns == NULL) {
        parseError( ERR_UNKNOWN_NS_PREFIX, txtBuf, startPos, (nsPos - startPos));
        return NULL;
      }
      startPos = nsPos + 1;
    }
    return ns->getQName( txtBuf, startPos, endPos );
  }
}

int XmlReader::parseQName() {
  char c;
  int nsPos = -1;
  do {
    c = read();
    if (c == 0) {
      return -2;
    }

    //--- tagName contains namespace prefix
    if (c == ':') {
      nsPos = txtPos-1;
    }
  }
  while ((c >= 'a' && c <= 'z')
      || (c >= 'A' && c <= 'Z')
      || (c >= '0' && c <= '9')
      || c == '_'
      || c == '-'
      || c == ':'
      || c == '.');

  return nsPos;
}

/*void* XmlReader::createValue( Namespace* defaultNS, QName attrName, int type, int startPos, int endPos) {
  Namespace* ns;

  if (type == null) {
    throw new IllegalArgumentException("Attribute type must not be null.");
  }

  if (type instanceof QNameType) {*/
const QName* XmlReader::createQName( Namespace* defaultNS, int startPos, int endPos ) {
  Namespace* ns = defaultNS;
  int nsPos = -1;
  for (int i = startPos; i < endPos; i++) {
    if (txtBuf[i] == ':') {
      nsPos = i;
      break;
    }
  }
  if (nsPos < 0) {
    if (targetNamespace != NULL) {
      ns = targetNamespace;
    }
    return ns->getQName(txtBuf, startPos, endPos);
  }
  else {
    ns = findNamespace(startPos, nsPos);
    if (ns == NULL) {
      parseError( ERR_UNKNOWN_NS, txtBuf, startPos, nsPos );
      return NULL;
    }
    return ns->getQName(txtBuf, nsPos+1, endPos);
  }
}

const char * XmlReader::createString( int startPos, int endPos, char* strPtr, int maxLen ) {
  int count = endPos - startPos;
  if (maxLen == 1) {
    // Single char is not terminated!
    if (count <= 0) {
      strPtr[0] = 0;
    }
    else {
      strPtr[0] = txtBuf[startPos];
    }
  }
  else {
    if (count > maxLen) {
      count = maxLen;
    }
    if (count > 0) {
      strncpy( strPtr, &txtBuf[startPos], count );
      strPtr[count] = 0;
    }
    else {
      strPtr[0] = 0;
    }
  }
  return 0;
}

const char * XmlReader::createByte( int startPos, int endPos, byte* bytePtr ) {
  int scale = 1;
  char value = 0;
  int sign = 1;
  if (txtBuf[startPos] == '-') {
    sign = -1;
    startPos++;
  }
  for (int i = endPos-1; i >= startPos; i--) {
    char c = txtBuf[i] - '0';
    if (c <= 9) {
      value += (scale * c);
      scale *= 10;
    }
    else {
      return ERR_NUMBER_FORMAT;
    }
  }
  *bytePtr = (value * sign);
  return 0;
}

const char * XmlReader::createShort( int startPos, int endPos, short* shortPtr ) {
  int scale = 1;
  short value = 0;
  int sign = 1;
  if (txtBuf[startPos] == '-') {
    sign = -1;
    startPos++;
  }
  for (int i = endPos-1; i >= startPos; i--) {
    char c = txtBuf[i] - '0';
    if (c <= 9) {
      value += (scale * c);
      scale *= 10;
    }
    else {
      return ERR_NUMBER_FORMAT;
    }
  }
  *shortPtr = (value * sign);
  return 0;
}

const char * XmlReader::createInt( int startPos, int endPos, int* intPtr ) {
  int scale = 1;
  int value = 0;
  int sign = 1;
  if (txtBuf[startPos] == '-') {
    sign = -1;
    startPos++;
  }
  for (int i = endPos-1; i >= startPos; i--) {
    char c = txtBuf[i] - '0';
    if (c <= 9) {
      value += (scale * c);
      scale *= 10;
    }
    else {
      return ERR_NUMBER_FORMAT;
    }
  }
  *intPtr = (value * sign);
  return 0;
}

const char * XmlReader::createLong( int startPos, int endPos, long* longPtr ) {
  long scale = 1;
  long value = 0;
  long sign = 1;
  if (txtBuf[startPos] == '-') {
    sign = -1;
    startPos++;
  }
  for (int i = endPos-1; i >= startPos; i--) {
    char c = txtBuf[i] - '0';
    if (c <= 9) {
      value += (scale * c);
      scale *= 10;
    }
    else {
      return ERR_NUMBER_FORMAT;
    }
  }
  *longPtr = (value * sign);
  return NULL;
}

const char * XmlReader::createLongHex( int startPos, int endPos, unsigned long* longPtr ) {
  long scale = 1;
  long value = 0;
  for (int i = endPos-1; i >= startPos; i--) {
    char c = txtBuf[i];
    if ((c >= '0') && (c <= '9')) {
      value += (scale * (c - '0'));
      scale *= 16;
    }
    else if ((c >= 'A') && (c <= 'F')) {
      value += (scale * (c - 'A' + 10));
      scale *= 16;
    }
    else if ((c >= 'a') && (c <= 'f')) {
      value += (scale * (c - 'a' + 10));
      scale *= 16;
    }
    else {
      return ERR_NUMBER_FORMAT;
    }
  }
  *longPtr = value;
  return 0;
}

const char * XmlReader::createBool( int startPos, int endPos, bool* boolPtr ) {
  if (endPos > startPos) {
    *boolPtr = compareString( VALUE_TRUE, startPos, endPos );
  }
  else {
    *boolPtr = false;
  }
  return NULL;
}

const char * XmlReader::createDateTime( int startPos, int endPos, char* str, int maxLen  ) {
  return createString( startPos, endPos, str, maxLen );
}

const char * XmlReader::createDecimal( int startPos, int endPos, char* str, int maxLen  ) {
  return createString( startPos, endPos, str, maxLen );
}

const char * XmlReader::createBinary( int startPos, int endPos, char* str, int maxLen  ) {
  return createString( startPos, endPos, str, maxLen );
}

const char * XmlReader::createDuration( int startPos, int endPos, char* str, int maxLen ) {
  return createString( startPos, endPos, str, maxLen );
}

/*  else {
    SimpleType baseType = (SimpleType) type.getBaseType();
    if (baseType == null) {
      parseError( "Unsupported SimpleType: " + type.name().getName(), NULL, 0, 0 );
      return null;
    }
    else {
      return createValue(defaultNS, attrName, baseType, startPos, endPos);
    }
  }
}
*/
/**
 * Parse a value between "
 * Exits when having read trailing "
 */
void XmlReader::parseValue(char closeChar) {
  char c;
  do {
    c = read();
    if (c == 0) {
      return;
    }
  }
  while (c != closeChar);
}

/**
 * Reads whitespace and increments "line" if CR is read.
 * Starts reading with current char
 * @return the first character not beeing white space
 * @throws IOException
 */
char XmlReader::readWhitespace() {
  char c = txtBuf[txtPos-1];
  while (c <= ' ') {
    if (c == '\n') line++;
    c = read();
    if (c == 0) {
      return 0;
    }
  }
  return c;
}

/*void* XmlReader::createAttributes( void* attributes, Namespace tagNS, ComplexType nodeType) {
  QName        attrName;
  int          attrIndex;
  SimpleType   attrType;
  Namespace    defaultValueNS;
  AnyAttribute anyAttr;
  int          keyAttrCount = nodeType.keyAttributeCount();
  bool         simpleKey = false;
  Object[]     keyAttrs = null;
  Key          key = null;

  if (keyAttrCount == 1) {
    simpleKey = true;
  }
  else if (keyAttrCount > 0) {
    keyAttrs = new Object[keyAttrCount];
  }

  if (attributes == null) {
    if (keyAttrCount > 0) {
      throw new IllegalArgumentException("Key attributes must be defined for type="+nodeType.name());
    }
    attributes = null;
  }
  else {
    int attrDefCount = attributes.length;
    defaultValueNS = getDefaultNamespace();
    if (defaultValueNS == null) defaultValueNS = nodeType.name().getNamespace();
    for (int i = 0; i < attrDefCount; i++) {
      attributes[i] = UNDEFINED;
    }
    for (int i = 0; i < this.attrCount; i++) {
      attrName = createQName(tagNS, attrStart[i], attrNsPos[i], attrEnd[i]); //TODO Any-Attribute können einen NS-Prefix haben??
      if (DEBUG) System.out.print(attrName.getName()+ " ");
      attrIndex = nodeType.indexOfAttribute(attrName);
      //----- anyAttribute
      if (attrIndex < 0) {
        anyAttr = nodeType.getAnyAttribute();
        if ((anyAttr == null) && (attrName.getNamespace() != XSIFactory.NAMESPACE)) {
          parseError("Unknown attribute: "+attrName);
        }
        attrType = AnyAttribute.findType(attrName);
        if (attrType == null) {
          parseError("Type not found for any attribute name="+attrName);
        }
        if (this.anyAttribs == null) {
          this.anyAttribs = new Hashtable();
        }
        this.anyAttribs.put(attrName, createValue(defaultValueNS, attrName, attrType, valueStart[i], valueEnd[i]));
      }
      else {
        attrType = nodeType.getAttributeType(attrIndex);
        Object value = createValue(defaultValueNS, attrName, attrType, valueStart[i], valueEnd[i]);
        int keyIndex = nodeType.getKeyIndex(attrName);
        //----- key attribute
        if (keyIndex >= 0) {
          attributes[attrIndex] = Key.KEY_ATTR;
          if (simpleKey) {
            if (attrType instanceof QNameType) {
              key = (QName) value;
            }
            else {
              key = new SimpleKey( value );
            }
          }
          else {
            keyAttrs[keyIndex] = value;
          }
        }
          //----- normal attribute
        else {
          attributes[attrIndex] = value;
        }
        if (attrName.getName().equals(ATTR_TARGETNAMESPACE)) {
          this.targetNamespace = Namespace.getInstance((String) value);
        }
      }
    }
    for (int i = 0; i < attrDefCount; i++) {
      if (attributes[i] == UNDEFINED) {
        attributes[i] = nodeType.getAttributeDefault(i);
      }
    }
  }

  //----- build combined key
  if (keyAttrs != null) {
    key = new CombinedKey( keyAttrs );
  }
  return key;
}
*/

int XmlReader::indexOfAttr( const QName* attrName ) {
  const char* name = attrName->getName();
  for (int i = 0; i < attrCount; i++) {
    int start = attrNsPos[i];
    if (start < 0) start = attrStart[i];
    if (compareString( name, start, attrEnd[i] )) {
      return i;
    }
  }
  return -1;
}

const char* XmlReader::getStringAttr( const QName* attrName, char* valuePtr, int maxValueLen ) {
  int attrIndex = indexOfAttr( attrName );
  if (attrIndex >= 0) {
    return createString( valueStart[attrIndex], valueEnd[attrIndex], valuePtr, maxValueLen );
  }
  else {
    valuePtr[0] = 0;
    return 0;
  }
}

int XmlReader::findNSPos( int startPos, int endPos ) {
  for (int i = startPos; i < endPos; i++) {
    if (txtBuf[i] == ':') {
      return i;
    }
  }
  return -1;
}

const char* XmlReader::getQNameAttr( const QName* attrName, const QName** valuePtr ) {
  int attrIndex = indexOfAttr( attrName );
  if (attrIndex >= 0) {
    int nsPos = findNSPos( valueStart[attrIndex], valueEnd[attrIndex] );
    *valuePtr = createQName( getDefaultNamespace(), valueStart[attrIndex], nsPos, valueEnd[attrIndex] );
    return 0;
  }
  else {
    *valuePtr = NULL;
    return 0;
  }
}

const char* XmlReader::getIntAttr( const QName* attrName, int* valuePtr, int defaultValue ) {
  int attrIndex = indexOfAttr( attrName );
  if (attrIndex >= 0) {
    return createInt( valueStart[attrIndex], valueEnd[attrIndex], valuePtr );
  }
  else {
    *valuePtr = defaultValue;
    return 0;
  }
}

const char* XmlReader::getLongAttr( const QName* attrName, long* valuePtr, long defaultValue ) {
  int attrIndex = indexOfAttr( attrName );
  if (attrIndex >= 0) {
    return createLong( valueStart[attrIndex], valueEnd[attrIndex], valuePtr );
  }
  else {
    *valuePtr = defaultValue;
    return 0;
  }
}

const char* XmlReader::getShortAttr( const QName* attrName, short* valuePtr, short defaultValue ) {
  int attrIndex = indexOfAttr( attrName );
  if (attrIndex >= 0) {
    return createShort( valueStart[attrIndex], valueEnd[attrIndex], valuePtr );
  }
  else {
    *valuePtr = defaultValue;
    return 0;
  }
}

const char* XmlReader::getByteAttr( const QName* attrName, byte* valuePtr, char defaultValue ) {
  int attrIndex = indexOfAttr( attrName );
  if (attrIndex >= 0) {
    return createByte( valueStart[attrIndex], valueEnd[attrIndex], valuePtr );
  }
  else {
    *valuePtr = defaultValue;
    return 0;
  }
}

const char* XmlReader::getBoolAttr( const QName* attrName, bool * valuePtr ) {
  int attrIndex = indexOfAttr( attrName );
  if (attrIndex >= 0) {
    return createBool( valueStart[attrIndex], valueEnd[attrIndex], valuePtr );
  }
  else {
    *valuePtr = false;
    return 0;
  }
}

const char* XmlReader::getEnumAttr( const QName* attrName, char *valueSet[], int valueCount, int * valuePtr ) {
  int attrIndex = indexOfAttr( attrName );
  if (attrIndex >= 0) {
    int length = valueEnd[attrIndex] - valueStart[attrIndex];
    char * start =  &txtBuf[valueStart[attrIndex]];
    for (int i = 0; i < valueCount; ++i) {
      char *value = valueSet[i];
      if (length == strlen( value ) && !strncmp( value, start, length) ) {
        *valuePtr = i;
        return 0;
      }
    }
    *valuePtr = -1;
    return ERR_VALUE_NOT_FOUND;
  }
  else {
    *valuePtr = -1;
    return ERR_ATTR_NOT_FOUND;
  }
}

Namespace* XmlReader::processNsDecl( int attrIndex ) {
  char* nsName;
  char* prefix;
  Namespace* ns;

  if (attrNsPos[attrIndex] < 0) {
    if (compareString( ATTR_XMLNS, attrStart[attrIndex], attrEnd[attrIndex])) {
      Namespace* ns = Namespace::getInstance( txtBuf, valueStart[attrIndex], valueEnd[attrIndex] );
      int pos = nsCounts[depth];
      namespaces[pos] = ns;
      strcpy( nsPrefixes[pos], "" );
      nsCounts[depth]++;
      return ns;
    }
  }
  else {
    if (compareString( ATTR_XMLNS, attrStart[attrIndex], attrNsPos[attrIndex])) {
      Namespace* ns = Namespace::getInstance( txtBuf, valueStart[attrIndex], valueEnd[attrIndex] );
      int pos = nsCounts[depth];
      namespaces[pos] = ns;
      createString( attrNsPos[attrIndex]+1, attrEnd[attrIndex], nsPrefixes[pos], MAX_PREFIX_LENGTH );
      nsCounts[depth]++;
      return ns;
    }
  }
  return NULL;
}

/**
 * Check if attribute is from XML Schema instance namespace. We have to process these
 * attributes here before processing tag and attributes.
 * @param attrIndex index of the attribute to check
 * @return true if attribute is from XML Schema instance namespace
 */
bool XmlReader::prcessXSI( int attrIndex ) {
  if ((xsiPrefix != 0) && (attrNsPos[attrIndex] > 0)) {
    if (compareString( xsiPrefix, attrStart[attrIndex], attrNsPos[attrIndex])) {
      if (compareString( ATTR_TYPE, attrNsPos[attrIndex] + 1, attrEnd[attrIndex])) {
        const QName* typeName = createQName( getDefaultNamespace(), valueStart[attrIndex], valueEnd[attrIndex] );
        //xsiType = ModelFactoryTable.getInstance().getFactory( typeName.getNamespace() ).getType( typeName );
        return true;
      }
    }
  }
  return false;
}

/*void XmlReader::expandIntArray( int* oldArray, int newSize ) {
  int newArray[newSize];
  System.arraycopy(oldArray, 0, newArray, 0, oldArray.length);
  //return newArray;
}

void XmlReader::checkAttrSizes() {
  if (attrCount >= sizeof(attrStart)) {
    int newSize = attrCount * 2;
    attrStart = expandIntArray( &attrStart, newSize );
    attrNsPos = expandIntArray( &attrNsPos, newSize );
    attrEnd = expandIntArray( &attrEnd, newSize );
    valueStart = expandIntArray( &valueStart, newSize );
    valueEnd = expandIntArray( &valueEnd, newSize );
  }
}
*/
char XmlReader::parseAttributes( char endChar )  {
  char       c;
  Namespace* ns;

  attrCount = 0;
  c = readWhitespace();

  while ((c != '>') && (c != endChar)) {
    //checkAttrSizes();
    attrStart[attrCount] = txtPos-1;
    int pos = parseQName(); // AnyAttribute haben einen NS-Prefix, andere nicht!
    if (pos == -2) {
      return 0;
    }
    attrNsPos[attrCount] = pos;
    attrEnd[attrCount] = txtPos-1;

    c = readWhitespace();
    if (c != '=') {
      parseError( ERR_EXPECTED_EQ, c );
      return 0;
    }
    c = read();
    c = readWhitespace();
    if (c != '"') {
      parseError( ERR_EXPECTED_DBLQUOTE, c );
      return 0;
    }
    valueStart[attrCount] = txtPos;
    parseValue('"');
    valueEnd[attrCount] = txtPos-1;

    c = txtBuf[txtPos-1];
    if (c != '"') {
      parseError(ERR_EXPECTED_DBLQUOTE, c );
      return 0;
    }
    c = read();
    c = readWhitespace();
    if (c == 0) {
      return 0;
    }
    ns = processNsDecl(attrCount);
    if (ns == NULL) {
      //if (!prcessXSI(attrCount)) {
        attrCount++;
      //}
    }
  }
  return c;
}

/*ChildList XmlReader::readChildren( QName parentRelation, ComplexType parentType) {
  ChildList children = new ChildList();
  QName  relation;
  do {
    relation = nextTag(parentRelation, parentType, children);
  }
  while (relation != null);
  return children;
}
*/
const char* XmlReader::readHeader( char* encodingPtr, int encodingMaxLen ) {
  char c;
  char* tag = readString();
  if (strcmp( TAG_XML, tag) != 0) {
    return ERR_HEADER_NOT_XML;
  }
  c = parseAttributes( '?' );
  if (c == 0) {
    return NULL;
  }
  for (int i = 0; i < attrCount; i++) {
    if (compareString( ATTR_ENCODING, attrStart[i], attrEnd[i] - attrStart[i] )) {
      createString( valueStart[i], valueEnd[i], encodingPtr, encodingMaxLen );
    }
  }
  while (c != '>') {
    c = read();
    if (c == 0) {
      return NULL;
    }
  }
  return NULL;
}

void XmlReader::readDocType() {
  char c;
  do {
    c = read();
    if (c == 0) {
      return;
    }
  } while (c != '>');
}

void XmlReader::readProcessingInstruction() {
  char c, c1;
  c = read();
  do {
    if (c == 0) {
      return;
    }
    c1 = c;
    c = read();
  } while ( !((c1=='?') && (c == '>')) );
}

void XmlReader::readComment() {
  char c, c1, c2;
  c = read();
  if (c == 0) {
    return;
  }
  if (c != '-') {
    if (c=='D') {
      readDocType();
    }
    else if (c=='?') {
      readProcessingInstruction();
    }
    else {
      parseError( ERR_EXPECTED_MINUS_D_QUESTION, c );
    }
    return;
  }
  c = read();
  if (c != '-') {
    parseError( ERR_EXPECTED_MINUS, c );
    return;
  }
  c2 = read();
  c = read();
  do {
    if (c == 0) {
      return;
    }
    c1 = c2;
    c2 = c;
    c = read();
  }
  while ( !((c1 == '-') && (c2 == '-') && (c == '>')) );
}

/**
 * Reads the next tag and creates the corresponding Model or returns null if
 * tag is a closing tag. Adds created models to parentChilds
 * @param  parentType the parent type for the new Model
 * @return true if parsed tag has children
 * @throws IOException
 */
const QName * XmlReader::nextTag( const QName* parentRelation ) {
  char c;
  const QName *relation;
  //Type         type;
  //ComplexType  nodeType;
  //Object[]     attributes;
  //ModelFactory nodeFactory;
  //ChildList    children;
  int tagStart, tagNsPos, tagEnd, valStart;
  bool foundTag = false;
  //Hashtable    anyAttribs;
  //Model        node;
  //Vector       nsDecls;

  //--- Loop until start tag begins
  while (!foundTag) {
    txtPos = 0;
    do {
      c = read();
      if (c == 0) return NULL;
    } while (c != '<');

    //--- Check for closing tag
    c = read();
    if (c == 0) {
      return NULL;
    }
    else if (c == '/') {
      processCloseTag( parentRelation );
      return NULL;
    }
    else if (c == '?') {
      const char * header = readHeader( encoding, ENCODING_MAX_LEN );
      if (header == NULL) {
        return NULL;
      }
    }
    else if (c == '!') {
      readComment();
    }
    else {
      foundTag = true;
    }
  }

  //--- Parse tag and attributes
  //xsiType = 0;
  tagStart = txtPos - 1;
  tagNsPos = parseQName();
  if (tagNsPos == -2) { // EOF
    return NULL;
  }
  tagEnd = txtPos - 1;
  //nsDecls = new Vector();
  c = parseAttributes( '/' );
  if (c == 0) {
    return NULL;
  }

  Namespace *tagNsDef = getDefaultNamespace();
  if ((tagNsDef == NULL) && (parentRelation != NULL)) {
    tagNsDef = parentRelation->getNamespace();
  }
  relation = createQName( tagNsDef, tagStart, tagNsPos, tagEnd );
/*  if (parentType == NULL) {
    ModelFactory factory = ModelFactoryTable.getInstance().getFactory( relation.getNamespace() );
    if (factory == null) {
      parseError("Factory not found (perhaps missing in system.properties) for Namespace: "+relation.getNamespace());
    }
    Relation rel = factory.getRootRelation(relation);
    if (rel == null) {
      parseError("Relation not found: "+relation);
    }
    type = rel.getChildType();
  }
  else {
    type = parentType.getChildType(relation);
  }
  if (DEBUG) {
    for (int sp = 0; sp < depth; sp++) System.out.print("  ");
    System.out.print("<"+relation+ " ");
  }
  if (type == null) {
    parseError("Type not found, relation="+relation);
  }

  if (xsiType != NULL) {
    if (!type.isAssignableFrom( xsiType )) {
      parseError( "xsi:type="+xsiType.name()+" does not inherit from required type="+type );
    }
    type = xsiType;
  }

  nodeFactory = ModelFactoryTable.getInstance().getFactory( type.name().getNamespace() );
  if (nodeFactory == null) {
    Logger.warn("No factory found for node tag=" + relation);
    return null;
  }
*/
  return relation;
}

void XmlReader::parseSimpleValue( const QName* relation, char* value, int valueMaxLen ) {
  char c = txtBuf[txtPos-1];
  if (c != '>') {
    if (c == ' ') c = readWhitespace();
    else if (c != '/') {
      parseError( ERR_EXPECTED_SLASH, c );
      return;
    }
  }
  if (c == '/') {
    value[0] = 0;
  }
  else {
    // if (DEBUG) System.out.print(">");
    int valStart = txtPos;
    parseValue('<');
    createString( valStart, txtPos-1, value, valueMaxLen );
    c = read();
    if (c != '/') {
      parseError( ERR_EXPECTED_SLASH, c );
      return;
    }
    processCloseTag(relation);
  }
  /*node = nodeFactory.createInstance((SimpleType) type, value);
  if (node == null) {
    parseError("Could not create node for relation="+relation+" typeID="+type.name());
  }
  addNamespaces(node, nsDecls);
  parentChilds.add(relation, node);*/
}

bool XmlReader::hasChildren() {
  char c = txtBuf[txtPos-1];
  //--- Read attributes
  //nodeType = (ComplexType) type;
  //anyAttribs = null;
  //int attrDefCount = nodeType.attributeCount();
  //attributes = new Object[attrDefCount];
  //Key key =
  //createAttributes( attributes, nodeType.name().getNamespace(), nodeType);
  //anyAttribs = this.anyAttribs;

  //--- Read children
  if (c == '/') {
    c = read();
    if (c != '>') {
      parseError( ERR_EXPECTED_GT, c );
      return false;
    }
    //if (DEBUG) System.out.println("/>");
    //children = null;
    return false;
  }
  else {
    //if (DEBUG) System.out.println(">");
    depth++;
    if (depth > 0) {
      nsCounts[depth] = nsCounts[depth-1];
    }
    //children = readChildren(relation, nodeType);
    //this.depth--;
    return true;
  }

  //--- Build node
  /*node = null;
  try {
    node = nodeFactory.createInstance(key, nodeType, attributes, anyAttribs, children);
  }
  catch (Exception ex) {
    parseError("Could not create node for relation="+relation+" typeID="+type.name()+" ex="+ex.getMessage());
  }
  if (node == null) {
    parseError("Could not create node for relation="+relation+" typeID="+type.name());
  }
  addNamespaces(node, nsDecls);
  parentChilds.add(relation, node);
  return relation; */
}

/**
 * Add the namespaces declared on the node
 * @param node the node
 * @param nsDecls the namepspaces declared on the node
 */
/*void XmlReader::addNamespaces( Model node, Vector nsDecls ) {
  if (nsDecls.size() > 0) {
    NamespaceTable nsTab = node.ownNamespaceTable();
    for (int i = 0; i < nsDecls.size(); i++) {
      Namespace namespace = (Namespace) nsDecls.elementAt(i);
      nsTab.addNamespace( namespace, (String) nsPrefixes.elementAt( namespaces.indexOf( namespace ) ) );
    }
  }
}*/

void XmlReader::processCloseTag( const QName *parentRelation ) {
  int tagStart;
  int tagNsPos;
  int tagEnd;
  const QName* relation;
  tagStart = txtPos;
  tagNsPos = parseQName();
  if (tagNsPos == -2) {
    return;
  }
  tagEnd = txtPos - 1;
  Namespace* tagNsDef;
  if ((tagNsDef == NULL) && (parentRelation != NULL)) {
    tagNsDef = parentRelation->getNamespace();
  }
  else{
    tagNsDef = getDefaultNamespace();
  }
  depth--;
  relation = createQName(tagNsDef, tagStart, tagNsPos, tagEnd);
  //TODO prüfe, ob zum öffnenden tag passt
  /*if (DEBUG) {
    for (int sp = 0; sp < this.depth-1; sp++) System.out.print("  ");
    System.out.println("</"+relation+">");
  }*/
}

/*void XmlReader::dumpInput() {
  char c;
  int max = MAX_DUMP_CHARS;
  try {
    do {
      c = read();
      System.out.print(c);
      max--;
    }
    while ((c > 0) && (max > 0));
    if (c > 0) {
      System.out.print( "[...]" );
    }
    System.out.println();
  }
  catch (Exception ex) {
    // ignore further exceptions
  }
}*/

void XmlReader::initParser( char* xmlBuffer, int xmlBufferSize, Stream* inputStream ) {
  depth = 0;
  nsCounts[0] = 0;
  srcBuf = xmlBuffer;
  srcBufSize = (xmlBufferSize >> 1) - 1;
  srcBuf[srcBufSize] = 0;
  dataStream = inputStream;
  txtBuf = &xmlBuffer[srcBufSize+1];
  txtBuf_length = srcBufSize;
  txtBuf[0] = 0;
  srcPos = 0;
  strcpy( encoding, CHARSET_UTF8 );
}

/**
 * Loads data from dataResource
 * Note: dataStream is NOT closed by loadData!
 *
 * @param dataStream the fully qualified data resource name
 * @param progressListener
 * @param continuous       if true no wrapping reader is created to avoid reading additional characters from
 *                         dataStream after end of XML document
 * @throws java.io.IOException
 */
/*Model XmlReader::loadData( InputStream dataStream, ProgressListener progressListener, bool continuous, Namespace defaultNS ) {
  this.progressListener = progressListener;
  this.continuous = continuous;
//    DefaultComplexType rootType = (DefaultComplexType) root.type();
  ChildList children;

  //----- open stream and read until first start tag
  if (DEBUG) Logger.debug("XmlReader.loadData URL=" + dataStream);
  if (dataStream == null) {
    throw new IllegalArgumentException("loadData: dataStream must not be null!");
  }

  this.modelNameStack.removeAllElements();
  this.namespaces.removeAllElements();
  this.nsPrefixes.removeAllElements();
  if (ModelFactoryTable.getInstance().getFactory( XmlFactory.NAMESPACE ) == null) {
    new XmlFactory();
  }
  storeNamespace( XmlFactory.NAMESPACE, "xml" ); // implicit Namespace prefix "xml", e.g. for attribute "xml:lang"
  depth = 0;
  nsCounts[0] = 0;
  if (defaultNS != NULL) {
    storeNamespace( defaultNS, "" );
  }

  this.dataStream = dataStream;
  srcCount = 0;

  children = new ChildList();
  nextTag(null, null, children);

  //close data stream
  reader = null;

  if (children.size() != 1) {
    throw new IllegalArgumentException("XML file must have exactly 1 root element");
  }
  Model rootNode = children.getFirstChild(null);
  return rootNode;
}*/
