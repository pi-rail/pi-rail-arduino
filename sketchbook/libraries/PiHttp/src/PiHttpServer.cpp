/*
 * This file is part of PI-Rail-Arduino (https://gitlab.com/pi-rail/pi-rail-arduino).
 * Copyright (c) 2013-2020 PI-Data AG, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <Arduino.h>
#include <FS.h>

#include "PiHttpServer.h"
#include "FileCfg.h"

#ifdef ESP32
#include "SPIFFS.h"
#endif

#define PI_HTTP_FILENAME_LENGTH 32
#define PI_HTTP_BUFSIZE 2048

/*

 ToDo: Fehlerbehandlung - momentan wird Quatsch angezeigt, wenn die Datei für download oder edit (noch) nicht da ist.
 ToDo: Nach erfolgreichem Upload redirect zu /index.html, incl. Rückmeldung dass OK
 ToDo: Nach fehlgeschlagenem Upload auf der Seite bleiben, incl. Fehlermeldung
 ToDo: evtl. logs reduzieren

*/

static PROGMEM const char *default_html_1 = "<html>\n"
                                            "<head>\n"
                                            "<meta charset=\"UTF-8\">\n"
                                            "<title>";

static PROGMEM const char *default_html_2 = "</title>\n"
                                            "</head>\n"
                                            "<body>\n";

static PROGMEM const char *default_html_Z = "</body>\n"
                                            "</html>";

static PROGMEM const char *editWifi_html_1 = "/setwifi\" method=\"post\">\n"
                                             "<label for=\"ssid\">WLAN SSID:</label><br>\n"
                                             "<input type=\"text\" id=\"ssid\" name=\"ssid\" value=\"";
static PROGMEM const char *editWifi_html_2 = "\"><br>\n"
                                             "<label for=\"passwd\">WLAN password:</label><br>"
                                             "<input type=\"text\" id=\"passwd\" name=\"passwd\" value=\"";
static PROGMEM const char *editWifi_html_3 = "\"><br><br>\n"
                                             "<input type=\"submit\" value=\"Save\">\n"
                                             "</form>\n";

static PROGMEM const char *postMultipart = "\" method=\"post\" enctype=\"multipart/form-data\">\n";

static PROGMEM const char *formAction = "<form action=\"";

static PROGMEM const char *editTextfile_html_2 = "<textarea rows=\"20\" cols=\"80\" name=\"filecontent\">";

static PROGMEM const char *editTextfile_html_3 = "</textarea><br>\n"
                                                 "<button type=\"submit\">Save</button>\n"
                                                 "</form>\n";

static PROGMEM const char *uploadLink_html = "<a href=\"/upload.html\">Upload file</a>\n";

static PROGMEM const char *edit_wifi_link_html = "<a href=\"/editwifi.html\">Edit WiFi SSID and password</a><br>\n";

static PROGMEM const char *uploadForm_html_1 = "/fileUpload";

static PROGMEM const char *uploadForm_html_2 = "<input type=\"file\" name=\"data\">\n"
                                             "<p>\n"
                                             "<strong>Hint:</strong> The uploaded file is always stored with the same file name.\n"
                                             "<p>\n"
                                             "<input type=\"submit\" name=\"upload\" value=\"Upload\" title=\"Upload File\">\n"
                                             "</form>\n";

// Text snippets, not necessarily HTML
static PROGMEM const char *newline  = "\n";

// Single HTML tags
static PROGMEM const char *html_br = "<br>";
static PROGMEM const char *html_p = "<p>";
static PROGMEM const char *html_h3 = "<h3>";
static PROGMEM const char *html_h3_end = "</h3>\n";
static PROGMEM const char *html_h6 = "<h6>";
static PROGMEM const char *html_h6_end = "</h6>\n";
static PROGMEM const char *html_hr = "<hr>";

// HTML Snippets
static PROGMEM const char *fileDelete_html_1 = "<a href=\"/delete.html?file=";
static PROGMEM const char *fileDelete_html_2 = "\">delete</a>";

static PROGMEM const char *fileDownload_html_1 = "<a href=\"";
static PROGMEM const char *fileDownload_html_2 = "\">download</a>";

static PROGMEM const char *fileEdit_html_1 = "<a href=\"/edit.html?file=";
static PROGMEM const char *fileEdit_html_2 = "\">edit</a>";

static PROGMEM const char *picture_html_1 = "<img src=\"";
static PROGMEM const char *picture_html_2 = "\">";

// labels
static const char *label_filelist PROGMEM = ": File list";

// Filename suffixes
static PROGMEM const char *suffix_txt = ".txt";
static PROGMEM const char *suffix_xml = ".xml";
static PROGMEM const char *suffix_jpg = ".jpg";
static PROGMEM const char *suffix_png = ".png";

// mime types
static PROGMEM const char *mimeType_txt = "text/plain";
static PROGMEM const char *mimeType_html = "text/html";
static PROGMEM const char *mimeType_xml = "text/xml";
static PROGMEM const char *mimeType_jpg = "image/jpg";
static PROGMEM const char *mimeType_png = "image/png";
static PROGMEM const char *mimeType_other = "application/octet-stream";

// messages
static PROGMEM const char *httpStatusMessage_OK = "OK";
static PROGMEM const char *httpStatusMessage_FILE_NOT_FOUND = "File not found.";
static PROGMEM const char *httpStatusMessage_FS_ERROR = "File System Error.";
static PROGMEM const char *httpStatusMessage_SSID_EMPTY = "SSID must not be empty";

static PROGMEM const char *httpStatusMessage_FILE_NOT_DELETABLE = "File is configured as not deletable.";

static PROGMEM const char *httpStatusMessage_MISSING_PARAMETER_FILE = "Missing parameter: \"file\".";

//page names
static PROGMEM const char* INDEX_HTML = "/index.html";
static PROGMEM const char* FILECFG_XML = "/fileCfg.xml";
static PROGMEM const char* EDITWIFI_HTML = "/editwifi.html";
static PROGMEM const char* EDIT_HTML = "/edit.html";
static PROGMEM const char* DELETE_HTML = "/delete.html";
static PROGMEM const char* UPLOAD_HTML = "/upload.html";
static PROGMEM const char* SETWIFI = "/setwifi";
static PROGMEM const char* FILE_UPLOAD = "/fileUpload";

// other constants
static PROGMEM const char *icon_files[] = { "/icon.jpg" };
static PROGMEM const char *system_files[] = { "/netCfg.xml", "/ioCfg.xml", "/cfg.xml" };

static PROGMEM const char* PARAM_SSID = "ssid";
static PROGMEM const char* PARAM_PASSWD = "passwd";
static PROGMEM const char* PARAM_PATH = "path";
static PROGMEM const char* FILECONTENT = "filecontent";

const char *piSketchNameAndCompileDate;
const char *piDeviceName;
static NetCfg *piNetCfg;
static XmlWriter* _xmlWriter;

char filenameToEdit[PI_HTTP_FILENAME_LENGTH + 1];
uint8_t buffer[PI_HTTP_BUFSIZE + 1];

#ifdef UPLOAD_LOGGING
extern void debugStr( const char* msg, const char* msg2 = NULL, const char* msg3 = NULL, const char* msg4 = NULL );
#endif
extern void doReset( long afterMillis );
extern void httpServerAccessed();

AsyncWebServer server(80);

PiHttpServer::PiHttpServer() {
}

bool checkFilenameAndReboot(const char *path) {
  bool reboot = false;
  for (const char *sysFile : system_files) {
    if (strcmp( sysFile, path ) == 0) {
      reboot = true;
      break;
    }
  }
  if (reboot) {
    doReset( 2500 );
  }
  return reboot;
}

void handleGetFileCfg( AsyncWebServerRequest *request ) {
  httpServerAccessed();
#ifdef UPLOAD_LOGGING
  debugStr( "GetFileCfg" );
#endif
  AsyncResponseStream *pStream = request->beginResponseStream( mimeType_xml );
  pStream->println( "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" );
  pStream->print( "<fileCfg xmlns=\"http://res.pirail.org/pi-rail.xsd\" free=\"" );
#ifdef ESP32
  long freeBytes = SPIFFS.totalBytes() - SPIFFS.usedBytes();
#else
  FSInfo fs_info;
  SPIFFS.info(fs_info);
  long freeBytes = fs_info.totalBytes - fs_info.usedBytes;
#endif
  pStream->print( freeBytes );
  pStream->println( "\">" );

  if (SPIFFS.begin()) {

#ifdef ESP32
    File root = SPIFFS.open("/");
    File file = root.openNextFile();
    while(file) {
      const char *filename = file.name();
      pStream->print( "<fileDef name=\"" );
      pStream->print( filename );
      pStream->println( "\" deletable=\"false\"/>" );
      file = root.openNextFile();
    }
#else
    Dir dir = SPIFFS.openDir( "/" );
    while (dir.next()) {
      String filename = dir.fileName();
      pStream->print( "<fileDef name=\"" );
      pStream->print( filename.c_str());
      pStream->println( "\" deletable=\"false\"/>" );
    }
#endif
  }
  pStream->println( "</fileCfg>" );
  request->send(pStream);
}

void handleSetWifi( AsyncWebServerRequest *request ) {
  static String      path = "/netCfg.xml";
  static String      ssid;
  static String      passwd;
  static File        file;                              // File handle output file
  static int         httpStatus = 200;
  static const char* message = httpStatusMessage_OK;

  httpServerAccessed();
#ifdef UPLOAD_LOGGING
  debugStr( "SetWifiStart" );
#endif

  ssid = request->arg( PARAM_SSID );
  passwd = request->arg( PARAM_PASSWD );
  if (ssid.isEmpty()) {
    httpStatus = 500;
    message = httpStatusMessage_SSID_EMPTY;
    request->send( httpStatus, mimeType_txt, message );
  }
  else {
    WifiCfg* wifiCfg = piNetCfg->getWifiCfg();
    if (wifiCfg == NULL) {
      wifiCfg = (WifiCfg *) piRailFactory->getNewWifiCfg( piNetCfg, piRailFactory->ID_WIFICFG );
      piNetCfg->addChild( piRailFactory->ID_WIFICFG, wifiCfg, NULL );
    }
    wifiCfg->setSsid( (char*) ssid.c_str() );
    wifiCfg->setPass( (char*) passwd.c_str() );

    if (SPIFFS.exists( path )) {
      SPIFFS.remove( path );                             // Remove old file
#ifdef UPLOAD_LOGGING
      debugStr( "removed old file ", path );
#endif
    }
    file = SPIFFS.open( path, "w" );                     // Create new file
    if (file) {                                       // file write error
#ifdef UPLOAD_LOGGING
      debugStr( "opened file ", path, " for writing" );
#endif
      _xmlWriter->begin( piRailFactory->NS_PIRAILFACTORY );
      piNetCfg->writeXml( piRailFactory->ID_NETCFG, _xmlWriter );
      const char* xmldata = _xmlWriter->getXml();
      int xmllen = strlen( xmldata );

      if ( file.write( (uint8_t *) xmldata, xmllen ) != xmllen ) {        // file write error
        httpStatus = 500;
        message = httpStatusMessage_FS_ERROR;
      }
      file.flush();
      file.close();
#ifdef UPLOAD_LOGGING
      debugStr( "closed file ", path );
      debugStr( "SetWifiEnd: updated netCfg.xml" );
#endif

      request->send( httpStatus, mimeType_txt, message );

      checkFilenameAndReboot(path.c_str());
    }
    else {
#ifdef UPLOAD_LOGGING
      debugStr( "Error opening file '", path, "' for writing") );
#endif
      httpStatus = 500;
      message = httpStatusMessage_FS_ERROR;
      request->send( httpStatus, mimeType_txt, message );
    }
  }
}

void handlePostFile( AsyncWebServerRequest *request, const char *path ) {
  httpServerAccessed();
#ifdef REQUEST_LOGGING
  dumpHeaders(request);
  dumpParameters(request);
#endif
  if (request->hasParam( FILECONTENT, true, false)) {
    AsyncWebParameter *parameter = request->getParam( FILECONTENT, true, false);
    if ( parameter != NULL ) {
#ifdef UPLOAD_LOGGING
      debugStr( "File content :", parameter->value().c_str() );
#endif
      File file = SPIFFS.open(path, "w");
      if (file) {
        file.print(parameter->value().c_str());
        file.close();
      }
      else {
#ifdef UPLOAD_LOGGING
        debugStr( "Error opening file '", path, "' for writing" );
#endif
        return;
      }
    }
    else {
#ifdef UPLOAD_LOGGING
      debugStr( "parameter \"filecontent\" is null" );
#endif
    }
  }
  request->send_P( 200, mimeType_txt, "OK." );
  checkFilenameAndReboot( path );
}

void handleFileUpload( AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final ) {
  static String      path;
  static File        file;                              // File handle output file
  static uint32_t    totallength;                       // Total file length
  static size_t      lastindex;                         // To test same index
  static int         httpStatus = 200;
  static const char* message = httpStatusMessage_OK;

  bool fileAdded = false;
  if ( index == 0 ) {
    httpServerAccessed();

#ifdef UPLOAD_LOGGING
    debugStr( "UploadStart: ", filename.c_str() );
#endif

    // Form SPIFFS filename
    path.clear();
    AsyncWebParameter *parameter = request->getParam(PARAM_PATH);
    if (parameter != NULL) {
      if (!parameter->value().startsWith("/")) {
        path += "/";
      }
      path += parameter->value();
    }
    if (!path.endsWith("/") && !filename.startsWith("/")) {
      path += "/";
    }
    path += filename;

    if (SPIFFS.exists(path)) {
      SPIFFS.remove( path );                             // Remove old file
#ifdef UPLOAD_LOGGING
      debugStr( "removed old file ", path );
#endif
    }
    else {
      fileAdded = true;
    }
    file = SPIFFS.open( path, "w" );                     // Create new file
    if ( !file ) {                                       // file write error
#ifdef UPLOAD_LOGGING
      debugStr( "Error opening file ", path, " for writing" );
#endif
      httpStatus = 500;
      message = httpStatusMessage_FS_ERROR;
    }
#ifdef UPLOAD_LOGGING
    else {
      debugStr( "opened file ", path, " for writing" );
    }
#endif
    totallength = 0;                                 // Total file lengt still zero
    lastindex = 0;                                   // Prepare test
  }

  if ( len ) {                                       // Something to write?
    if ( ( index != lastindex ) || ( index == 0 ) ) { // New chunk?
      if ( httpStatus == 200 ) {                     // No error so far?
        if ( file.write( data, len ) != len ) {        // file write error
          httpStatus = 500;
          message = httpStatusMessage_FS_ERROR;
        }
      }
      totallength += len;
      lastindex = index;
    }
  }

  if ( final ) {
    file.close();
#ifdef UPLOAD_LOGGING
    debugStr( "closed file ", filename );
    debugStr( "UploadEnd: ", filename.c_str() );
#endif

    request->send( httpStatus, mimeType_txt, message );

    const char * filepath = path.c_str();
    if (!checkFilenameAndReboot( filepath ) && fileAdded) {
#ifdef UPLOAD_LOGGING
      debugStr( "RegisterFile ", filepath );
#endif
      server.on( filepath, HTTP_POST, [filepath]( AsyncWebServerRequest *request ) {
        handlePostFile( request, filepath );
      } );
    }
  }
}

#ifdef REQUEST_LOGGING
void PiHttpServer::dumpHeaders(AsyncWebServerRequest *request) {
  debugStr( request->methodToString(), request->url() );

  int headers = request->headers();
  int i;

  //List all collected headers
  for(i=0;i<headers;i++){
    AsyncWebHeader* h = request->getHeader(i);
    debugStr( "HEADER[", h->name(), "]: ", h->value() );
  }

/*
  //get specific header by name
  if(request->hasHeader("MyHeader")){
    AsyncWebHeader* h = request->getHeader("MyHeader");
    debugStr( "MyHeader: ", h->value() );
  }
*/
}

void PiHttpServer::dumpParameters(AsyncWebServerRequest *request) {
  int params = request->params();

  if (params == 0) {
    debugStr( "No params." );
  }
  for(int i=0;i<params;i++){
    AsyncWebParameter* p = request->getParam(i);
    if(p->isFile()){ //p->isPost() is also true
      debugStr( "FILE[", p->name(), "]: ", p->value() );
    }
    else if(p->isPost()){
      debugStr( "POST[", p->name(), "]: ", p->value() );
    }
    else {
      debugStr( "GET[", p->name(), "]: ", p->value() );
    }
  }
}
#endif

void handleDelete( AsyncWebServerRequest *request ) {
  httpServerAccessed();
#ifdef REQUEST_LOGGING
  dumpHeaders(request);
    dumpParameters(request);
#endif
  AsyncWebParameter *parameter = request->getParam("file");
  if ( parameter != NULL ) {
    const char *pathToDelete = parameter->value().c_str();

    if (SPIFFS.exists(pathToDelete)) {
      for (const char *sysFile : system_files) {
        if (strcmp(sysFile, pathToDelete) == 0) {
          request->send_P( 403, mimeType_txt, httpStatusMessage_FILE_NOT_DELETABLE );
          return;
        }
      }

      if (SPIFFS.remove( pathToDelete )) {
        request->send( 200, mimeType_txt, httpStatusMessage_OK );
#ifdef UPLOAD_LOGGING
        debugStr( "Deleted: ", pathToDelete );
#endif
      }
      else {
        request->send( 500, mimeType_txt, httpStatusMessage_FS_ERROR );
      }
    }
    else {
      request->send_P( 404, mimeType_txt, httpStatusMessage_FILE_NOT_FOUND );
    }
  }
  else {
    request->send_P( 400, mimeType_txt, httpStatusMessage_MISSING_PARAMETER_FILE );
  }
}

void printPageTitle( AsyncResponseStream *pStream ) {
  pStream->print( html_h3 );
  for (const char *iconFile : icon_files) {
    if (SPIFFS.exists( iconFile )) {
      pStream->print( picture_html_1 );
      pStream->print( iconFile );
      pStream->print( picture_html_2 );
      pStream->print( html_br );
      break;
    }
  }
  pStream->print(piDeviceName);
}

void printFileListItem( AsyncResponseStream *pStream, const char *path ) {

  bool deletable = true;
  bool skip = false;
  for (const char *iconFile : icon_files) {
    if (strcmp( iconFile, path ) == 0) {
      skip = true; // skip icon in file list, because it's already shown beside header
      deletable = false;
      break;
    }
  }
  const char *suffix = strrchr( path, '.' );
  bool editable = false;
  bool isImage = false;
  if (suffix != NULL) {
    editable = (strcmp( suffix, suffix_xml ) == 0);
    isImage = ( (strcmp( suffix, suffix_jpg ) == 0) || (strcmp( suffix, suffix_png ) == 0) );
  }
  if (deletable) {
    for (const char *sysFile: system_files) {
      if (strcmp( sysFile, path ) == 0) {
        deletable = false;
        break;
      }
    }
  }
  if (strcmp( path, "/fileCfg.xml" ) == 0) {
    // old fileCfg.xml is ignored, but may be deleted - it is now created on the fly
    editable = false;
    deletable = true;
  }

  if (!skip) {
    pStream->print( "<LI>" );
    pStream->print( path );
    //--- download and edit only for XML files
    if (editable) {
      pStream->print( ": " );
      pStream->print( fileDownload_html_1 );
      pStream->print( path );
      pStream->print( fileDownload_html_2 );
      pStream->print( " " );
      pStream->print( fileEdit_html_1 );
      pStream->print( path );
      pStream->print( fileEdit_html_2 );
    }
    //--- only non system files are deletable.
    if (deletable) {
      pStream->print( " " );
      pStream->print( fileDelete_html_1 );
      pStream->print( path );
      pStream->print( fileDelete_html_2 );
    }
    pStream->print( html_br );
    if (isImage) {
      pStream->print( "&nbsp;&nbsp;&nbsp;" );
      pStream->print( picture_html_1 );
      pStream->print( path );
      pStream->print( picture_html_2 );
      pStream->print( html_br );
    }
    pStream->print( "</LI>" );
    pStream->print( newline );
  }
}

int printIndex( AsyncResponseStream *pStream ) {
  int httpStatusCode = 200;

  pStream->print( default_html_1 );
  pStream->print( piDeviceName );
  pStream->print( label_filelist );
  pStream->print( default_html_2 );

  printPageTitle( pStream);
  pStream->print( label_filelist );
  pStream->print( html_h3_end );

  pStream->print( html_h6 );
  pStream->print( piSketchNameAndCompileDate );
  pStream->print( html_h6_end );
  pStream->print( edit_wifi_link_html );
  pStream->print( html_p );
  pStream->print( newline );

  pStream->print( html_hr );
  pStream->print( newline );
  pStream->print( "<UL>" );

  if (SPIFFS.begin()) {

#ifdef ESP32
    File root = SPIFFS.open("/");
    File file = root.openNextFile();
    while (file) {
      const char *filename = file.name();
      printFileListItem( pStream, filename );
      file = root.openNextFile();
    }
#else
    Dir dir = SPIFFS.openDir( "/" );
    while (dir.next()) {
      String filename = dir.fileName();
      printFileListItem( pStream, filename.c_str() );
    }
#endif
  }
  pStream->print( "</UL>" );
  pStream->print( html_hr );
  pStream->print( newline );

  pStream->print( uploadLink_html );

  pStream->print( default_html_Z );
  return httpStatusCode;
}

int printEditWifiForm( AsyncResponseStream *pStream ) {
  int httpStatusCode = 200;

  pStream->print( default_html_1 );
  pStream->print( piDeviceName );
  pStream->print( F(": Edit WiFi") );
  pStream->print( default_html_2 );

  printPageTitle( pStream );
  pStream->print( F(" - Edit WiFi ") );
  pStream->print( html_h3_end );

  WifiCfg* wifiCfg = piNetCfg->getWifiCfg();
  if (wifiCfg == NULL) {
    wifiCfg = (WifiCfg *) piRailFactory->getNewWifiCfg( piNetCfg, piRailFactory->ID_WIFICFG );
    piNetCfg->addChild( piRailFactory->ID_WIFICFG, wifiCfg, NULL );
  }
  pStream->print(formAction);
  pStream->print( editWifi_html_1 );
  const char* ssid = wifiCfg->getSsid();
  if (ssid != NULL) {
    pStream->print( ssid );
  }
  pStream->print( editWifi_html_2 );
  const char* passwd = wifiCfg->getPass();
  if (passwd != NULL) {
    pStream->print( passwd );
  }
  pStream->print( editWifi_html_3 );

  pStream->print( default_html_Z );
  return httpStatusCode;
}

int printEditForm( AsyncResponseStream *pStream ) {
  int httpStatusCode = 200;

  pStream->print( default_html_1 );
  pStream->print( piDeviceName );
  pStream->print( F(": Edit file") );
  pStream->print( default_html_2 );

  printPageTitle( pStream);
  pStream->print( F(" - Edit file ") );
  pStream->print( filenameToEdit );
  pStream->print( html_h3_end );

  pStream->print( formAction );
  pStream->print( filenameToEdit );
  pStream->print( postMultipart );
  pStream->print( editTextfile_html_2 );

  if (SPIFFS.exists( filenameToEdit )) {
    File file = SPIFFS.open( filenameToEdit, "r" );
    if (file) {
      size_t available = file.available();
      while (available > 0) {
        memset( buffer, 0, PI_HTTP_BUFSIZE + 1 );
        file.read( buffer, available < PI_HTTP_BUFSIZE ? available : PI_HTTP_BUFSIZE );
        pStream->print((char *) buffer );
        available = file.available();
      }
      file.close();
    }
  }
  pStream->print( editTextfile_html_3 );

  pStream->print( default_html_Z );
  return httpStatusCode;
}

int printUploadForm(AsyncResponseStream *pStream, const char *path) {
  int httpStatusCode = 200;

  pStream->print( default_html_1 );
  pStream->print( piDeviceName );
  pStream->print( F(": Upload file") );
  pStream->print( default_html_2 );

  printPageTitle( pStream );
  pStream->print( F(" - Upload file ") );
  pStream->print( html_h3_end );

  pStream->print( formAction );
  pStream->print( uploadForm_html_1 );
  if (path != NULL && strlen(path) > 0) {
    pStream->print( F("?path=") );
    pStream->print( path );
  }
  pStream->print( postMultipart );
  pStream->print( uploadForm_html_2 );

  pStream->print( default_html_Z );
  return httpStatusCode;
}

void handleEdit( AsyncWebServerRequest *request ) {
  httpServerAccessed();
#ifdef REQUEST_LOGGING
  dumpHeaders(request);
    dumpParameters(request);
#endif
  AsyncWebParameter *parameter = request->getParam("file");
  if ( parameter != NULL ) {
    const char *path = parameter->value().c_str();
    if (SPIFFS.exists(path)) {
      strncpy( filenameToEdit, path, PI_HTTP_FILENAME_LENGTH);
      filenameToEdit[PI_HTTP_FILENAME_LENGTH] = 0;

      AsyncResponseStream *pStream = request->beginResponseStream(mimeType_html);
      int httpStatusCode = printEditForm( pStream );
      pStream->setCode(httpStatusCode);
      request->send(pStream);
    }
    else {
      request->send_P( 404, mimeType_txt, httpStatusMessage_FILE_NOT_FOUND );
    }
  }
  else {
    request->send_P( 400, mimeType_txt, httpStatusMessage_MISSING_PARAMETER_FILE );
  }
}

void handleEditWifi( AsyncWebServerRequest *request ) {
  httpServerAccessed();
#ifdef REQUEST_LOGGING
  dumpHeaders(request);
    dumpParameters(request);
#endif
  const char *path = "/netCfg.xml";
  if (SPIFFS.exists(path)) {
    strncpy( filenameToEdit, path, PI_HTTP_FILENAME_LENGTH);
    filenameToEdit[PI_HTTP_FILENAME_LENGTH] = 0;

    AsyncResponseStream *pStream = request->beginResponseStream( mimeType_html );
    int httpStatusCode = printEditWifiForm( pStream );
    pStream->setCode( httpStatusCode );
    request->send( pStream );
  }
  else {
    request->send_P( 404, mimeType_txt, httpStatusMessage_FILE_NOT_FOUND );
  }
}

void PiHttpServer::init( const char *sketchNameAndCompileDate, const char *deviceName, NetCfg* netCfg ) {
  piSketchNameAndCompileDate = sketchNameAndCompileDate;
  piDeviceName = deviceName;
  if (netCfg == NULL) {
    netCfg = new NetCfg();
  }
  piNetCfg = netCfg;
  _xmlWriter = new XmlWriter( 300 );
}

void PiHttpServer::begin() {
  server.on( "/", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->redirect( "/index.html" );
  });

  server.on( INDEX_HTML, HTTP_GET, [](AsyncWebServerRequest *request) {
    httpServerAccessed();
#ifdef REQUEST_LOGGING
    dumpHeaders(request);
    dumpParameters(request);
#endif

    AsyncResponseStream *pStream = request->beginResponseStream(mimeType_html);
    int httpStatusCode = printIndex( pStream);
    pStream->setCode( httpStatusCode);
    request->send(pStream);
  });

  server.on( FILECFG_XML, HTTP_GET, [](AsyncWebServerRequest *request) {
    handleGetFileCfg( request );
  });

  if (SPIFFS.begin()) {
#ifdef ESP32
    File root = SPIFFS.open("/");
    File file = root.openNextFile();
    while(file) {
      const char *path = file.name();
#else
    Dir dir = SPIFFS.openDir( "/" );
    while (dir.next()) {
      String pathStr = dir.fileName();
      const char *path = pathStr.c_str();
#endif
      int len = strlen(path);
      char* filepath = new char[len+1];
      strcpy( filepath, path );
      filepath[len] = 0;
      // ignore old fileCfg.xml - is now created on the fly
      if (strcmp( filepath, "/fileCfg.xml" ) != 0) {
#ifdef UPLOAD_LOGGING
        debugStr( "RegisterFile ", filepath );
#endif
        server.on( filepath, HTTP_POST, [filepath]( AsyncWebServerRequest *request ) {
          handlePostFile( request, filepath );
        } );
      }
#ifdef ESP32
      file = root.openNextFile();
#endif
    }
  }

  server.on( EDITWIFI_HTML, HTTP_GET, [](AsyncWebServerRequest *request) {
    handleEditWifi( request );
  });

  server.on( EDIT_HTML, HTTP_GET, [](AsyncWebServerRequest *request) {
    handleEdit( request );
  });

  server.on( DELETE_HTML, HTTP_GET, [](AsyncWebServerRequest *request) {
    handleDelete( request );
  });

  server.on( UPLOAD_HTML, HTTP_GET, [](AsyncWebServerRequest *request) {
    httpServerAccessed();
#ifdef REQUEST_LOGGING
    dumpHeaders(request);
    dumpParameters(request);
#endif

    AsyncWebParameter *parameter = request->getParam(PARAM_PATH);
    AsyncResponseStream *pStream = request->beginResponseStream(mimeType_html);
    int httpStatusCode = printUploadForm(pStream, parameter != NULL ? parameter->value().c_str() : NULL);
    pStream->setCode(httpStatusCode);
    request->send(pStream);
  });

  server.on( SETWIFI, HTTP_POST, [](AsyncWebServerRequest *request) {
    handleSetWifi( request );
  } );

  server.on( FILE_UPLOAD, HTTP_POST, [](AsyncWebServerRequest *request) {}, handleFileUpload );

  server.onNotFound(onUnhandledRequest);

  delay(100);

  server.begin();
}

void PiHttpServer::onUnhandledRequest(AsyncWebServerRequest *request) {
  httpServerAccessed();
#ifdef REQUEST_LOGGING
  dumpHeaders(request);
  dumpParameters(request);
#endif
  const String &path = request->url();

  if ((strcmp("GET", request->methodToString()) == 0) && SPIFFS.exists( path )) {
    const char *contentType = mimeType_other;
    const char *suffix = strrchr( path.c_str(), '.' );
    if (suffix) {
      if ( strcmp( suffix, suffix_txt ) == 0 ) {
        contentType = mimeType_txt;
      }
      if ( strcmp( suffix, suffix_xml ) == 0) {
        contentType = mimeType_xml;
      }
      if ( strcmp( suffix, suffix_jpg ) == 0) {
        contentType = mimeType_jpg;
      }
      if ( strcmp( suffix, suffix_png ) == 0) {
        contentType = mimeType_png;
      }
    }
#ifdef REQUEST_LOGGING
    debugStr( "Sending SPIFFS file ", path );
#endif
    request->send( SPIFFS, path, contentType );
  }
  else {
    request->send_P( 404, mimeType_txt, httpStatusMessage_FILE_NOT_FOUND );
  }
}
