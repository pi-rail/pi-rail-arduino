/*
 * This file is part of PI-Rail-Arduino (https://gitlab.com/pi-rail/pi-rail-arduino).
 * Copyright (c) 2013-2020 PI-Data AG, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SKETCHBOOK_PIOTA_H
#define SKETCHBOOK_PIOTA_H

#include <ArduinoOTA.h>

// to enable, change NO_OTA_LOGGING to OTA_LOGGING
#define NO_OTA_LOGGING

class PiOTA {

public:

  static void startOTA();

  // shows the progress in percent
  static void progressOTA( unsigned int progress, unsigned int total );

  // just show message "End"
  static void endOTA();

  // if an error occurred, shows especificaly the error type
  static void errorOTA( ota_error_t error );

  // initialize all handlers
  void begin( const char *hostname );
};

#endif //SKETCHBOOK_PIOTA_H
