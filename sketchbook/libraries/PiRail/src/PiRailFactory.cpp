#include "PiRailFactory.h"
#include "Cfg.h"
#include "Cmd.h"
#include "Csv.h"
#include "Data.h"
#include "ExtCfg.h"
#include "FileCfg.h"
#include "FileDef.h"
#include "InCfg.h"
#include "IoCfg.h"
#include "LockCmd.h"
#include "MoCmd.h"
#include "MotorAction.h"
#include "MotorMode.h"
#include "MotorState.h"
#include "NetCfg.h"
#include "OutCfg.h"
#include "Param.h"
#include "PortCfg.h"
#include "PortPin.h"
#include "SecurityCfg.h"
#include "State.h"
#include "SyncCmd.h"
#include "TrackMsg.h"
#include "WifiCfg.h"

PiRailFactory instancePiRailFactory;

PiRailFactory* PiRailFactory::getInstance() {
 return &(instancePiRailFactory);
}

int PiRailFactory::getVersion() {
 return 4;
}
//----------------------------------------------------------
// Manual extensions

extern void printHeap( const __FlashStringHelper* msg );

ActionState* PiRailFactory::getNewActionState( Model* forParent, const QName* forRelation ) {
  ActionState* result = (ActionState*) findFree( forRelation );
  if (result == NULL) {
    if (debug_heap) printHeap( msg_create_new, ID_ACTIONSTATE->getName() );
    result = new ActionState();
  }
  else {
    forParent->removeChild( result );
  }
  result->setId( NULL );
  result->setCur( '-' );
  result->setTgt( '-' );
  result->setCurI( 0 );
  result->setTgtI( 0 );
  result->setUpd( 0 );
  result->setSrcID( NULL );
  result->setLck( NULL );
  return result;
}

CallCmd* PiRailFactory::getNewCallCmd( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_CALLCMD->getName() );
  return new CallCmd();
}

Cfg* PiRailFactory::getNewCfg( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_CFG->getName() );
  return new Cfg();
}

Cmd* PiRailFactory::getNewCmd( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_CMD->getName() );
  return new Cmd();
}

Csv* PiRailFactory::getNewCsv( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_CSV->getName() );
  return new Csv();
}

Data* PiRailFactory::getNewData( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_DATA->getName() );
  Data* data = new Data();
  data->setId( NULL );
  return data;
}

DelayCmd* PiRailFactory::getNewDelayCmd( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_DELAYCMD->getName() );
  return new DelayCmd();
}

EnumAction* PiRailFactory::getNewEnumAction( Model *forParent, const QName *forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_ENUMACTION->getName() );
  return new EnumAction();
}

ExtCfg* PiRailFactory::getNewExtCfg( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_EXTCFG->getName() );
  return new ExtCfg();
}

FileCfg* PiRailFactory::getNewFileCfg( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_FILECFG->getName() );
  return new FileCfg();
}

FileDef* PiRailFactory::getNewFileDef( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_FILEDEF->getName() );
  return new FileDef();
}

IfCmd* PiRailFactory::getNewIfCmd( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_IFCMD->getName() );
  return new IfCmd();
}

InCfg* PiRailFactory::getNewInCfg( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_INCFG->getName() );
  return new InCfg();
}

InPin* PiRailFactory::getNewInPin( Model *forParent, const QName *forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_INPIN->getName() );
  return new InPin();
}

IoCfg* PiRailFactory::getNewIoCfg( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_IOCFG->getName() );
  return new IoCfg();
}

ItemConn* PiRailFactory::getNewItemConn( Model *forParent, const QName *forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_ITEMCONN->getName() );
  return new ItemConn();
}

Ln* PiRailFactory::getNewLn( Model* forParent, const QName* forRelation ) {
  Ln* result = (Ln*) findFree( forRelation );
  if (result == NULL) {
    if (debug_heap) printHeap( msg_create_new, ID_LN->getName() );
    result = new Ln();
  }
  else {
    forParent->removeChild( result );
  }
  return result;
}

LockCmd* PiRailFactory::getNewLockCmd( Model* forParent, const QName* forRelation ) {
  Cmd* cmd = (Cmd*) forParent;
  LockCmd* result = (LockCmd*) findFree( forRelation );
  if (result == NULL) {
    if (debug_heap) printHeap( msg_create_new, ID_LOCKCMD->getName() );
    result = new LockCmd();
  }
  else {
    cmd->removeChild( result );
  }
  return result;
}

MotorAction* PiRailFactory::getNewMotorAction( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_MOTORACTION->getName() );
  return new MotorAction();
}

MoCmd* PiRailFactory::getNewMoCmd( Model* forParent, const QName* forRelation ) {
  MoCmd* result = (MoCmd*) findFree( forRelation );
  if (result == NULL) {
    if (debug_heap) printHeap( msg_create_new, ID_MOCMD->getName() );
    result = new MoCmd();
  }
  else {
    forParent->removeChild( result );
  }
  return result;
}

MotorMode* PiRailFactory::getNewMotorMode( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_MOTORMODE->getName() );
  return new MotorMode();
}

MotorState* PiRailFactory::getNewMotorState( Model* forParent, const QName* forRelation ) {
  MotorState* result = (MotorState*) findFree( forRelation );
  if (result == NULL) {
    if (debug_heap) printHeap( msg_create_new, ID_MOTORSTATE->getName() );
    result = new MotorState();
  }
  else {
    forParent->removeChild( result );
  }
  return result;
}

MsgState* PiRailFactory::getNewMsgState( Model* forParent, const QName* forRelation ) {
  MsgState* result = (MsgState*) findFree( forRelation );
  if (result == NULL) {
    if (debug_heap) printHeap( msg_create_new, ID_MSGSTATE->getName() );
    return new MsgState();
  }
  else {
    forParent->removeChild( result );
  }
  return result;
}

NetCfg* PiRailFactory::getNewNetCfg( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_NETCFG->getName() );
  return new NetCfg();
}

OutCfg* PiRailFactory::getNewOutCfg( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_OUTCFG->getName() );
  return new OutCfg();
}

OutPin* PiRailFactory::getNewOutPin( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_OUTPIN->getName() );
  return new OutPin();
}

Param* PiRailFactory::getNewParam( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_PARAM->getName() );
  return new Param();
}

Point* PiRailFactory::getNewPoint( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_POINT->getName() );
  return new Point();
}

Port* PiRailFactory::getNewPort( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_PORT->getName() );
  return new Port();
}

PortCfg* PiRailFactory::getNewPortCfg( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_PORTCFG->getName() );
  return new PortCfg();
}

PortPin* PiRailFactory::getNewPortPin( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_PORTPIN->getName() );
  return new PortPin();
}

RangeAction* PiRailFactory::getNewRangeAction( Model *forParent, const QName *forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_RANGEACTION->getName() );
  return new RangeAction();
}

SecurityCfg* PiRailFactory::getNewSecurityCfg( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_SECURITYCFG->getName() );
  return new SecurityCfg();
}

SensorAction* PiRailFactory::getNewSensorAction( Model *forParent, const QName *forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_SENSORACTION->getName() );
  return new SensorAction();
}

SetCmd* PiRailFactory::getNewSetCmd( Model* forParent, const QName* forRelation ) {
  Cmd* cmd = (Cmd*) forParent;
  SetCmd* result = (SetCmd*) findFree( forRelation );
  if (result == NULL) {
    if (debug_heap) printHeap( msg_create_new, ID_SETCMD->getName() );
    result = new SetCmd();
  }
  else {
    cmd->removeChild( result );
  }
  return result;
}

SetIO* PiRailFactory::getNewSetIO( Model *forParent, const QName *forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_SETIO->getName() );
  return new SetIO();
}

State* PiRailFactory::getNewState( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_STATE->getName() );
  return new State();
}

StateScript* PiRailFactory::getNewStateScript( Model *forParent, const QName *forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_STATESCRIPT->getName() );
  return new StateScript();
}

SyncCmd* PiRailFactory::getNewSyncCmd( Model* forParent, const QName* forRelation ) {
  Cmd* cmd = (Cmd*) forParent;
  SyncCmd* result = (SyncCmd*) findFree( forRelation );
  if (result == NULL) {
    if (debug_heap) printHeap( msg_create_new, ID_SYNCCMD->getName() );
    result = new SyncCmd();
  }
  else {
    cmd->removeChild( result );
  }
  return result;
}

TimerAction* PiRailFactory::getNewTimerAction( Model *forParent, const QName *forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_TIMERACTION->getName() );
  return new TimerAction();
}

TrackMsg* PiRailFactory::getNewTrackMsg( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_TRACKMSG->getName() );
  return new TrackMsg();
}

TrackPos* PiRailFactory::getNewTrackPos( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_TRACKPOS->getName() );
  return new TrackPos();
}

TriggerAction* PiRailFactory::getNewTriggerAction( Model *forParent, const QName *forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_TRIGGERACTION->getName() );
  return new TriggerAction();
}

WifiCfg* PiRailFactory::getNewWifiCfg( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( msg_create_new, ID_WIFICFG->getName() );
  return new WifiCfg();
}

//-------------------------------------------------------------------------------------

void PiRailFactory::addFree( Model* model ) {
  model->interalSetNextModel( this->firstFree );
  this->firstFree = model;
}

Model* PiRailFactory::findFree( const QName* relationName ) {
  Model* model = this->firstFree;
  Model* prev = NULL;
  while (model != NULL) {
    if (model->parentRelation() == relationName) {
      if (prev == NULL) {
        this->firstFree = model->nextSibling( NULL );
      }
      else {
        prev->interalSetNextModel( model->nextSibling(NULL ));
      }
      return model;
    }
    prev = model;
    model = model->nextSibling( NULL );
  }
  return NULL;
}

void PiRailFactory::clearCmd( Cmd* cmd ) {
  Model* model = cmd->firstChild( NULL );
  while (model != NULL) {
    Model* next = model->nextSibling( NULL );
    cmd->removeChild( model );
    addFree( model );
    model = next;
  }
}

void PiRailFactory::clearState( State* state ) {
  Model* model = state->firstChild( NULL );
  while (model != NULL) {
    Model* next = model->nextSibling( NULL );
    state->removeChild( model );
    addFree( model );
    model = next;
  }
}
