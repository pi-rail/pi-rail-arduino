// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

#include  "PortCfg.h"

extern void fatalError( const __FlashStringHelper* msg );

PortCfg::PortCfg() {
}

const QName* PortCfg::typeID() {
  return piRailFactory->ID_PORTCFG;
}

void PortCfg::parseAttr( XmlReader* xmlReader ) {
  xmlReader->getQNameAttr( piRailFactory->ID_ID, (const QName**) &(this->id) );
  xmlReader->getEnumAttr( piRailFactory->ID_TYPE, (char **) piRailFactory->portType_names, 8, (int *) &(this->type) );
}

void PortCfg::writeAttr( XmlWriter* xmlWriter ) {
  xmlWriter->attribute( piRailFactory->ID_ID, this->id );
  xmlWriter->attribute( piRailFactory->ID_TYPE,(char *) piRailFactory->portType_names[this->type] );
}

Model* PortCfg::createChild( const QName* tag ) {
  if (tag == piRailFactory->ID_PARAM) {
    return piRailFactory->getNewParam( this, tag );
  }
  else if (tag == piRailFactory->ID_PORTPIN) {
    return piRailFactory->getNewPortPin( this, tag );
  }
  else {
    return Model::createChild( tag );
  }
}

const QName* PortCfg::getId() {
  return this->id;
}

void PortCfg::setId( const QName* id ) {
  this->id = id;
}

portType_t PortCfg::getType() {
  return this->type;
}

void PortCfg::setType( portType_t type ) {
  this->type = type;
}

//----------------------------------------------------------
// Manual extensions

PortPin* PortCfg::getPortPinByType( portPinType_t portPinType, int index ) {
  int i = 0;
  PortPin* portPin = (PortPin*) this->firstChild( piRailFactory->ID_PORTPIN );
  while (portPin != NULL) {
    if (portPin->getType() == portPinType) {
      if (i == index) {
        return portPin;
      }
      i++;
    }
    portPin = (PortPin*) portPin->nextSibling( piRailFactory->ID_PORTPIN );
  }
  return NULL;
}

int PortCfg::getPinByType( portPinType_t portPinType, int index ) {
  PortPin* portPin = getPortPinByType( portPinType, index );
  if (portPin == NULL) {
    return -1;
  }
  else {
    return portPin->getPin();
  }
}

const char PortCfg::getParamValue( const QName* paramId ) {
  Param* param = (Param*) this->firstChild( piRailFactory->ID_PARAM );
  while (param != NULL) {
    if (param->getId() == paramId) {
      return param->getValue();
    }
    param = (Param*) param->nextSibling( piRailFactory->ID_PARAM );
  }
  return 0;
}

int PortCfg::getParamIntVal( const QName* paramId ) {
  Param* param = (Param*) this->firstChild( piRailFactory->ID_PARAM );
  while (param != NULL) {
    if (param->getId() == paramId) {
      return param->getIntVal();
    }
    param = (Param*) param->nextSibling( piRailFactory->ID_PARAM );
  }
  return 0;
}

ExtCfg* PortCfg::getExtCfg() {
  Model* parent = this->parent();
  if (parent != NULL) {
    if (parent->typeID() == piRailFactory->ID_EXTCFG) {
      return (ExtCfg*) parent;
    }
  }
  return NULL;
}


