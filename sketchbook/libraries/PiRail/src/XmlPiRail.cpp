/*
 * This file is part of PI-Rail-Arduino (https://gitlab.com/pi-rail/pi-rail-arduino).
 * Copyright (c) 2013-2020 PI-Data AG, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include "Arduino.h"
#include "FS.h"
#include "XmlPiRail.h"
#include "Cfg.h"

#ifdef ESP32
#include <SPIFFS.h>
#endif

//MD5_CTX ctx;

const PROGMEM char *hexChars = "0123456789abcdef";
const PROGMEM char *Msg_outCfgNotFound  = " -- outCfg not found";
const PROGMEM char *Msg_chipCfgNotFound  = " -- chipCfg not found";

XmlPiRail::XmlPiRail( const char* wifiModuleType, int xmlReadBufferSize, int xmlWriteBufferSize ) {
  this->xmlBufferSize = xmlReadBufferSize;
  this->_xmlBuffer = new char[xmlReadBufferSize+1];
  this->_wifiModuleType = strrchr( wifiModuleType, '/' );
  if (this->_wifiModuleType != NULL) {
    (this->_wifiModuleType)++;
  }
  else {
    this->_wifiModuleType = wifiModuleType;
  }
  this->_xmlReader = new XmlReader( xmlReadBufferSize );
  this->_xmlWriter = new XmlWriter( xmlWriteBufferSize );
  resetXmlBuffer();
}

void XmlPiRail::begin( char* udpOut ) {
  _udpOut = udpOut;
  debugStr( "UDP connected" );
}

void XmlPiRail::resetXmlBuffer() {
  _receivePos = -1;
  memset( _xmlBuffer, 0, this->xmlBufferSize + 1 );
}

char* XmlPiRail::getXmlBuffer() {
  return _xmlBuffer;
}

bool XmlPiRail::isErrorMode() {
  return errorMode;
}

void XmlPiRail::setErrorMode( bool errorMode ) {
  this->errorMode = errorMode;
}

uint8_t XmlPiRail::calcCRC( char* data, int len ) {
  uint8_t crc = 0xff;
  size_t i, j;
  for (i = 0; i < len; i++) {
    crc ^= data[i];
    for (j = 0; j < 8; j++) {
      if ((crc & 0x80) != 0) {
        crc = (uint8_t)((crc << 1) ^ 0x31 );
      }
      else {
        crc <<= 1;
      }
    }
  }
  crc &= 0x7F;
  if (crc < 32) {
    crc += 32;
  }
  if (crc == 127) {
    crc = 126;
  }
  return crc;
}

void XmlPiRail::loadConfigFiles() {
  if (errorMode) {
    return;
  }
  const char* read = "read ";
  const char* missing = " missing";
  const char* invalid = " invalid";
  const char* ignoring = " - ignoring";
  printHeap( read, "config..." );

  //--- Read netCfg.xml
  const char* netCfgPath = "/netCfg.xml";
  Model* model = loadConfigToXmlBuffer( netCfgPath );
  if (model == NULL) {
    fatalError( netCfgPath, missing );
    return;
  }
  if (model->typeID() == piRailFactory->ID_NETCFG) {
    this->netCfg = (NetCfg*) model;
    this->netCfgOld = NULL;
    printHeap( read, netCfgPath );
  }
  else if (model->typeID() == piRailFactory->ID_NETCFG_OLD){
    this->netCfg = NULL;
    this->netCfgOld = (NetCfgOld*) model;
    printHeap( read, piRailFactory->ID_NETCFG_OLD->getName() );
  }
  else {
    fatalError( netCfgPath, invalid );
    return;
  }

  //--- Read ioCfg.xml
  const char* ioCfgPath = "/ioCfg.xml";
  model = loadConfigToXmlBuffer( ioCfgPath );
  if (model == NULL) {
    fatalError( ioCfgPath, missing );
    return;
  }
  if (model->typeID() != piRailFactory->ID_IOCFG) {
    fatalError( ioCfgPath, invalid );
    return;
  }
  this->ioCfg = (IoCfg*) model;
  printHeap( read, ioCfgPath );

  //--- Read cfg.xml
  const char* cfgPath = "/cfg.xml";
  model = loadConfigToXmlBuffer( cfgPath );
  if (model == NULL) {
    debugStr( cfgPath, missing, ignoring );
    Cfg* newCfg = piRailFactory->getNewCfg( NULL, piRailFactory->ID_CFG );
    newCfg->setId( piRailFactory->ID_EMPTY_CFG );
    model = newCfg;
  }
  if (model->typeID() == piRailFactory->ID_CFG) {
    this->cfg = (Cfg *) model;
  }
  else {
    debugStr( cfgPath, invalid, ignoring );
    this->cfg = piRailFactory->getNewCfg( NULL, piRailFactory->ID_CFG );
    this->cfg->setId( piRailFactory->ID_EMPTY_CFG );
  }
  this->cfg->setIoCfg( this->ioCfg );
  printHeap( read, cfgPath );
}

void XmlPiRail::initActions() {
  debugStr( "Init Actions" );
  Model* model = this->cfg->firstChild( NULL );
  while (model != NULL) {
    if (this->cfg->isAction( model )) {
      Action *action = (Action *) model;
      debugIDVal( "--", action->getId() );
      action->initState( this->deviceState, this->cfg, this->ioCfg );
    }
    model = model->nextSibling( NULL );
  }
}

char* softwareVersion() {
  char* result = new char[11];
  char s_month[5];
  int month, day, year;
  static const char month_names[] = "JanFebMarAprMayJunJulAugSepOctNovDec";
  sscanf(__DATE__, "%s %d %d", s_month, &day, &year);
  month = 1 + (strstr(month_names, s_month) - month_names) / 3;
  result[0] = '0' + (year / 1000);
  year = year % 1000;
  result[1] = '0' + (year / 100);
  year = year % 100;
  result[2] = '0' + (year / 10);
  result[3] = '0' +  year % 10;
  result[4] = '0' + (month / 10);
  result[5] = '0' +  month % 10;
  result[6] = '0' + (day / 10);
  result[7] = '0' +  day % 10;
  result[8] = 0;
  return result;
}

char* createMacID( const char* macAddress ) {
  char* result = new char[19];
  for (int i = 0; i < 19; i++) {
   if (macAddress[i] == ':') {
     result[i] = '-';
   }
   else {
     result[i] = macAddress[i];
   }
  }
  return result;
}

void XmlPiRail::setup( const char* macAddress, deviceType_t deviceType ) {
  char* version = softwareVersion();
  char* deviceID = createMacID( macAddress );
  int xsdVersion = PiRailFactory::getVersion();
  debugStr( "Sketch=", this->_wifiModuleType, ", DeviceID (MAC)=", deviceID );
  debugStr( "  SW-Version=", version );
  debugVal( ", XSD-Version=", xsdVersion );
  this->deviceState = piRailFactory->getNewState( NULL, piRailFactory->ID_STATE );
  this->deviceState->setType( deviceType );
  this->deviceState->setFw( (char *) this->_wifiModuleType );
  this->deviceState->setVer( version );
  this->deviceState->setXsd( xsdVersion );
  this->deviceState->setId( piRailFactory->NS_PIRAILFACTORY->getQName( deviceID ) );
  loadConfigFiles();
  Ln* ln = piRailFactory->getNewLn( deviceState, piRailFactory->ID_LN );
  ln->setMs( 0 );
  ln->setSp( 0 );
  ln->setDist( 0 );
  ln->setClk( DIR_UNKNOWN );
  ln->setSx( 0 );
  deviceState->addChild( piRailFactory->ID_LN, ln, deviceState->lastChild( NULL ) );
  processSchedule( NULL, 0 ); // clear schedule list
  if (errorMode) {
    return;
  }

  //--- init tower state ---> komplett in XmlPIRail
  this->deviceState->setCfg( cfg->getVersion() );

  //--- Initialize actions
  initActions();

  debugStr( "finished init" );
}

const char* XmlPiRail::getDeviceID() {
  if (netCfg != NULL) {
    return netCfg->getId();
  }
  else if (netCfgOld != NULL) {
    return netCfgOld->getId();
  }
  return NULL;
}

WifiCfg* XmlPiRail::getWifiCfg() {
  if (netCfg != NULL) {
    return netCfg->getWifiCfg();
  }
  else if (netCfgOld != NULL) {
    WifiCfgOld *wifiCfgOld = netCfgOld->getWifiCfg();
    if (wifiCfgOld != NULL) {
      WifiCfg* wifiCfg = new WifiCfg();
      wifiCfg->setSsid( (char*) wifiCfgOld->getSsid() );
      wifiCfg->setPass( (char*) wifiCfgOld->getPass() );
      return wifiCfg;
    }
  }
  return NULL;
}

NetCfg *XmlPiRail::getNetCfg() {
  return netCfg;
}

IoCfg *XmlPiRail::getIoCfg() {
  return ioCfg;
}

Cfg *XmlPiRail::getCfg() {
  return cfg;
}

int XmlPiRail::getXmlBufferSize() {
  return xmlBufferSize;
}

Model* XmlPiRail::loadConfigToXmlBuffer( const char *path ) {
  const char * strFile = "File";
  if (SPIFFS.begin()) {
#ifdef VERBOSE_DEBUG
    debugStr( "SPIFFS.begin() success." );
#endif
    if (!SPIFFS.exists(path)) {
      debugStr( strFile, path, " not found" );
      return NULL;
    }
#ifdef VERBOSE_DEBUG
    debugStr( strFile, " exists. :-)" );
#endif
    File file = SPIFFS.open(path, "r");
    if (!file) {
      debugStr( strFile,path, " can not be opened" );
      return NULL;
    }
    memset( _xmlBuffer, 0, this->xmlBufferSize+1 );
    Model* rootModel = parse( &file );
    file.close();

//    openwall_MD5::MD5_Update( &ctx, _xmlBuffer, available );

    // keep open for HTTP, so do not: SPIFFS.end();
    return rootModel;
  }
  else {
    debugStr( "SPIFFS.begin() failed!" );
    return NULL;
  }
}

Model* XmlPiRail::parse( Stream* inputStream ) {
  _xmlReader->initParser( _xmlBuffer, xmlBufferSize, inputStream );
  const QName* tag = _xmlReader->nextTag( NULL );
  Model* rootModel = NULL;
  if (tag == piRailFactory->ID_NETCFG) {
    rootModel = piRailFactory->getNewNetCfg( NULL, piRailFactory->ID_NETCFG );
  }
  else if (tag == piRailFactory->ID_NETCFG_OLD) { // old namespace
    rootModel = new NetCfgOld();
  }
  else if (tag == piRailFactory->ID_FILECFG) {
    rootModel = piRailFactory->getNewFileCfg( NULL, piRailFactory->ID_FILECFG );
  }
  else if (tag == piRailFactory->ID_IOCFG) {
    rootModel = piRailFactory->getNewIoCfg( NULL, piRailFactory->ID_IOCFG );
  }
  else if (tag == piRailFactory->ID_CFG) {
    rootModel = piRailFactory->getNewCfg( NULL, piRailFactory->ID_CFG );
  }
  if (rootModel != NULL) {
    rootModel->parse( _xmlReader, tag );
  }
  resetXmlBuffer();
  return rootModel;
}

 /*
  We expect every message to arrive as one continuous stream,
  i.e. sent as single UDP message.
*/
void XmlPiRail::received( char val ) {
  if (_receivePos < 0) {
    if (val == '<') { // start of message
      _xmlBuffer[0] = val;
      _xmlBuffer[1] = 0;
      _receivePos = 1;
    }
  }
  else if (_receivePos >= this->xmlBufferSize) {
    // Message exceeds max size --> skip
    _receivePos = -1;
  }
  else {
    _xmlBuffer[_receivePos++] = val;
    _xmlBuffer[_receivePos+1] = 0;
  }
}

State* XmlPiRail::getDeviceState() {
  return this->deviceState;
}

void XmlPiRail::sendDeviceState( unsigned long currentTimeMillis, int signalDB, const char* apMAC  ) {
  if (this->deviceState != NULL) {
    long deltaTime = (long) currentTimeMillis - this->deviceState->getMs();
    if (abs( deltaTime ) < 150) {
      // Avoid too many messages
      return;
    }
    this->deviceState->setMs((long) currentTimeMillis );
    this->deviceState->setHeap( getHeapSize());
    this->deviceState->setNum( this->stateMsgNum++ );
    this->deviceState->setDB( signalDB );
    this->deviceState->setApMAC((char *) apMAC );

    _xmlWriter->begin( piRailFactory->NS_PIRAILFACTORY );
    _xmlWriter->startTag( piRailFactory->ID_STATE );
    this->deviceState->writeAttr( _xmlWriter );
    Model *child = this->deviceState->firstChild( NULL );
    if (child == NULL) {
      _xmlWriter->endAttributes( true );
    }
    else {
      _xmlWriter->endAttributes( false );
    }
    while (child != NULL) {
      child->writeXml( child->parentRelation(), _xmlWriter );
      child = child->nextSibling( NULL );
      if (child != NULL) {
        if (_xmlWriter->getBufPos() > 1200) {
          _xmlWriter->endTag( piRailFactory->ID_STATE );
          const char *udpMsg = _xmlWriter->getXml();
          sendUdpMessage( udpMsg );


          //--- Start next message
          _xmlWriter->begin( piRailFactory->NS_PIRAILFACTORY );
          _xmlWriter->startTag( piRailFactory->ID_STATE );
          this->deviceState->writeAttr( _xmlWriter );
          _xmlWriter->endAttributes( false );
        }
      }
    }
    _xmlWriter->endTag( piRailFactory->ID_STATE );
    const char *udpMsg = _xmlWriter->getXml();
    sendUdpMessage( udpMsg );

    //--- Send optional sensor data
    SensorAction *sensorAction = (SensorAction *) cfg->firstChild( piRailFactory->ID_SENSOR );
    while (sensorAction != NULL) {
      Data *data = sensorAction->data;
      if (data != NULL) {
        sendUdpMessage( writeData( data ) );
      }
      sensorAction = (SensorAction *) sensorAction->nextSibling( piRailFactory->ID_SENSOR );
    }
  }
}

const char * XmlPiRail::writeData( Data* data ) {
  if (data != NULL) {
    _xmlWriter->begin( piRailFactory->NS_PIRAILFACTORY );
    data->writeXml( piRailFactory->ID_DATA, this->_xmlWriter );
    return this->_xmlWriter->getXml();
  }
  else {
    return "Data is null";
  }
}

//------------------------------------------------------------------
// <editor-fold description="Command Execution (doXXX, processXXX)">
//------------------------------------------------------------------

long XmlPiRail::getNextSyncTime() {
  return this->nextSyncTime;
}

void XmlPiRail::setNextSyncTime( long nextSyncTime ) {
 this->nextSyncTime = nextSyncTime;
}

void XmlPiRail::processState( unsigned int myIP, unsigned int fromIP, State* deviceState ) {
  if (deviceState != NULL) {
    const QName *fromID = deviceState->getId();
    const QName *myID = this->deviceState->getId();
    Model *stateChild = (ActionState *) deviceState->firstChild( NULL );
    while (stateChild != NULL) {
      const QName *typeID = stateChild->typeID();
      if ((typeID == piRailFactory->ID_ACTIONSTATE) || (typeID == piRailFactory->ID_MOTORSTATE)) {
        ActionState *stateEntry = (ActionState *) stateChild;
        TriggerAction *triggerAction = (TriggerAction *) this->cfg->firstChild( piRailFactory->ID_TRIGGER );
        while (triggerAction != NULL) {
          const QName *onDevice = triggerAction->getOnDevice();
          if (((onDevice == NULL) && (fromID == myID)) || (onDevice == fromID)) {
            const QName *onAction = triggerAction->getOnAction();
            if ((onAction == NULL) || (onAction == stateEntry->getId())) {
              const QName *onSrcID = triggerAction->getOnSrcID();
              if ((onSrcID == NULL) || (onSrcID == stateEntry->getSrcID())) {
                triggerAction->setValue( stateEntry->getCur(), stateEntry->getCurI(), cfg, ioCfg, NULL );
              }
            }
          }
          triggerAction = (TriggerAction *) triggerAction->nextSibling( piRailFactory->ID_TRIGGER );
        }
      }
      stateChild = stateChild->nextSibling( NULL );
    }
  }
}

bool matchesPosID( const QName* posId1, const QName* posId2 ) {
  if ((posId1 == NULL) || (posId2 == NULL)) {
    return false;
  }
  const char * posId1Str = posId1->getName();
  const char * posId2Str = posId2->getName();
  int count = strlen( posId1Str );
  int posId2Len = strlen( posId2->getName() );
  if (posId2Len < count) {
    count = posId2Len;
  }
  for (int i = 0; i < count; i++) {
    if (posId1Str[i] != posId2Str[i]) {
      return false;
    }
  }
  return true;
}

char getCmdDir( char cmd ) {
  if ((cmd >= 'A') && (cmd <= 'Z')) {
    return DIR_FORWARD;
  }
  else if ((cmd >= 'a') && (cmd <= 'z')) {
    return DIR_BACK;
  }
  else {
    return DIR_UNKNOWN;
  }
}

MsgState* XmlPiRail::getSchedule4Pos( const QName* posID ) {
  if (this->deviceState == NULL) {
    return NULL;
  }
  Ln* ln = this->deviceState->getLn();
  if (ln == NULL) {
    return NULL;
  }
  int currentSchedule = ln->getSx();
  if (matchesPosID( scheduleList[currentSchedule].getPos(), posID)) {
    return &scheduleList[currentSchedule];
  }
  if (currentSchedule+2 >= LOKO_MAX_SCHEDULE) {
    return NULL;
  }
  if (matchesPosID( scheduleList[currentSchedule+1].getPos(), posID)) {
    currentSchedule++;
    ln->setSx( currentSchedule );
    return &scheduleList[currentSchedule];
  }
  return NULL;
}

void XmlPiRail::processSchedule( MsgState* schedule, int scheduleIndex ) {
  if (scheduleIndex == 0) {
    for (int i = 0; i < LOKO_MAX_SCHEDULE; i++) {
      scheduleList[i].setId( NULL );
    }
    if (this->deviceState != NULL) {
      Ln* ln = this->deviceState->getLn();
      if (ln != NULL) {
        ln->setSx( 0 );
      }
    }
  }
  if ((schedule != NULL) && (scheduleIndex < LOKO_MAX_SCHEDULE)) {
    scheduleList[scheduleIndex].setId( schedule->getId() );
    scheduleList[scheduleIndex].setPos( schedule->getPos() );
    scheduleList[scheduleIndex].setDist( schedule->getDist() );
    scheduleList[scheduleIndex].setCmd( schedule->getCmd() );
    scheduleList[scheduleIndex].setCmdDist( schedule->getCmdDist() );

    //--- Immediately process schedule if it is for current block
    MotorAction *motorAction = (MotorAction *) cfg->childAt( piRailFactory->ID_MOTOR, 0 );
    if (motorAction != NULL) {
      Ln *ln = getDeviceState()->getLn();
      if (ln != NULL) {
        if (matchesPosID( schedule->getPos(), ln->getPosID() )) {
          char cmd = schedule->getCmd();
          char scheduleDir = getCmdDir( cmd );
          // Only if cmd is AutoDrive on/off or cmdDir is matching current dir
          if ((cmd == 'A') || (cmd == 'a') || (ln->getClk() == scheduleDir)) {
            motorAction->setValue( cmd, schedule->getCmdDist(), getCfg(), getIoCfg(), motorAction->getItemConn()->getLocker() );
          }
        }
      }
    }
  }
}

/**
 * @param myIP
 * @param fromIP
 * @param cmd
 * @return true if state was changed by cmd, i.e. state message has to be sent immediately
 */
bool XmlPiRail::processCmd( unsigned int myIP, unsigned int fromIP, Cmd* cmd ) {
  bool stateChanged = false;
  unsigned long currentTime = millis();
  Model* cmdChild = cmd->firstChild( NULL );
  const QName* locker = cmd->getSender();
  bool isSync = false;
  int msgStateIndex = 0;
  while (cmdChild != NULL) {
    const QName* childRelName = cmdChild->parentRelation();
    //----- Sync command --> calcSendSlotMillis
    if (childRelName == piRailFactory->ID_SYN) {
      SyncCmd* syncCmd = (SyncCmd*) cmdChild;
      this->timeOffset = syncCmd->getTime() - currentTime;
      int mySyncIP = (myIP >> 24) & 0xff;
      if ((mySyncIP > syncCmd->getStartIP() && (mySyncIP < syncCmd->getEndIP()))) {
        int sendIndex = mySyncIP - syncCmd->getStartIP();
        this->nextSyncTime = currentTime + sendIndex * syncCmd->getMsSlotSize();
      }
      isSync = true;
    }
    //----- Lock command
    else if (childRelName == piRailFactory->ID_LCK) {
      LockCmd* lockCmd = (LockCmd*) cmdChild;
      const QName* itemConnID = lockCmd->getId();
      if (itemConnID == NULL) {
        //--- Lock whole module
        if (lockCmd->getApply()) {
          this->deviceState->setLocker( locker );
        }
        else {
          this->deviceState->setLocker( NULL );
        }
        Model* cfgChild = cfg->firstChild( NULL );
        while (cfgChild != NULL) {
          if (cfgChild->typeID() == piRailFactory->ID_ITEMCONN) {
            ItemConn *itemConn = (ItemConn *) cfgChild;
            if (lockCmd->getApply()) {
              itemConn->setLocker( locker );
            }
            else {
              itemConn->setLocker( NULL );
            }
          }
          cfgChild = cfgChild->nextSibling( NULL );
        }
      }
      else {
        //--- Lock single itemConn
        ItemConn *itemConn = cfg->getItemConn( itemConnID );
        if (itemConn != NULL) {
          if (lockCmd->getApply()) {
            itemConn->setLocker( locker );
            debugIDVal( "Lock ", itemConn->getId() );
          }
          else {
            itemConn->setLocker( NULL );
            debugIDVal( "Unlock ", itemConn->getId() );
          }
        }
      }
      //--- Update all actions with lockID of their itemConn
      Model* cfgChild = cfg->firstChild( NULL );
      while (cfgChild != NULL) {
        const QName* childTypeID = cfgChild->typeID();
        if ((itemConnID == NULL) && (childTypeID == piRailFactory->ID_ITEMCONN)) {
          ItemConn *itemConn = (ItemConn*) cfgChild;
          if (lockCmd->getApply()) {
            itemConn->setLocker( locker );
            debugIDVal( "Lock ", itemConn->getId() );
          }
          else {
            itemConn->setLocker( NULL );
            debugIDVal( "Unlock ", itemConn->getId() );
          }
        }
        if ((childTypeID == piRailFactory->ID_TIMERACTION)
            || (childTypeID == piRailFactory->ID_MOTORACTION)
            || (childTypeID == piRailFactory->ID_ENUMACTION)
            || (childTypeID == piRailFactory->ID_RANGEACTION)
            || (childTypeID == piRailFactory->ID_SENSORACTION)) {
          Action *action = (Action *) cfgChild;
          ItemConn *conn = action->getItemConn();
          if (conn != NULL) {
            ActionState *actionState = action->getState();
            if (actionState != NULL) {
              actionState->setLck( conn->getLocker() );
            }
          }
        }
        cfgChild = cfgChild->nextSibling( NULL );
      }
      stateChanged = true;
    }
    //----- Motor speed and dir
    else if (childRelName == piRailFactory->ID_MO) {
      MoCmd *moCmd = (MoCmd*) cmdChild;
      const QName *id = moCmd->getId();
      MotorAction *motorAction = cfg->getMotor( id );
      if (motorAction != NULL) {
        motorAction->setValue( moCmd->getDir(), moCmd->getSpeed(), cfg, ioCfg, locker );
        stateChanged = true;
      }
    }
    //----- Set function
    else if (childRelName == piRailFactory->ID_SET) {
      SetCmd *setCmd = (SetCmd *) cmdChild;
      const QName *id = setCmd->getId();
      Action *action = cfg->getAction( id );
      if (action == NULL) {
        Ln* ln = deviceState->getLn();
        if (ln != NULL) {
          if (ln->lastPosID == id) {
            // new command for last pos
            MotorAction *motorAction = (MotorAction *) cfg->childAt( piRailFactory->ID_MOTOR, 0 );
            motorAction->setValue( setCmd->getValue(), setCmd->getIntVal(), cfg, getIoCfg(), motorAction->getItemConn()->getLocker() );
          }
        }
      }
      else {
        const QName* varID = setCmd->getVarID();
        if (varID == NULL) {
          action->setValue( setCmd->getValue(), setCmd->getIntVal(), cfg, ioCfg, locker );
          stateChanged = true;
        }
        else {
          if (!action->setVar( varID, setCmd->getValue(), setCmd->getIntVal() )) {
            debugIDVal( "Var not found id=", varID );
          }
        }
      }
    }
    //----- Test output
    else if (childRelName == piRailFactory->ID_TST) {
      SetCmd *tstCmd = (SetCmd *) cmdChild;
      const QName *id = tstCmd->getId();
      if (id == piRailFactory->ID_OUTCFG) {
        const QName* ioID = tstCmd->getVarID();
        OutCfg *pinCfg = cfg->getOutCfg( ioID );
        if (pinCfg == NULL) {
          debugStr( "process tst: ", Msg_outCfgNotFound );
        }
        else {
          int pinValue = -1;
          if (pinCfg->getType() == OUTTYPE_SERVO) {
            attachOutPin( pinCfg, OUTMODE_SERVO );
            doOutputPin( pinCfg, pinValue, -1 );
          }
          else {
            attachOutPin( pinCfg, OUTMODE_PULSE );
            doOutputPin( pinCfg, pinValue, 500 );
          }
        }
      }
      else {
        PortCfg* portCfg = cfg->getPortCfg( id );
        if (portCfg != NULL) {
          if (portCfg->getType() == PORTTYPE_SERIALMODULATED) {
            int index = portCfg->getParamIntVal( piRailFactory->ID_INDEX );
            debugVal( "test IR-Port ", index );
            attachPort( portCfg, PORTMODE_IR_READER );
            doSendMsg( portCfg, "[XY0aHbl]" );
          }
        }
      }
    }
    //----- MsgState for NFC tag mapping or Loko schedule
    else if (childRelName == piRailFactory->ID_MSG) {
      if (isSync) {
        processMsgState((MsgState *) cmdChild );
      }
      else {
        processSchedule( (MsgState *) cmdChild, msgStateIndex );
      }
      msgStateIndex++;
    }
    else {
      debugIDVal( "Unknown cmd rel=", childRelName );
    }
    cmdChild = cmdChild->nextSibling( NULL );
  }
  if (!isSync) {
    //--- If not sync: For any command clear schedule if autoDrive of first motor is off
    MotorAction *motorAction = (MotorAction *) cfg->firstChild( piRailFactory->ID_MOTOR );
    if ((motorAction != NULL) && (!motorAction->isAutoDrive())) {
      processSchedule( NULL, 0 );
    }
  }
  return stateChanged;
}

/**
 * Read XML from inputStream and process contained command or state message
 * @param myIP
 * @param fromIP
 * @param inputStream
 * @return true if state was changed by cmd, i.e. state message has to be sent immediately
 */
bool XmlPiRail::processXml( unsigned int myIP, unsigned int fromIP, Stream* inputStream ) {
  bool stateChanged = false;
  _xmlReader->initParser( _xmlBuffer, xmlBufferSize, inputStream );
  const QName *tag = _xmlReader->nextTag( NULL );
  if (tag == piRailFactory->ID_CMD) {
    udpCmd.parse( _xmlReader, tag );
    stateChanged = processCmd( myIP, fromIP, &udpCmd );
    piRailFactory->clearCmd( &udpCmd );
  }
  else if (tag == piRailFactory->ID_STATE) {
    udpState.parse( _xmlReader, tag );
    processState( myIP, fromIP, &udpState );
    piRailFactory->clearState( &udpState );
  }
  resetXmlBuffer();
  return stateChanged;
}

// </editor-fold>

//------------------------------------------------------------------
// Event-processing
//------------------------------------------------------------------

SensorAction* XmlPiRail::getIDSensorAction() {
  const QName* idReaderConnID = NULL;
  ItemConn* itemConn = (ItemConn*) cfg->firstChild( piRailFactory->ID_ITEMCONN );
  while (itemConn != NULL) {
    Port* port = itemConn->getPort( NULL );
    if (port != NULL) {
      portMode_t mode = port->getMode();
      if ((mode == PORTMODE_IR_READER) || (mode == PORTMODE_NFC_READER) || (mode == PORTMODE_IDREADER)) {
        idReaderConnID = itemConn->getId();
        itemConn = NULL;
      }
    }
    else {
      itemConn = (ItemConn*) itemConn->nextSibling( piRailFactory->ID_ITEMCONN );
    }
  }
  if (idReaderConnID == NULL) {
    // for compatibility with old cfg:
    int sensorCount = cfg->sensorCount();
    if (sensorCount > 0) {
      return cfg->getSensorAt( 0 );
    }
    else {
      return NULL;
    }
  }
  else {
    SensorAction* sensorAction = (SensorAction*) cfg->firstChild( piRailFactory->ID_SENSOR );
    while (sensorAction != NULL) {
      if (sensorAction->getItemID() == idReaderConnID) {
        return sensorAction;
      }
      sensorAction = (SensorAction*) sensorAction->nextSibling( piRailFactory->ID_SENSOR );
    }
    return NULL;
  }
}

bool XmlPiRail::processTrackMsg(SensorAction* sensorAction, const QName* posID, int dist_mm, char cmd, int cmdDist_mm, unsigned long idReadMillis ) {
  Ln *ln = getDeviceState()->getLn();
  if (this->deviceState->getType() == DEVICETYPE_LOCOMOTIVE) {
    unsigned long readDelta = idReadMillis - ln->lastIdReadMillis;
    ln->msgCount++; // count overall received msg for debugging

    //---------- repeated commands are not executed and only first event is sent
    //           time stamp (idReadTime) is kept from first occurrence
    bool unchanged = ((ln->lastPosID == posID) && (ln->lastCmd == cmd));
    if (unchanged && ((readDelta < 1000) || (cmd == 's') || (cmd == 'S'))) {
      return false;
    }

    //----- Process speed and dir
    if ((posID != NULL) && (ln->lastPosID != NULL)) {
      const char *posIDStr = posID->getName();
      const char *lastPosIDStr = ln->lastPosID->getName();
      if ((readDelta < 30000) && (strlen( posIDStr ) == 3) && (strlen( lastPosIDStr ) == 3)) {
        // check if same track line
        if ((lastPosIDStr[0] == posIDStr[0]) && (lastPosIDStr[1] == posIDStr[1])) {
          int spd = 0;
          char clockDir = DIR_UNKNOWN;
          if ((lastPosIDStr[2] + 1) == posIDStr[2]) {
            spd = dist_mm * 1000 / readDelta; // mm per second
            clockDir = DIR_FORWARD;
          }
          else if (lastPosIDStr[2] == (posIDStr[2] + 1)) {
            spd = -1 * ln->lastIdDist_mm * 1000 / readDelta; // mm per second
            clockDir = DIR_BACK;
          }
          ln->setLineSpeed( spd, clockDir, idReadMillis );
        }
      }
    }
  }

  //----- Add event to send queue
  bool sendEvent = false;
  if (sensorAction != NULL) {
    sendEvent = sensorAction->processEvent( posID, cmd, idReadMillis, cfg, ioCfg );
  }
  if (this->deviceState->getType() != DEVICETYPE_LOCOMOTIVE) {
    return sendEvent;
  }

  //----- Update pos info - this must happen after addEvent()
  ln->setPosInfo( posID, idReadMillis );
  ln->lastPosID = posID;
  ln->lastIdDist_mm = dist_mm;
  ln->lastIdReadMillis = idReadMillis;
  ln->lastCmd = cmd;

  //----- Check if command's direction matches loco direction
  char clockDir = ln->getClk();
  char cmdDir = getCmdDir( cmd );

  //----- Check if loco has a schedule for ID read.
  //      Schedule is ignored if loco dir is unknown.
  MsgState* schedule = getSchedule4Pos( posID );
  if ((schedule != NULL) && (clockDir != DIR_UNKNOWN)) {
    char scheduleCmd = schedule->getCmd();
    int scheduleCmdDist_mm = schedule->getCmdDist() * 10;
    char scheduleCmdDir = getCmdDir( scheduleCmd );
    if (clockDir == scheduleCmdDir) {
      // scheduleCmd is only valid if it matches loco direction
      if (cmdDir != clockDir) {
        //-- cmd is for other dir, so use schedule cmd
        cmd = scheduleCmd;
        cmdDist_mm = scheduleCmdDist_mm;
        cmdDir = scheduleCmdDir;
      }
      else {
        // cmd and schedule are for same dir, so check for higher priority command
        if ((cmd == 'E') || (cmd == 'e')) {
          // highest priority command
        }
        else if ((scheduleCmd == 'S') || (scheduleCmd == 's') || (scheduleCmd == 'R') || (scheduleCmd == 'r')) {
          // Schedule stop or reverse is an extended halt, so it has higher priority than halt
          cmd = scheduleCmd;
          cmdDist_mm = scheduleCmdDist_mm;
        }
        else if ((cmd == 'H') || (cmd == 'h') || (cmd == 'T') || (cmd == 't') || (cmd == 'M') || (cmd == 'm')) {
          // halt command --> ignore non halting schedule
        }
        else if ((cmd == 'W') || (cmd == 'w')) {
          if ((scheduleCmd == 'L') || (scheduleCmd == 'l')) {
            //-- both limit - use lower
            if (scheduleCmdDist_mm < LOKO_WARN_SPEED) {
              cmd = scheduleCmd;
              cmdDist_mm = scheduleCmdDist_mm;
            }
          }
        }
        else if ((cmd == 'L') || (cmd == 'l')) {
          if ((scheduleCmd == 'L') || (scheduleCmd == 'l')) {
            //-- both limit - use lower
            if (scheduleCmdDist_mm < cmdDist_mm) {
              cmdDist_mm = scheduleCmdDist_mm;
            }
          }
          else if ((scheduleCmd == 'W') || (scheduleCmd == 'w')) {
            //-- both limit - use lower
            if (cmdDist_mm > LOKO_WARN_SPEED) {
              cmdDist_mm = scheduleCmdDist_mm;
            }
          }
        }
      }
    }
  }

  //----- Process command if changed, ignore at least ' ', '+' and '-'
  if (cmd >= '0') {
    Cfg *cfg = getCfg();
    MotorAction *motorAction = (MotorAction *) cfg->childAt( piRailFactory->ID_MOTOR, 0 );
    if ((clockDir == DIR_UNKNOWN) || (cmdDir == DIR_UNKNOWN) || (clockDir == cmdDir)) {
      if ((cmd == 'S') || (cmd == 's') || (cmd == 'R') || (cmd == 'r') || (cmd == 'L') || (cmd == 'l')) {
        // cmdDist is used as other type of parameter, so do not use sensorDist
      }
      else {
        int sensorDist = 0;
        if (sensorAction != NULL) {
          if (clockDir == cmdDir) {
            char motorDir = motorAction->getState()->getCur();
            if (motorDir == DIR_FORWARD) {
              sensorDist = (sensorAction->dFront) * 10;
            }
            else {
              sensorDist = (sensorAction->dBack) * 10;
            }
          }
          else {
            sensorDist = (sensorAction->dFront) * 10;
          }
        }
        cmdDist_mm -= sensorDist;
        if (cmdDist_mm < 0) {
          cmdDist_mm = 0;
        }
      }
      motorAction->setValue( cmd, cmdDist_mm, cfg, getIoCfg(), motorAction->getItemConn()->getLocker() );
    }
  }
  return true;
}

