/*
 * This file is part of PI-Rail-Arduino (https://gitlab.com/pi-rail/pi-rail-arduino).
 * Copyright (c) 2013-2020 PI-Data AG, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PIRAIL_XMLPIRAIL_H
#define PIRAIL_XMLPIRAIL_H

#include "Arduino.h"
#include "PiRailSketch.h"
#include "../../PiMobile/src/Namespace.h"
#include "../../PiMobile/src/XmlReader.h"
#include "../../PiMobile/src/XmlWriter.h"
#include "EnumAction.h"
#include "MotorAction.h"
#include "MsgState.h"
#include "OutCfg.h"
#include "StateScript.h"
#include "WifiCfg.h"
#include "WifiCfgOld.h"
#include "TimerAction.h"
#include "NetCfg.h"
#include "NetCfgOld.h"
#include "FileCfg.h"
#include "IoCfg.h"
#include "Cfg.h"
#include "Cmd.h"
#include "State.h"
#include "Csv.h"
#include "Data.h"

#define LOKO_MAX_SCHEDULE 50

//----- Constants for UPD communication
#define piRailReceivePort 9751
#define piRailSendPort 9750
//static const char udpSendIP[] = "255.255.255.255";

//----- Constants for RFID
#define EVENT_ID_LEN 20
#define EVENT_CMD_LEN 6
#define EVENT_MAX_COUNT 4

#define VAR_MAX_COUNT 16

//-------- XML types union ------------

class XmlPiRail {
private:
  bool errorMode = false;
  const char* _wifiModuleType = NULL;
  XmlReader * _xmlReader = NULL;
  XmlWriter * _xmlWriter = NULL;
  int _receivePos;

  NetCfg* netCfg = NULL;
  NetCfgOld* netCfgOld = NULL;
  IoCfg* ioCfg = NULL;
  Cfg* cfg = NULL;
  State* deviceState = NULL;
  int stateMsgNum = 0;

  long timeOffset = 0;
  unsigned long nextSyncTime = -1;

  MsgState scheduleList[LOKO_MAX_SCHEDULE];

  Cmd udpCmd;
  State udpState;
  char* _udpOut = NULL;
  int xmlBufferSize = 0;
  char* _xmlBuffer = NULL;

  Model* loadConfigToXmlBuffer( const char * path );
  void loadConfigFiles();

  void initPins();
  void initActions();

public:
  XmlPiRail( const char* wifiModuleType, int xmlReadBufferSize, int xmlWriteBufferSize );
  void begin( char* udpOut );

  void setup( const char* macAddress, deviceType_t deviceType );

  bool isErrorMode();
  void setErrorMode( bool errorMode );
  uint8_t calcCRC( char* data, int len );

  void resetXmlBuffer();
  char* getXmlBuffer();

  int getXmlBufferSize();

  const char* getDeviceID();
  WifiCfg* getWifiCfg();
  NetCfg *getNetCfg();
  IoCfg *getIoCfg();
  Cfg *getCfg();

  long getNextSyncTime();
  void setNextSyncTime( long nextSyncTime );

  State* getDeviceState();

  void received( char val );
  Model* parse( Stream* inputStream );

  MsgState* getSchedule4Pos( const QName* posID );
  void processSchedule( MsgState* schedule, int index );
  void processState( unsigned int myIP, unsigned int fromIP, State* deviceState );
  bool processCmd( unsigned int myIP, unsigned int fromIP, Cmd* cmd );
  bool processXml( unsigned int myIP, unsigned int fromIP, Stream* inputStream );

  void sendDeviceState( unsigned long currentTimeMillis, int wifiDB, const char* apMAC );
  const char* writeData( Data* data );

  SensorAction* getIDSensorAction();
  bool processTrackMsg( SensorAction* sensorAction, const QName* posID, int dist, char cmd, int cmdDist, unsigned long idReadMillis );

};


#endif //PIRAIL_XMLPIRAIL_H
