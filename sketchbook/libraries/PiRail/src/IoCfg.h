// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

#ifndef IOCFG_H
#define IOCFG_H

#include <string.h>
#include "../../PiMobile/src/Model.h"
#include "../../PiMobile/src/QName.h"
#include "../../PiMobile/src/XmlReader.h"
#include "ExtCfg.h"
#include "InCfg.h"
#include "Model.h"
#include "OutCfg.h"
#include "PiRailFactory.h"
#include "PortCfg.h"
#include "QName.h"
#include "XmlReader.h"

class IoCfg : public Model {

protected:
  char fw[FW_MAX_LEN+1];
  char version[VERSION_MAX_LEN+1];

public:
  IoCfg();

  virtual const QName* typeID();
  virtual void parseAttr( XmlReader* xmlReader );
  virtual void writeAttr( XmlWriter* xmlWriter );
  virtual Model* createChild( const QName* tag );
  const char* getFw();
  void setFw( char* fw );
  const char* getVersion();
  void setVersion( char* version );
//----------------------------------------------------------
// Manual extensions
public:
  OutCfg* getOutCfg( int pin );
  OutCfg* getOutCfg( const QName* pinID );
  InCfg* getInCfg( const QName* pinID );
  ExtCfg* getExtCfg( const QName* name );
  PortCfg* getPortCfg( const QName* name );
};

#endif //IOCFG_H
