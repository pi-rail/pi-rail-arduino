// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

#ifndef SENSORACTION_H
#define SENSORACTION_H

#include <string.h>
#include "../../PiMobile/src/Model.h"
#include "../../PiMobile/src/QName.h"
#include "../../PiMobile/src/XmlReader.h"
#include "Action.h"
#include "Cfg.h"
#include "IoCfg.h"
#include "PiRailFactory.h"
#include "QName.h"
#include "State.h"
#include "StateScript.h"
#include "TrackPos.h"
#include "XmlReader.h"

class SensorAction : public Action {

protected:
  inModeType_t type = INMODETYPE_NONE;
  int maxAgo = 0;
  const QName* inID = NULL;
  int history = 0;

public:
  SensorAction();

  virtual const QName* typeID();
  virtual void parseAttr( XmlReader* xmlReader );
  virtual void writeAttr( XmlWriter* xmlWriter );
  virtual Model* createChild( const QName* tag );
  inModeType_t getType();
  void setType( inModeType_t type );
  int getMaxAgo();
  void setMaxAgo( int maxAgo );
  const QName* getInID();
  void setInID( const QName* inID );
  int getHistory();
  void setHistory( int history );
//----------------------------------------------------------
// Manual extensions
private:
  ActionState* state;
  InPin* inPin = NULL;
  int pollInterval = 0;

public:
  bool eventOccurred = false;
  unsigned long nextPoll = 0;
  unsigned long lastEventTime = 0;
  int dFront = 0;
  int dBack = 0;
  Data* data = NULL;

  virtual void initState( State* newState, Cfg* cfg, IoCfg* ioCfg );
  virtual ActionState* getState();
  virtual void setValue( char value, int intVal, Cfg* cfg, IoCfg* ioCfg, const QName* locker );
  virtual char getVar( const QName* varID );
  virtual int getVarInt( const QName* varID );
  virtual bool setVar( const QName* varID, char value, int intValue );
  InCfg* getInCfg( Cfg* cfg );
  StateScript* getStateScript( char value );
  bool processEvent( const QName* srcID, char eventValue, unsigned long eventTime, Cfg* cfg, IoCfg* ioCfg );
};

#endif //SENSORACTION_H
