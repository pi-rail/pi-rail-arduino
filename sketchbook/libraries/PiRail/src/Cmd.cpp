// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

#include  "Cmd.h"

extern void fatalError( const __FlashStringHelper* msg );

Cmd::Cmd() {
}

const QName* Cmd::typeID() {
  return piRailFactory->ID_CMD;
}

void Cmd::parseAttr( XmlReader* xmlReader ) {
  xmlReader->getQNameAttr( piRailFactory->ID_ID, (const QName**) &(this->id) );
  xmlReader->getQNameAttr( piRailFactory->ID_SENDER, (const QName**) &(this->sender) );
}

void Cmd::writeAttr( XmlWriter* xmlWriter ) {
  xmlWriter->attribute( piRailFactory->ID_ID, this->id );
  xmlWriter->attribute( piRailFactory->ID_SENDER, this->sender );
}

Model* Cmd::createChild( const QName* tag ) {
  if (tag == piRailFactory->ID_MO) {
    return piRailFactory->getNewMoCmd( this, tag );
  }
  else if (tag == piRailFactory->ID_SET) {
    return piRailFactory->getNewSetCmd( this, tag );
  }
  else if (tag == piRailFactory->ID_LCK) {
    return piRailFactory->getNewLockCmd( this, tag );
  }
  else if (tag == piRailFactory->ID_SYN) {
    return piRailFactory->getNewSyncCmd( this, tag );
  }
  else if (tag == piRailFactory->ID_TST) {
    return piRailFactory->getNewSetCmd( this, tag );
  }
  else if (tag == piRailFactory->ID_MSG) {
    return piRailFactory->getNewMsgState( this, tag );
  }
  else {
    return Model::createChild( tag );
  }
}

const QName* Cmd::getId() {
  return this->id;
}

void Cmd::setId( const QName* id ) {
  this->id = id;
}

const QName* Cmd::getSender() {
  return this->sender;
}

void Cmd::setSender( const QName* sender ) {
  this->sender = sender;
}

//----------------------------------------------------------
// Manual extensions

