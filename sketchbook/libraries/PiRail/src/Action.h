// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

#ifndef ACTION_H
#define ACTION_H

#include <string.h>
#include "../../PiMobile/src/Model.h"
#include "../../PiMobile/src/QName.h"
#include "../../PiMobile/src/XmlReader.h"
#include "Model.h"
#include "PiRailFactory.h"
#include "QName.h"
#include "TrackPos.h"
#include "XmlReader.h"

class Action : public Model {

protected:
  const QName* id = NULL;
  const QName* itemID = NULL;
  const QName* group = NULL;

public:
  Action();

  virtual const QName* typeID();
  virtual void parseAttr( XmlReader* xmlReader );
  virtual void writeAttr( XmlWriter* xmlWriter );
  virtual Model* createChild( const QName* tag );
  const QName* getId();
  void setId( const QName* id );
  const QName* getItemID();
  void setItemID( const QName* itemID );
  const QName* getGroup();
  void setGroup( const QName* group );
//----------------------------------------------------------
// Manual extensions
protected:
  ItemConn* itemConn = NULL;
public:
  ItemConn* getItemConn();
  virtual void initState( State* newState, Cfg* cfg, IoCfg* ioCfg ) = 0;
  virtual ActionState* getState() = 0;
  virtual void setValue( char value, int intVal, Cfg* cfg, IoCfg* ioCfg, const QName* locker ) = 0;
  virtual char getVar( const QName* varID ) = 0;
  virtual int getVarInt( const QName* varID ) = 0;
  virtual bool setVar( const QName* varID, char value, int intValue ) = 0;
  bool isLocker( const QName* locker );
};

#endif //ACTION_H
