// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

#ifndef TRACKPOS_H
#define TRACKPOS_H

#include <string.h>
#include "../../PiMobile/src/Model.h"
#include "../../PiMobile/src/QName.h"
#include "../../PiMobile/src/XmlReader.h"
#include "Model.h"
#include "PiRailFactory.h"
#include "QName.h"
#include "XmlReader.h"

class TrackPos : public Model {

protected:
  const QName* panelID = NULL;
  int x = 0;
  int y = 0;
  int rot = 0;
  const QName* posID = NULL;
  int dist = 0;

public:
  TrackPos();

  virtual const QName* typeID();
  virtual void parseAttr( XmlReader* xmlReader );
  virtual void writeAttr( XmlWriter* xmlWriter );
  const QName* getPanelID();
  void setPanelID( const QName* panelID );
  int getX();
  void setX( int x );
  int getY();
  void setY( int y );
  int getRot();
  void setRot( int rot );
  const QName* getPosID();
  void setPosID( const QName* posID );
  int getDist();
  void setDist( int dist );
//----------------------------------------------------------
// Manual extensions
};

#endif //TRACKPOS_H
