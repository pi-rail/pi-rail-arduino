// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

#include  "RangeAction.h"
#include "Cfg.h"
#include "PiRailSketch.h"


extern void fatalError( const __FlashStringHelper* msg );

RangeAction::RangeAction() {
}

const QName* RangeAction::typeID() {
  return piRailFactory->ID_RANGEACTION;
}

void RangeAction::parseAttr( XmlReader* xmlReader ) {
  Action::parseAttr( xmlReader );
  xmlReader->getQNameAttr( piRailFactory->ID_PINID, (const QName**) &(this->pinID) );
  xmlReader->getQNameAttr( piRailFactory->ID_PARAMID, (const QName**) &(this->paramID) );
  xmlReader->getIntAttr( piRailFactory->ID_DEFAULT, (int *) &(this->_default), 0 );
  xmlReader->getIntAttr( piRailFactory->ID_MINVAL, (int *) &(this->minVal), 0 );
  xmlReader->getIntAttr( piRailFactory->ID_MAXVAL, (int *) &(this->maxVal), 0 );
}

void RangeAction::writeAttr( XmlWriter* xmlWriter ) {
  Action::writeAttr( xmlWriter );
  xmlWriter->attribute( piRailFactory->ID_PINID, this->pinID );
  xmlWriter->attribute( piRailFactory->ID_PARAMID, this->paramID );
  xmlWriter->attribute( piRailFactory->ID_DEFAULT, this->_default );
  xmlWriter->attribute( piRailFactory->ID_MINVAL, this->minVal );
  xmlWriter->attribute( piRailFactory->ID_MAXVAL, this->maxVal );
}

Model* RangeAction::createChild( const QName* tag ) {
  if (tag == piRailFactory->ID_TRACKPOS) {
    return piRailFactory->getNewTrackPos( this, tag );
  }
  else {
    return Action::createChild( tag );
  }
}

const QName* RangeAction::getPinID() {
  return this->pinID;
}

void RangeAction::setPinID( const QName* pinID ) {
  this->pinID = pinID;
}

const QName* RangeAction::getParamID() {
  return this->paramID;
}

void RangeAction::setParamID( const QName* paramID ) {
  this->paramID = paramID;
}

int RangeAction::getDefault() {
  return this->_default;
}

void RangeAction::setDefault( int _default ) {
  this->_default = _default;
}

int RangeAction::getMinVal() {
  return this->minVal;
}

void RangeAction::setMinVal( int minVal ) {
  this->minVal = minVal;
}

int RangeAction::getMaxVal() {
  return this->maxVal;
}

void RangeAction::setMaxVal( int maxVal ) {
  this->maxVal = maxVal;
}

//----------------------------------------------------------
// Manual extensions

void RangeAction::initState( State* deviceState, Cfg* cfg, IoCfg* ioCfg ) {
  this->state = piRailFactory->getNewActionState( deviceState, piRailFactory->ID_ACT );
  this->state->setId( this->getId() );
  this->state->setCurI( this->getDefault() );
  this->state->setTgtI( this->getDefault() );
  deviceState->addChild( piRailFactory->ID_ACT, this->state, deviceState->lastChild( NULL ));
  this->itemConn = cfg->getItemConn( this->getItemID() );
  if (this->itemConn == NULL) {
    debugStr( piRailFactory->MSG_ITEM_CONN_NOT_FOUND );
  }
  else {
    if (this->itemConn->attach( cfg )) {
      debugStr( piRailFactory->MSG_SUCESS );
    }
    else {
      debugStr( piRailFactory->MSG_COULD_NOT_ATTACH );
    }
  }
}

ActionState* RangeAction::getState() {
  return this->state;
}

void RangeAction::setValue( char value, int intVal, Cfg* cfg, IoCfg* ioCfg, const QName* locker ) {
  if (this->state == NULL) {
    debugStr( "range not init" );
    return;
  }
  if (!this->isLocker( locker )) {
    debugStr( piRailFactory->MSG_LOCKED );
    return;
  }
  OutPin *outPin = this->itemConn->getOutPin( this->getPinID() );
  if (outPin == NULL) {
    debugStr( piRailFactory->MSG_PIN_NOT_FOUND );
  }
  else {
    const QName* ioID = outPin->getIoID();
    OutCfg *outCfg = cfg->getOutCfg( ioID );
    if (outCfg == NULL) {
      debugStr( piRailFactory->MSG_OUT_CFG_NOT_FOUND );
    }
    else {
      doOutputPin( outCfg, intVal, -1 );
      this->state->setCurI( intVal );
      this->state->setUpd( millis());
    }
  }
}

char RangeAction::getVar( const QName* varID ) {
  return '?';
}

int RangeAction::getVarInt( const QName* varID ) {
  return -1;
}

bool RangeAction::setVar( const QName* varID, char value, int intValue ) {
  // no variables - do nothing
  return false;
}


