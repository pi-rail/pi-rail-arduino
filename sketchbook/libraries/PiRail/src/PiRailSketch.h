//
// Created by pru on 08.01.21.
//

#ifndef SKETCHBOOK_PIRAILSKETCH_H
#define SKETCHBOOK_PIRAILSKETCH_H

#include "ExtCfg.h"
#include "InCfg.h"
#include "MotorAction.h"
#include "OutCfg.h"
#include "PortCfg.h"
#include "SensorAction.h"
#include "TimerAction.h"

//-------- Declaration of callback functions --------
extern bool attachOutPin( OutCfg* outCfg, outMode_t outMode );
extern bool attachInPin( InCfg* inCfg, inModeType_t inMode, fireType_t fireType, bool useInterrupt );
extern bool attachPort( PortCfg* portCfg, portMode_t portMode );
extern void prepareMotorSensor( PortCfg* portCfg, char dir );
extern int readMotorSensor( PortCfg* portCfg, char dir, int currentSpeed );
extern bool checkIDReader( SensorAction* sensorAction );
extern void processMsgState( MsgState* msgState );
extern void doMotor( PortCfg* portCfg, char dir, int motorOutput );
extern void doOutputPin( OutCfg* outCfg, int pinValue, int duration );
extern void sendUdpMessage( const char *msg );
extern void doSendMsg( PortCfg* portCfg, const char* msg );

extern void debugStr( const char* msg, const char* msg2 = NULL, const char* msg3 = NULL, const char* msg4 = NULL );
extern void debugVal( const char* msg, long val, const char* msg2 = NULL, long val2 = 0 );
extern void debugIDVal( const char* msg, const QName* id, const char* msg2 = NULL, int intVal = 0, char charVal = 0 );

extern int getHeapSize();

extern void fatalError( const char* msg1, const char* msg2 );
extern void printHeap( const char* msg1, const char* msg2 );

#endif //SKETCHBOOK_PIRAILSKETCH_H
