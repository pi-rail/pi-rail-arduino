// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

#include  "LockCmd.h"

extern void fatalError( const __FlashStringHelper* msg );

LockCmd::LockCmd() {
}

const QName* LockCmd::typeID() {
  return piRailFactory->ID_LOCKCMD;
}

void LockCmd::parseAttr( XmlReader* xmlReader ) {
  xmlReader->getQNameAttr( piRailFactory->ID_ID, (const QName**) &(this->id) );
  xmlReader->getBoolAttr( piRailFactory->ID_APPLY, (bool *) &(this->apply) );
}

void LockCmd::writeAttr( XmlWriter* xmlWriter ) {
  xmlWriter->attribute( piRailFactory->ID_ID, this->id );
  xmlWriter->attribute( piRailFactory->ID_APPLY, this->apply );
}

const QName* LockCmd::getId() {
  return this->id;
}

void LockCmd::setId( const QName* id ) {
  this->id = id;
}

bool LockCmd::getApply() {
  return this->apply;
}

void LockCmd::setApply( bool apply ) {
  this->apply = apply;
}

//----------------------------------------------------------
// Manual extensions
