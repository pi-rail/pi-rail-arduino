// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

#include  "IfCmd.h"
#include "Cfg.h"
#include "Command.h"


extern void fatalError( const __FlashStringHelper* msg );

IfCmd::IfCmd() {
}

const QName* IfCmd::typeID() {
  return piRailFactory->ID_IFCMD;
}

void IfCmd::parseAttr( XmlReader* xmlReader ) {
  Command::parseAttr( xmlReader );
  xmlReader->getQNameAttr( piRailFactory->ID_ID, (const QName**) &(this->id) );
  xmlReader->getStringAttr( piRailFactory->ID_VALUE, (char *) &(this->value), 1 );
  xmlReader->getIntAttr( piRailFactory->ID_INTVAL, (int *) &(this->intVal), 0 );
}

void IfCmd::writeAttr( XmlWriter* xmlWriter ) {
  Command::writeAttr( xmlWriter );
  xmlWriter->attribute( piRailFactory->ID_ID, this->id );
  xmlWriter->attribute( piRailFactory->ID_VALUE, this->value );
  xmlWriter->attribute( piRailFactory->ID_INTVAL, this->intVal );
}

Model* IfCmd::createChild( const QName* tag ) {
  if (tag == piRailFactory->ID_IF) {
    return piRailFactory->getNewIfCmd( this, tag );
  }
  else if (tag == piRailFactory->ID_SETIO) {
    return piRailFactory->getNewSetIO( this, tag );
  }
  else if (tag == piRailFactory->ID_SET) {
    return piRailFactory->getNewSetCmd( this, tag );
  }
  else if (tag == piRailFactory->ID_CALL) {
    return piRailFactory->getNewCallCmd( this, tag );
  }
  else if (tag == piRailFactory->ID_DELAY) {
    return piRailFactory->getNewDelayCmd( this, tag );
  }
  else if (tag == piRailFactory->ID_ON) {
    return piRailFactory->getNewMsgState( this, tag );
  }
  else {
    return Command::createChild( tag );
  }
}

const QName* IfCmd::getId() {
  return this->id;
}

void IfCmd::setId( const QName* id ) {
  this->id = id;
}

char IfCmd::getValue() {
  return this->value;
}

void IfCmd::setValue( char value ) {
  this->value = value;
}

int IfCmd::getIntVal() {
  return this->intVal;
}

void IfCmd::setIntVal( int intVal ) {
  this->intVal = intVal;
}

//----------------------------------------------------------
// Manual extensions

bool IfCmd::checkIf( ItemConn* itemConn, Cfg* cfg ) {
  const QName *varName = this->getId();
  if (varName == NULL) {
    return false;
  }
  const QName* id = this->getId();
  Param* param = itemConn->getParam( id );
  char cmpChar;
  int cmpInt;
  if (param == NULL) {
    Action* action = cfg->getAction( id );
    if (action == NULL) {
      return false;
    }
    else {
      cmpChar = action->getState()->getCur();
      cmpInt = action->getState()->getCurI();
    }
  }
  else {
    cmpChar = param->getValue();
    cmpInt = param->getIntVal();
  }
  char value = this->getValue();
  int intVal = this->getIntVal();
  if ((value == '=') || (value == 0)) {
    return (intVal == cmpInt);
  }
  else if (value == '!') {
    return (intVal != cmpChar);
  }
  else if (value == '<') {
    return (intVal < cmpChar);
  }
  else if (value == '>') {
    return (intVal > cmpChar);
  }
  else {
    return (value == cmpChar);
  }
}

void IfCmd::execute( ItemConn* itemConn, Cfg* cfg, IoCfg* ioCfg, const QName* locker ) {
  if (this->checkIf( itemConn, cfg )) {
    Command* op = (Command*) firstChild( NULL );
    while (op != NULL) {
      op->execute( itemConn, cfg, ioCfg, locker );
      op = (Command*) op->nextSibling( NULL );
    }
  }
}

