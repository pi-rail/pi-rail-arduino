/*
 * This file is part of PI-Rail-Arduino (https://gitlab.com/pi-rail/pi-rail-arduino).
 * Copyright (c) 2013-2020 PI-Data AG, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <FS.h>

#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

#define LOCO_ESP8285 1
#include <QName.h>
#include <XmlPiRail.h>
#include <PiHttpServer.h>
#include <PiOTA.h>

#include <DCCPacketScheduler.h>

int INFO_LED = 10; // vorne

int LED_ON = HIGH;
int LED_OFF = LOW;

//----- Constants and variable for WiFi
const char *wifiModuleType = __FILE__;
int wifiStatus = WL_IDLE_STATUS;

//----- Constants and variable for Motor
const int LOKO_MODE_DIRECT = 0;
const int LOKO_MODE_DCC = 1;
const int LOKO_MODE_MM2 = 2;
int lokoMode = LOKO_MODE_DIRECT;
int sensorPin = A0;
int mo_speed_inc = 15;                // increment/decrement for acceleration/breaking
unsigned long mo_wait = 0;              // time to wait when having stopped
int mo_delta_spd = 0;
Data* data = NULL;

//----- Constants and variable for ID-Reader
bool serialEventOccurred = false;
int idReaderPos = -1;
char idReaderData[MSG_MAX_LEN + 1];
char lastIdReaderData[MSG_MAX_LEN + 1];
long idReadMillis = 0;

#define HOSTNAME_SIZE 32
#define ERROR_MSG_SIZE 64
const int UDP_BUF_SIZE = 1500;

//char inputString[UDP_BUF_SIZE + 1]; // a string to hold incoming data
char myHostname[HOSTNAME_SIZE + 1];
boolean stringComplete = false;        // whether the string is complete
int inputPos;

WiFiUDP Udp;
char udpOut[UDP_BUF_SIZE+1];

const PROGMEM char *sketchFileNameAndCompileDate = __FILE__ " " __DATE__ " " __TIME__;
const char *sketchNameAndCompileDate;

bool initialConfigMode = false;
char lastErrorMsg[ERROR_MSG_SIZE + 1];

static PROGMEM const char *PI_SETUP = "Pi-Setup";

XmlPiRail xmlPiRail( wifiModuleType, 1024, UDP_BUF_SIZE );

PiHttpServer piHttpServer;
bool httpServerWasAccessed = false;

PiOTA piOta;

DCCPacketScheduler dps;
bool dpsActive = false;
extern volatile bool get_next_packet;
extern volatile uint8_t current_packet[6];

//-------------------------------------------------
void fatalError( const __FlashStringHelper* msg ) {
  Serial.println( msg );
  Serial.println( F("Stopped - try to fix config") );
  xmlPiRail.setErrorMode( true );
  xmlPiRail.setNextSyncTime( millis() );
  int count = strlen_P( (PGM_P) msg );
  if (count > ERROR_MSG_SIZE) {
    count = ERROR_MSG_SIZE;
  }
  memcpy_P( lastErrorMsg, msg, count );
  lastErrorMsg[count] = 0;
}

unsigned long resetMillisStart = 0L;
unsigned long resetAfterMillis = 0L;

//-------------------------------------------------
void printHeap( const __FlashStringHelper* msg ) {
  Serial.print( F("ms=") );
  Serial.print( millis() );
  Serial.print( " " );
  Serial.print( msg );
  Serial.print( F(", heap=") );
  Serial.println( ESP.getFreeHeap() );
}

//-------------------------------------------------
const char* getVersion() {
  return __DATE__;
}

//-------------------------------------------------
int getHeapSize() {
  int heap = ESP.getFreeHeap();
  return heap;
}

//-------------------------------------------------
void doReset( long afterMillis ) {
  resetMillisStart = millis();
  resetAfterMillis = afterMillis > 0 ? afterMillis : 250; // have some minimum value
  Serial.println();
  Serial.print( F("Requested reset ") );
  Serial.print( afterMillis );
  Serial.print( F(" ms after ") );
  Serial.println( resetMillisStart );
}

//-------------------------------------------------
void httpServerAccessed() {
  httpServerWasAccessed = true;
}

int ledSpeedDefault = 250;

int ledSpeed = ledSpeedDefault;
int ledState = LED_ON;
unsigned long lastToggle = 0;

//-------------------------------------------------
bool toggleLed() {
  unsigned long now = millis();

  if (!lastToggle || now - lastToggle > ledSpeed) {
    ledState = ledState == LED_ON ? LED_OFF : LED_ON;
    digitalWrite( INFO_LED, ledState );
    lastToggle = now;
    return true;
  }
  else {
    return false;
  }
}

//-------------------------------------------------
void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print( F("SSID: ") );
  Serial.println( WiFi.SSID() );

  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print( F("IP: ") );
  Serial.print( ip.toString() );

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print( F(" RSSI: ") );
  Serial.print( rssi );
  Serial.println( F(" dBm") );
}

//-------------------------------------------------
boolean connectToWifi() {
  const char* ssid = xmlPiRail.getSsid();
  Serial.print( "ssid=" );  Serial.println( ssid );
  if ((ssid == NULL) || (strlen(ssid) <= 0)) {
    return false;
  }
  const char* pass = xmlPiRail.getPass();

  //--- connect to Wifi
  Serial.print( F("Connecting to ") );
  Serial.print( ssid );
  // Not supported by ESP8266/8285: WiFi.setHostname( myHostname );

  // Delay based on MAC to avoid all devices connecting same time
  Serial.print( F(", delay=") );
  uint8_t mac[6];
  WiFi.macAddress( mac );
  int waitConn = mac[5] * 4;
  Serial.println( waitConn );
  delay( waitConn );

  // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
  WiFi.begin( ssid, pass );

  ledSpeed = 200;
  unsigned long startMillis = millis();

  while (WiFi.status() != WL_CONNECTED && millis() - startMillis < 60 * 1000) {
    Serial.print(".");
    delay(200);
    toggleLed();
  }

  bool success = WiFi.status() == WL_CONNECTED;

  if (success) {
    Serial.println();
    Serial.println( F("Connected") );
    printWifiStatus();
  }
  else {
    Serial.println();
    Serial.println( F("WiFi failure") );
    //WiFi.disconnect( true, false );
  }
  digitalWrite(INFO_LED, success ? LED_OFF : LED_ON);

  return success;
}

boolean isValidNameChar( char ch ) {
  return ( ((ch == '-') || (ch == '_'))
           || ((ch >= '0') && (ch <= '9'))
           || ((ch >= 'A') && (ch <= 'Z'))
           || ((ch >= 'a') && (ch <= 'z'))
         );
}

void setUpHostname( const char * deviceId, boolean macPrefix ) {
  memset( myHostname, 0, HOSTNAME_SIZE + 1 );
  int pos = 0;
  int i = 0;
  if (macPrefix) {
    String mac = WiFi.macAddress();
    while (pos < mac.length()) {
      char ch = mac.charAt(pos);
      if (isValidNameChar( mac[pos] )) {
        myHostname[i++] = ch;
      }
      pos++;
    }
    myHostname[i++] = '-';
  }
  if (deviceId != NULL) {
    pos = 0;
    while ((i < HOSTNAME_SIZE - 1) && (deviceId[pos] > 0)) {
      char ch = deviceId[pos];
      if (isValidNameChar( ch )) {
        myHostname[i++] = ch;
      }
      else {
        myHostname[i++] = '-';
      }
      pos++;
    }
  }
}

void setUpConfigWifi() {
  digitalWrite( INFO_LED, HIGH );
  Serial.println();
  Serial.print( F("Setup AP: ") );
  Serial.println( myHostname );

  WiFi.softAP( myHostname );

  IPAddress wifiLocalIp = WiFi.softAPIP();
  Serial.print( F("AP IP=") );
  Serial.println( wifiLocalIp );
}

//--------------------------
void initOutput( OutCfg* outCfg ) {
  int pin = outCfg->getPin();
  outType_t pinType = outCfg->getType();
  if (pin < 0x100) {
    switch (pinType) {
      case OUTTYPE_LOGIC:
      case OUTTYPE_HIGHSIDE:
      case OUTTYPE_LOWSIDE:
      case OUTTYPE_HALFBRIDGE: {
        pinMode( pin, OUTPUT );
        digitalWrite( pin, LOW );
        break;
      }
      default: {
        Serial.println( "unsupported pinType" );
      }
    }
  }
  else {
    Serial.println( F("--TODO-- init I2C port") );
  }
}

void initInput( InCfg* pinCfg ) {
  // Input not supported"
}

bool attachOutPin( OutCfg* outCfg, outMode_t outMode ) {
  ExtCfg *extCfg = outCfg->getExtCfg();
  if (extCfg == NULL) {
    if (outCfg->attached == OUTMODE_NONE) {
      outCfg->attached = outMode;
      return true;
    }
    else {
      return (outMode == outCfg->attached);
    }
  }
  else {
    PortCfg* extPort = xmlPiRail.getCfg()->getPortCfg( extCfg->getPort() );
    if (extPort == NULL) {
      return false;
    }
    else if (extPort->attached == PORTMODE_NONE) {
      portMode_t extPortMode = extCfg->getType();
      if (attachPort( extPort, extPortMode )) {
        outCfg->attached = outMode;
        return true;
      }
      else {
        return false;
      }
    }
    else {
      if (outCfg->attached == OUTMODE_NONE) {
        outCfg->attached = outMode;
        return true;
      }
      else {
        return (outCfg->attached == outMode);
      }
    }
  }
}

bool attachInPin( InCfg* inCfg, inModeType_t inMode, fireType_t fireType, bool useIterrupt ) {
  return true;
}

bool attachPort( PortCfg* portCfg, portMode_t portMode ) {
  ExtCfg *extCfg = portCfg->getExtCfg();
  if (extCfg == NULL) {
    if (portCfg->attached == PORTMODE_NONE) {
      if ((portMode == PORTMODE_IR_READER) || (portMode == PORTMODE_IDREADER)) {
        // always using normal serial port
        portCfg->attached = portMode;
        return true;
      }
      else if (portMode == PORTMODE_DCCDECODER) {
        portType_t portType = portCfg->getType();
        if (portType == PORTTYPE_MOTORPORT) {
          Serial.print(F( "  Setting up DCC pin=" ));
          int dirPin = portCfg->getPinByType( PORTPINTYPE_DIRPIN );
          if (dirPin < 0) {
            Serial.println(F( " - ERROR dirPin invalid" ));
            return false;
          }
          int pwmPin = portCfg->getPinByType( PORTPINTYPE_PWM_PIN );
          if (pwmPin < 0) {
            Serial.println(F( " - ERROR pwmPin invalid" ));
            return false;
          }
          Serial.print( pwmPin );
          pinMode( pwmPin, OUTPUT );
          digitalWrite( pwmPin, HIGH );
          portCfg->attached = portMode;

          dps.setup( dirPin, 0xFF, true, ROCO );
          dps.setpower( ON );
          dps.setSpeed128( 3, 0 );
          dpsActive = true;
          Serial.println(F( " - DCC initialized" ));
          return true;
        }
        else {
          Serial.println(F( "Unsupported motor port type" ));
          return false;
        }
      }
      else {
        Serial.println(F( "Unsupported port mode" ));
        return false;
      }
    }
    else {
      return (portCfg->attached == portMode);
    }
  }
  else {
    PortCfg* extPort = xmlPiRail.getCfg()->getPortCfg( extCfg->getPort() );
    if (extPort == NULL) {
      return false;
    }
    else if (extPort->attached == PORTMODE_NONE) {
      portMode_t extPortMode = extCfg->getType();
      if (attachPort( extPort, extPortMode )) {
        portCfg->attached = portMode;
        return true;
      }
      else {
        return false;
      }
    }
    else {
      if (portCfg->attached == PORTMODE_NONE) {
        portCfg->attached = portMode;
        return true;
      }
      else {
        return (portCfg->attached == portMode);
      }
    }
  }
}

//--------------------------
void setup() {
  sketchNameAndCompileDate = strrchr( sketchFileNameAndCompileDate, '/' );
  if (sketchNameAndCompileDate) {
    sketchNameAndCompileDate ++;
  }
  else {
    sketchNameAndCompileDate = sketchFileNameAndCompileDate;
  }

  pinMode(INFO_LED, OUTPUT);     // Initialize the INFO_LED pin as an output
  digitalWrite(INFO_LED, HIGH);  // Turn the LED off by making the voltage HIGH
  lastErrorMsg[0] = 0;

  Serial.begin( 2400 ); // 74880 ); //115200 );
  inputPos = 0;
  Serial.println();
  Serial.println( sketchNameAndCompileDate );
  Serial.println();
  printHeap( F("Begin setup") );

  piRailFactory->max_csv_count = 5;
  piRailFactory->max_ev_count = 4;
  piRailFactory->max_fileDef_count = 10;

  data = new Data();
  xmlPiRail.setup( WiFi.macAddress().c_str(), DEVICETYPE_LOCOMOTIVE );
  ESP.wdtFeed();

  // Ensure we have a hostname before connectToWifi()
  Cfg* cfg = xmlPiRail.getCfg();
  const char *deviceName;
  if ((cfg != NULL) && (cfg->getId() != NULL) && (strlen(cfg->getId()->getName()) > 0)) {
    deviceName = cfg->getId()->getName();
    setUpHostname( deviceName, false );
  }
  else {
    deviceName = xmlPiRail.getDeviceID();
    if (deviceName == NULL) {
      deviceName = "INIT";
    }
    setUpHostname( deviceName, true );
  }

  if (!initialConfigMode) {
    initialConfigMode = !connectToWifi();
  }

  if (!initialConfigMode && !xmlPiRail.isErrorMode()) {
    Serial.print( F("Start listen UDP ") );
    Serial.println( piRailReceivePort );
    // if you get a connection, report back via serial:
    Udp.begin( piRailReceivePort );
    xmlPiRail.begin( udpOut );
  }

  if (initialConfigMode) {
    // discard the hostname and rebuild
    setUpHostname( deviceName, true );
  }

  Serial.print( F("Hostname=") );
  Serial.println( myHostname );

  if (initialConfigMode) {
    setUpConfigWifi();
  }

  ESP.wdtFeed();
  delay(250);
  ESP.wdtFeed();

  NetCfg* netCfg = xmlPiRail.getNetCfg();
  piHttpServer.init( sketchNameAndCompileDate, myHostname, netCfg );
  piHttpServer.begin();

  //--- Does not work on ESP8285: piOta.begin( myHostname, &ArduinoOTA );
  //    So do initialization inline
  ArduinoOTA.setHostname( myHostname );
  ArduinoOTA.onStart( PiOTA::startOTA );
  ArduinoOTA.onEnd( PiOTA::endOTA );
  ArduinoOTA.onProgress( PiOTA::progressOTA );
  ArduinoOTA.onError( PiOTA::errorOTA );
  ArduinoOTA.begin();

  printHeap( F("Finished setup") );
}

//------------------------------------------------------------------
// Hardware access functions
//------------------------------------------------------------------

int readMotorSensor( PortCfg* portCfg, int currentSpeed ) {
  // not supported
  return 0;
}

void processMsgState( MsgState* newMsgState ) {
  // not supported
}

bool checkIDReader( SensorAction* sensorAction ) {
  // not supported
  return false;
}

void doMotor( PortCfg* portCfg, char dir, int motorOutput ) {
  if (portCfg == NULL) {
    return;
  }
  ExtCfg* extCfg = portCfg->getExtCfg();
  if (extCfg == NULL) {
    // not supported
  }
  else {
    if (extCfg->getType() == PORTMODE_DCCDECODER) {
      ExtCfg* extCfg = portCfg->getExtCfg();
      uint16_t lokID = extCfg->getBusAddr();
      uint8_t dccSpeed = (motorOutput >> 3); // reduce 0..1023 to 0..127
      if (dir == DIR_FORWARD) {
        dccSpeed = dccSpeed + 128;
      }
      dps.setSpeed128( lokID, dccSpeed );
      dps.update();
    }
  }
}

void doOutputPin( OutCfg* outCfg, int pinValue, int duration ) {
  if (outCfg == NULL) {
    return;
  }
  ExtCfg* extCfg = outCfg->getExtCfg();
  if (extCfg == NULL) {
    int pin = outCfg->getPin();
    if (pin < 0) {
      return;
    }
    outMode_t outMode = outCfg->attached;
    switch (outMode) {
      case OUTMODE_SERVO: {
        // not supported
        break;
      }
      case OUTMODE_PWM: {
        Serial.print(F( "  Set PWM pin " ));
        analogWrite( pin, pinValue );
        Serial.println( pinValue );
        break;
      }
      case OUTMODE_SWITCH: {
        Serial.print(F( "  Set port " ));
        Serial.print( pin );
        if (pinValue == 0) {
          digitalWrite( pin, LOW );
          Serial.println( " off" );
        }
        else if (pinValue == -1) {
          digitalWrite( pin, HIGH );
          Serial.println( " on" );
        }
        break;
      }
      default: { // PULSE or NONE
        if ((duration <= 0) || (duration > 500)) {
          duration = 500;
        }
        Serial.print(F( "  Pulse pin " ));
        Serial.print( pin );
        Serial.print( F(", val=") );
        Serial.print( pinValue );
        Serial.print( F(", len=") );
        Serial.println( duration );
        if (pinValue < 0) {
          digitalWrite( pin, HIGH );
        }
        else {
          Serial.print( F("doPulse NOT YET IMPLEMENTED for value=") );
          Serial.println( pinValue );
          //analogWrite( swPort, pinValue );
        }
        delay( duration );
        digitalWrite( pin, LOW );
      }
    }
  }
  else if (extCfg->getType() == PORTMODE_DCCDECODER) {
    int fn = outCfg->getPin();
    if (outCfg->attached == OUTMODE_SWITCH) {
      if (pinValue != 0) {
        pinValue = 1;
      }
    }
    else {
      pinValue = 2; // toggle
    }
    uint16_t locoAddr = extCfg->getBusAddr();
    Serial.print( F("Set DCC addr=") );
    Serial.print( locoAddr );
    Serial.print( F(", f") );
    Serial.print( fn );
    Serial.print( F("=") );
    Serial.println( pinValue );
    dps.setLocoFunc( locoAddr, pinValue, fn );
    dps.update();
  }
}

void doSendMsg( PortCfg* portCfg, const char* msg ) {
  // not supported - do nothing
}

//------------------------------------------------------------------
// Command Execution (doXXX, processXXX)
//------------------------------------------------------------------

void sendUdpMessage( const char *msg ) {
  //IPAddress sendIP;
  //WiFi.hostByName(udpSendIP, sendIP);
  //Udp.beginPacketMulticast( WiFi.localIP(), sendIP, piRailSendPort ); // funzt nicht
  //Udp.beginPacketMulticast( sendIP, piRailSendPort, WiFi.localIP() ); // funzt nicht
  //Udp.beginPacket( "192.168.43.50", piRailSendPort ); // funzt
  //Udp.beginPacket( "255.255.255.255", piRailSendPort ); // funzt nicht
  //Udp.beginPacketMulticast(addr, port, WiFi.localIP())
  IPAddress sendIP = WiFi.localIP();
  sendIP[3] = 255;
  Udp.beginPacket( sendIP, piRailSendPort ); // funzt
  Udp.print( msg );
  Udp.endPacket();
  udpOut[0] = 0; // clear udpOut buffer
}

//------------------------------------------------------------------
// Serial Event Handling
//------------------------------------------------------------------
/*
 SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */
/*void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();

    //----- Add char to ID reader input
    if (inChar == '[') { // start flag
      idReaderPos = 0;
      idReadMillis = millis();
    }
    else if (idReaderPos >= 0) {
      if (inChar == ']') {
        if (idReaderPos == 7) {
          // checksum
          int cksum = 0;
          for (int i = 0; i < idReaderPos-1; i++) {
            cksum += (byte) idReaderData[i];
          }
          cksum = cksum & 0x7F;
          if (cksum < 32) {
            cksum += 32; // avoid using control characters
          }
          if ((cksum == '[') || (cksum==']')) {
            cksum++; // avoid using mesage start/end characters
          }
          char ckSumCh = (char) cksum;

          if (ckSumCh == idReaderData[6]) {
            // replace checksum byte with string end
            idReaderData[6] = 0;
            bool repeated = false;

            //-- Process speed, dir and command
            long dist = (idReaderData[3] - 32) * 20; //mm
            const QName* posID = piRailFactory->NS_PIRAILFACTORY->getQName( idReaderData, 0, 3 );
            char cmd = idReaderData[4];
            int cmdDist = (idReaderData[5] - 32) * 20;
            xmlPiRail.processTrackMsg( posID, dist, cmd, cmdDist, idReadMillis );
            for (int i = 0; i < idReaderPos; i++) {
              lastIdReaderData[i] = idReaderData[i];
            }
            serialEventOccurred = true;
          }
        }
        idReaderPos = -1;
      }
      else {
        if (idReaderPos < MSG_MAX_LEN) {
          idReaderData[idReaderPos] = inChar;
          idReaderPos++;
        }
        else {
          idReaderPos = -1;
        }
      }
    }
  }
}
*/
void debugMsg( const char* msg, const QName* id, int val ) {
  Csv* csv = data->addCsv( CSVTYPE_DEBUGDATA );
  if (csv == NULL) { // buffer is full --> send
    sendUdpMessage( xmlPiRail.writeData( data ) ); // prints csv data to udpOut
    data->clear();
    csv = data->addCsv( CSVTYPE_DEBUGDATA );
  }
  csv->appendStr( msg );
  if (id == NULL) {
    csv->appendStr( "null" );
    csv->appendStr( "null" );
  }
  else {
    csv->appendStr( id->getNamespace()->getUri() );
    csv->appendStr( id->getName() );
  }
  csv->append( val );
  sendUdpMessage( xmlPiRail.writeData( data ) ); // prints csv data to udpOut
  data->clear();
}

//==========================
// Main Loop
//==========================
void loop() {
  if ( resetMillisStart != 0L && (unsigned long)( millis() - resetMillisStart ) >= resetAfterMillis ) {
    Serial.println();
    Serial.print( F("Performing reset at ") );
    Serial.println( millis() );
    ESP.restart();
  }

  ArduinoOTA.handle();

  if (initialConfigMode) {
    if (!httpServerWasAccessed && millis() >= 3 * 60 * 1000) {
      // 3 minutes = 1 minute trying to connect to existing WiFi plus 2 minutes configuration window
      Serial.println();
      Serial.println( F("Config timeout. Restarting…") );
      Serial.println();
      ESP.restart();
    }
    return; // return even if it's not yet time to restart
  }

  if (xmlPiRail.isErrorMode()) {
    unsigned long currentTime = millis();
    long nextSyncTime = xmlPiRail.getNextSyncTime();
    if ((nextSyncTime >= 0) && (currentTime >= nextSyncTime)) {
      IPAddress sendIP = WiFi.localIP();
      sendIP[3] = 255;
      Udp.beginPacket( sendIP, piRailSendPort ); // funzt
      const char* deviceID = xmlPiRail.getDeviceID();
      if ((deviceID != NULL) && (strlen(deviceID) > 0)) {
        Udp.print( deviceID );
      }
      else {
        Udp.print( myHostname );
      }
      Udp.print( ":\n" );
      Udp.print( sketchNameAndCompileDate );
      Udp.print( "\nERROR - " );
      Udp.print( lastErrorMsg );
      Udp.endPacket();
      udpOut[0] = 0; // clear udpOut buffer
      xmlPiRail.setNextSyncTime( currentTime + 5000 );
    }
  }
  else {
    bool sendState = false;
    int packetSize = Udp.parsePacket();
    if (packetSize > 0) {
      IPAddress remoteIp = Udp.remoteIP();
      sendState = xmlPiRail.processXml( WiFi.localIP(), remoteIp, &Udp );
    }
    //--- Event processing and notification
/*    else if (Serial.available()) {
      // ESP8266 is missing implementation for calling serialEvent(), so do it manually
      // see https://github.com/esp8266/Arduino/issues/752
      serialEvent();
      if (serialEventOccurred) {
        sendUdpMessage( xmlPiRail.writeDeviceState( millis() ) );
        serialEventOccurred = false;
      }
    }*/
    else {
      if (dpsActive) {
        dps.update();
      }
      delay( 5 );
    }
    ESP.wdtFeed();

    //--- accelerate or slow down
    Cfg* cfg = xmlPiRail.getCfg();
    Ln* ln = xmlPiRail.getDeviceState()->getLn();
    MotorAction* motorAction = (MotorAction*) cfg->firstChild( piRailFactory->ID_MOTOR );
    while (motorAction != NULL) {
      int oldSpeed = motorAction->getState()->getCurI();
      motorAction->doLoop( data, ln );
      int newSpeed = motorAction->getState()->getCurI();
      if (oldSpeed != newSpeed) {
        sendState = true;
      }
      motorAction = (MotorAction*) motorAction->nextSibling( piRailFactory->ID_MOTOR );
    }
    ESP.wdtFeed();

    IoCfg* ioCfg = xmlPiRail.getIoCfg();

    //--- Send sync message
    unsigned long currentTime = millis();
    unsigned long nextSyncTime = xmlPiRail.getNextSyncTime();
    if ((nextSyncTime >= 0) && (currentTime >= nextSyncTime)) {
      sendState = true;
      xmlPiRail.setNextSyncTime( -1 );
    }
    if (sendState) {
      // print events and speed changes into udpOut
      xmlPiRail.sendDeviceState( currentTime, WiFi.RSSI() );
    }

    //--- Check timers and execute if time out
    TimerAction* timerAction = (TimerAction*) cfg->firstChild( piRailFactory->ID_TIMER );
    while (timerAction != NULL) {
      timerAction->doLoop( cfg, ioCfg );
      timerAction = (TimerAction*) timerAction->nextSibling( piRailFactory->ID_TIMER );
    }

    //--- Check triggers
    xmlPiRail.processState( WiFi.localIP(), WiFi.localIP(), xmlPiRail.getDeviceState() );
    ESP.wdtFeed();

    //--- Send motor tuning data if buffer is full
    if (data->isFull()) {
      sendUdpMessage( xmlPiRail.writeData( data ) ); // prints csv data to udpOut
      data->clear();
    }
  }
}
