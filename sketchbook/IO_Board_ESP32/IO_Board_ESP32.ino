/*
 * This file is part of PI-Rail-Arduino (https://gitlab.com/pi-rail/pi-rail-arduino).
 * Copyright (c) 2013-2020 PI-Data AG, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <string.h>
#include <PString.h>

#include <EEPROM.h>
#include <FS.h>

#include <SPIFFS.h>

#include <WiFi.h>
#include <WiFiUdp.h>
#include <esp_wifi.h>
#include <esp_wifi_types.h>
#include <esp_event_legacy.h>
#include <Wire.h>
#include <Servo.h>
#include <driver/adc.h>

#include <XmlPiRail.h>
#include <PiHttpServer.h>
#include <PiOTA.h>

#include <DCCPacketScheduler.h>

#include <PN532_HSU.h>
#include <PN532.h>

//#define LOG_ALL_EVENTS

int INFO_LED = 2; //INFO_LED;

int LED_ON = HIGH;
int LED_OFF = LOW;
int ledState = LED_ON;
int ledSpeedDefault = 250;
int ledSpeed = ledSpeedDefault;
unsigned long lastToggle = 0;

//----- Constants and variable for WiFi
const char *wifiModuleType = __FILE__;
int wifiStatus = WL_IDLE_STATUS;

//----- Constants and variable for IO pins
#define MAX_IO_PIN 39
enum iopin_usage_t { PIN_UNUSED, PIN_INTERNAL, PIN_LED, PIN_OUTPUT, PIN_PWM,
    PIN_INPUT, PIN_SERVO, PIN_SERIAL, PIN_MOTOR, PIN_MOTORSENSOR,
    PIN_IR_SENDER, PIN_DCC, PIN_SUSI };
const char* iopin_usage_names[13] = { "unused", "internal", "LED", "output", "PWM",
  "input", "servo", "serial", "motor", "motorSensor",
  "IR_sender", "DCC", "SUSI" };
iopin_usage_t ioPinUsage[MAX_IO_PIN+1] =
    {PIN_UNUSED,PIN_SERIAL,PIN_UNUSED,PIN_SERIAL,PIN_UNUSED,
     PIN_UNUSED,PIN_INTERNAL,PIN_INTERNAL,PIN_INTERNAL,PIN_INTERNAL,
     PIN_INTERNAL,PIN_INTERNAL,PIN_UNUSED,PIN_UNUSED,PIN_UNUSED,
     PIN_UNUSED,PIN_UNUSED,PIN_UNUSED,PIN_UNUSED,PIN_UNUSED,
     PIN_UNUSED,PIN_UNUSED,PIN_UNUSED,PIN_UNUSED,PIN_UNUSED,
     PIN_UNUSED,PIN_UNUSED,PIN_UNUSED,PIN_UNUSED,PIN_UNUSED,
     PIN_UNUSED,PIN_UNUSED,PIN_UNUSED,PIN_UNUSED,PIN_UNUSED,
     PIN_UNUSED,PIN_UNUSED,PIN_UNUSED,PIN_UNUSED,PIN_UNUSED };

//----- Constants for Logging
#define UDP_DATA_COUNT 30
Data *udpData[UDP_DATA_COUNT];
int currentDataIndex = 0;
unsigned long lastDataMsg = 0;
bool logSerial = false;
bool logUDP = false;

HardwareSerial HwSerial1(1);
HardwareSerial HwSerial2(2);

//----- Constants and variable for ID-Reader
bool hwSerial2Used = false;
PN532_HSU * pn532hsu = NULL;
PN532 * nfc = NULL;
bool serialChangedState = false;
int idReaderPos = -1;
char idReaderData[MSG_MAX_LEN + 1];
long idReadMillis = 0;
const int MAX_MSG_STATE = 100;
MsgState msgStateList[MAX_MSG_STATE];
uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the returned UID
uint8_t uidLength;                        // Length of the UID (4 or 7 bytes depending on ISO14443A card type)

//----- Constants and variables for WIFI and UDP
#define HOSTNAME_SIZE 32
#define ERROR_MSG_SIZE 64
const int UDP_BUF_SIZE = 1500;
char myHostname[HOSTNAME_SIZE + 1];
WiFiUDP Udp;
char udpOut[UDP_BUF_SIZE+1];
bool udpConnected = false;
unsigned long wifiBeginTime = 0;
bool wifiScanning = false;
char apMAC[18];
unsigned long resetMillisStart = 0L;
unsigned long resetAfterMillis = 0L;
int wifi_stations = -1;

int nextPwmChannel = 1; // used for motor and PWM pins

//----- Variables for servos
#define MAX_SERVOS 2
Servo* servos[MAX_SERVOS] = {NULL, NULL};
int nextServo = 0;

const PROGMEM char *sketchFileNameAndCompileDate = __FILE__ " " __DATE__ " " __TIME__;
const char *sketchNameAndCompileDate;

bool initialConfigMode = false;
bool hasWifiCfg = false;
char lastErrorMsg[ERROR_MSG_SIZE + 1];

static PROGMEM const char *PI_SETUP = "Pi-Setup";

XmlPiRail xmlPiRail( wifiModuleType, 1024, UDP_BUF_SIZE );

//----- Variables for web page and firmware update
PiHttpServer piHttpServer;
bool httpServerWasAccessed = false;
PiOTA piOta;

bool inputEventOccurred = false;
portMUX_TYPE mux = portMUX_INITIALIZER_UNLOCKED;

//----- Variables for DCC interface
DCCPacketScheduler dps;
bool dpsActive = false;
extern volatile bool get_next_packet;
extern volatile uint8_t current_packet[6];

//-------------------------------------------------
void IRAM_ATTR eventIsr(void *arg) {
  // Die ISR-Routine muss ohne fremden Code auskommen!
  // Der Grund:
  // Sowohl XmlPiRail als auch die Model-klassen haben NICHT das IRAM_ATTR, dadurch müssen
  // sie ggf. erst aus dem Flash-Speicher nachgeaden werden und dann dauert das zu lange
  // Symptom: Ich hatte gleich am Anfang von xmlPiRail.addEvent() ein Serial.println() drin
  // und das kam manchmal und manchmal nicht.
  portENTER_CRITICAL( &mux );
  InCfg *inCfg = static_cast<InCfg*>(arg);
  inputEventOccurred = true;
  inCfg->eventOccurred = true;
  portEXIT_CRITICAL( &mux );
}

/*  TODO Funktioniert teilweise nicht bei 2 Events (Dreiwegweiche) - schaltet zweiten nicht ab
struct timerAction {
  unsigned long millis;
  int interval;
  int pin;
  int value;
};

#define MAX_TIMER_ACTIONS 2

volatile int timerActionCount = 0;
struct timerAction timerActions[MAX_TIMER_ACTIONS];

bool addTimerAction( unsigned long millis, int len, const int port, int i );

hw_timer_t * timer = NULL;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;

void IRAM_ATTR onTimer() {
  portENTER_CRITICAL_ISR(&timerMux);
  if (timerActionCount > 0) {
    unsigned long now = millis();
    if( (unsigned long)( now - timerActions[0].millis ) >= (unsigned long)timerActions[0].interval ) {
      // set the pin
      digitalWrite(timerActions[0].pin, timerActions[0].value);

      if ( MAX_TIMER_ACTIONS > 1 ) {
        // memcpy overlaping areas from higher to lower address works.
        memcpy(timerActions, timerActions + sizeof(struct timerAction), ( MAX_TIMER_ACTIONS - 1 ) * sizeof(struct timerAction));
      }
      timerActionCount --;
    }
  }
  portEXIT_CRITICAL_ISR(&timerMux);
}

bool addTimerAction( const unsigned long millis, const int interval, const int pin, const int value ) {
  bool success = false;
  portENTER_CRITICAL_ISR(&timerMux);
  if (timerActionCount < MAX_TIMER_ACTIONS) {
    success = true;
    timerActions[timerActionCount].millis = millis;
    timerActions[timerActionCount].interval = interval;
    timerActions[timerActionCount].pin = pin;
    timerActions[timerActionCount].value = value;
    timerActionCount++;
  }
  portEXIT_CRITICAL_ISR(&timerMux);
  return success;
}
*/
//-------------------------------------------------
void fatalError( const char* msg1, const char* msg2 ) {
  if (logSerial) {
    Serial.print( msg1 );
    Serial.print( msg2 );
    Serial.println( "Stopped - fix config" );
  }
  xmlPiRail.setErrorMode( true );
  xmlPiRail.setNextSyncTime( millis() );
  int count = strlen_P( msg1 );
  if (count > ERROR_MSG_SIZE) {
    count = ERROR_MSG_SIZE;
  }
  memcpy_P( lastErrorMsg, msg1, count );
  int count2 = strlen_P( msg2 );
  if (count2 > (ERROR_MSG_SIZE - count)) {
    count2 = (ERROR_MSG_SIZE - count);
  }
  if (count2 > 0) {
    memcpy_P( lastErrorMsg, msg1, count );
  }
  lastErrorMsg[count] = 0;
}

//-------------------------------------------------
Csv* nextLogLine() {
  Csv *csv = udpData[currentDataIndex]->addCsv( CSVTYPE_DEBUGDATA, 1 );
  if (csv == NULL) { // buffer is full --> send
    if (udpConnected) {
      for (int i = 0; i <= currentDataIndex; i++) {
        sendUdpMessage( xmlPiRail.writeData( udpData[i] )); // prints csv data to udpOut
        udpData[i]->clear();
      }
      currentDataIndex = 0;
    }
    else {
      if (currentDataIndex < (UDP_DATA_COUNT-1)) {
        currentDataIndex++;
      }
      else {
        return NULL;
      }
    }
    csv = udpData[currentDataIndex]->addCsv( CSVTYPE_DEBUGDATA, 1 );
  }
  if (csv != NULL) {
    csv->append( millis() );
  }
  return csv;
}

void debugStr( const char* msg, const char* msg2, const char* msg3, const char* msg4 ) {
  if (logSerial) {
    Serial.print( msg );
    if (msg2 != NULL) {
      Serial.print( msg2 );
    }
    if (msg3 != NULL) {
      Serial.print( msg3 );
    }
    if (msg4 != NULL) {
      Serial.print( msg4 );
    }
    Serial.println();
  }
  if (logUDP) {
    Csv* csv = nextLogLine();
    if (csv != NULL) {
      csv->appendStr( msg );
      if (msg2 != NULL) {
        csv->appendStr( msg2 );
      }
      if (msg3 != NULL) {
        csv->appendStr( msg3 );
      }
      if (msg4 != NULL) {
        csv->appendStr( msg4 );
      }
    }
  }
}

void debugVal( const char* msg, long val, const char* msg2, long val2 ) {
  if (logSerial) {
    Serial.print( msg );
    Serial.print( val );
    if (msg2 != NULL) {
      Serial.print( ", " );
      Serial.print( msg2 );
      Serial.print( val2 );
    }
    Serial.println();
  }
  if (logUDP) {
    Csv* csv = nextLogLine();
    if (csv != NULL) {
      csv->appendStr( msg );
      csv->append( val );
      if (msg2 != NULL) {
        csv->appendStr( msg2 );
        csv->append( val2 );
      }
    }
  }
}

void debugIDVal( const char* msg, const QName* id, const char* msg2, int intVal, char charVal ) {
  if (logSerial) {
    Serial.print( msg );
    if (id == NULL) {
      Serial.print( "NULL" );
    }
    else {
      Namespace* ns = id->getNamespace();
      if (ns != piRailFactory->NS_PIRAILFACTORY) {
        Serial.print( id->getNamespace()->getUri());
        Serial.print( ":" );
      }
      Serial.print( id->getName() );
    }
    if (msg2 != NULL) {
      Serial.print( msg2 );
      Serial.print( intVal );
      Serial.print( "," );
      Serial.print( (char) charVal );
    }
    Serial.println();
  }
  if (logUDP) {
    Csv* csv = nextLogLine();
    if (csv != NULL) {
      csv->appendStr( msg );
      if (id == NULL) {
        csv->appendStr( "null" );
        csv->appendStr( "null" );
      }
      else {
        Namespace *ns = id->getNamespace();
        if (ns == piRailFactory->NS_PIRAILFACTORY) {
          csv->appendStr( "" );
        }
        else {
          csv->appendStr( id->getNamespace()->getUri());
        }
        csv->appendStr( id->getName());
      }
      if (msg2 != NULL) {
        csv->appendStr( msg2 );
        csv->append( intVal );
        csv->appendChar( charVal );
      }
    }
  }
}

//-------------------------------------------------
void printHeap( const char* msg1, const char* msg2 ) {
  if (logSerial) {
    Serial.print(F( "ms=" ));
    Serial.print( millis());
    Serial.print( " " );
    Serial.print( msg1 );
    Serial.print( " " );
    Serial.print( msg2 );
    Serial.print(F( ", heap=" ));
    Serial.println( ESP.getFreeHeap());
  }
}

//-------------------------------------------------
const char* getVersion() {
  return __DATE__;
}

//-------------------------------------------------
int getHeapSize() {
  int heap = ESP.getFreeHeap();
  return heap;
}

//-------------------------------------------------
void doReset( long afterMillis ) {
  resetMillisStart = millis();
  resetAfterMillis = afterMillis > 0 ? afterMillis : 250; // have some minimum value
  debugVal( "Reset after ms=", afterMillis );
}

//-------------------------------------------------
void httpServerAccessed() {
  httpServerWasAccessed = true;
}

//-------------------------------------------------
bool toggleLed() {
  unsigned long now = millis();
  if ((now - lastToggle) > ledSpeed) {
    if (ledState == LED_ON) {
      ledState = LED_OFF;
    }
    else {
      ledState = LED_ON;
    }
    digitalWrite( INFO_LED, ledState );
    lastToggle = now;
    return true;
  }
  else {
    return false;
  }
}

//-------------------------------------------------
IPAddress getMyIP() {
  if (wifi_stations >= 0) {
    return WiFi.softAPIP();
  }
  else {
    return WiFi.localIP();
  }
}

//-------------------------------------------------
void printWifiStatus() {
  // print the SSID of the network you're attached to:
  debugStr( "SSID: ", WiFi.SSID().c_str() );

  // print your WiFi shield's IP address:
  IPAddress ip = getMyIP();
  debugStr( "IP: ", ip.toString().c_str() );

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  debugVal( " RSSI (dBm) ", rssi );
}

//-------------------------------------------------
void printStations() {
  wifi_sta_list_t stationList;
  esp_wifi_ap_get_sta_list( &stationList );
  wifi_stations = stationList.num;
#ifdef LOG_ALL_EVENTS
  debugVal( "Connected stations:", stationList.num );
  for (int i = 0; i < stationList.num; i++) {
    wifi_sta_info_t station = stationList.sta[i];
    if (logSerial) {
      for (int j = 0; j < 6; j++) {
        char str[3];
        sprintf( str, "%02x", (int) station.mac[j] );
        Serial.print( str );
        if (j < 5) {
          Serial.print(":");
        }
      }
      Serial.println();
    }
  }
  if (logSerial) Serial.println("-----------------");
#endif
}

//-------------------------------------------------
void WiFiEvent(WiFiEvent_t event) {
  switch (event) {
#ifdef LOG_ALL_EVENTS
    case SYSTEM_EVENT_STA_START:                /**< ESP32 station start */
      debugStr( "SYSTEM_EVENT_STA_START" );
      break;
    case SYSTEM_EVENT_STA_STOP:                 /**< ESP32 station stop */
      debugStr( "SYSTEM_EVENT_STA_STOP" );
      break;
    case SYSTEM_EVENT_STA_CONNECTED:            /**< ESP32 station connected to AP */
      debugStr( "SYSTEM_EVENT_STA_CONNECTED" );
      break;
    case SYSTEM_EVENT_STA_DISCONNECTED:         /**< ESP32 station disconnected from AP */
      debugStr( "SYSTEM_EVENT_STA_DISCONNECTED" );
      break;
    case SYSTEM_EVENT_STA_AUTHMODE_CHANGE:      /**< the auth mode of AP connected by ESP32 station changed */
      debugStr( "SYSTEM_EVENT_STA_AUTHMODE_CHANGE" );
      break;
    case SYSTEM_EVENT_STA_GOT_IP:               /**< ESP32 station got IP from connected AP */
      debugStr( "SYSTEM_EVENT_STA_GOT_IP" );
      break;
    case SYSTEM_EVENT_STA_LOST_IP:              /**< ESP32 station lost IP and the IP is reset to 0 */
      debugStr( "SYSTEM_EVENT_STA_LOST_IP" );
      break;
    case SYSTEM_EVENT_STA_WPS_ER_SUCCESS:       /**< ESP32 station wps succeeds in enrollee mode */
      debugStr( "SYSTEM_EVENT_STA_WPS_ER_SUCCESS" );
      break;
    case SYSTEM_EVENT_STA_WPS_ER_FAILED:        /**< ESP32 station wps fails in enrollee mode */
      debugStr( "SYSTEM_EVENT_STA_WPS_ER_FAILED" );
      break;
    case SYSTEM_EVENT_STA_WPS_ER_TIMEOUT:       /**< ESP32 station wps timeout in enrollee mode */
      debugStr( "SYSTEM_EVENT_STA_WPS_ER_TIMEOUT" );
      break;
    case SYSTEM_EVENT_STA_WPS_ER_PIN:           /**< ESP32 station wps pin code in enrollee mode */
      debugStr( "SYSTEM_EVENT_STA_WPS_ER_PIN" );
      break;
    case SYSTEM_EVENT_AP_START:                 /**< ESP32 soft-AP start */
      debugStr( "SYSTEM_EVENT_AP_START" );
      break;
    case SYSTEM_EVENT_AP_STOP:                  /**< ESP32 soft-AP stop */
      debugStr( "SYSTEM_EVENT_AP_STOP" );
      break;
    case SYSTEM_EVENT_AP_STACONNECTED:          /**< a station connected to ESP32 soft-AP */
      debugStr( "SYSTEM_EVENT_AP_STACONNECTED" );
      break;
    case SYSTEM_EVENT_AP_PROBEREQRECVED:        /**< Receive probe request packet in soft-AP interface */
      debugStr( "SYSTEM_EVENT_AP_PROBEREQRECVED" );
      break;
    case SYSTEM_EVENT_GOT_IP6:                  /**< ESP32 station or ap or ethernet interface v6IP addr is preferred */
      debugStr( "SYSTEM_EVENT_GOT_IP6" );
      break;
#endif
    case SYSTEM_EVENT_AP_STADISCONNECTED:       /**< a station disconnected from ESP32 soft-AP */
#ifdef LOG_ALL_EVENTS
      debugStr( "SYSTEM_EVENT_AP_STADISCONNECTED" );
#endif
      printStations();
      break;
    case SYSTEM_EVENT_AP_STAIPASSIGNED:         /**< ESP32 soft-AP assign an IP to a connected station */
#ifdef LOG_ALL_EVENTS
      debugStr( "SYSTEM_EVENT_AP_STAIPASSIGNED" );
#endif
      printStations();
      break;
    default:
      debugVal( "Other WiFi-Event: ", event );
      break;
  }
}

//-------------------------------------------------
boolean connectToWifi() {
  WifiCfg* wifiCfg = xmlPiRail.getWifiCfg();
  if (wifiCfg == NULL) {
    hasWifiCfg = false;
    return false;
  }
  const char* ssid = wifiCfg->getSsid();
  debugStr( "ssid=", ssid );
  if ((ssid == NULL) || (strlen(ssid) <= 0)) {
    hasWifiCfg = false;
    return false;
  }
  hasWifiCfg = true;
  const char* pass = wifiCfg->getPass();

  //--- connect to Wifi
  WiFi.setHostname( myHostname );

  if (wifiCfg->getMode() == WIFIMODE_AP) {
    digitalWrite( INFO_LED, HIGH );
    WiFi.onEvent(WiFiEvent);
    int channel = wifiCfg->getCh();
    if ((channel < 1) || (channel > 13)) {
      channel = 1;
    }
    if (logSerial) {
      Serial.print(F( "Setup Router: " ));
      Serial.print( ssid );
      Serial.print( ", ch=" );
      Serial.println( channel );
    }

    WiFi.softAP( ssid, pass, channel, 0, 2 );

    IPAddress wifiLocalIp = WiFi.softAPIP();
    if (logSerial) {
      Serial.print(F( "IP=" ));
      Serial.println( wifiLocalIp );
    }
    wifi_stations = 0;
    digitalWrite( INFO_LED, LED_ON );
    return true;
  }

  // Delay based on MAC to avoid all devices connecting same time
  uint8_t mac[6];
  WiFi.macAddress( mac );
  int waitConn = mac[5] * 4;
  debugVal( "Connecting, delay=", waitConn );
  delay( waitConn );

  // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
  WiFi.begin( ssid, pass );

  ledSpeed = 200;
  unsigned long startMillis = millis();

  int count = 50;
  while (WiFi.status() != WL_CONNECTED && millis() - startMillis < 60 * 1000) {
    if (logSerial) {
      Serial.print( "." );
      count--;
      if (count == 0) {
        Serial.println();
        count = 50;
      }
    }
    delay(200);
    toggleLed();
  }

  bool success = WiFi.status() == WL_CONNECTED;

  if (success) {
    printWifiStatus();
  }
  else {
    debugStr( "WiFi failure" );
    WiFi.disconnect( true, false );
  }
  digitalWrite(INFO_LED, success ? LED_OFF : LED_ON);

  return success;
}

/**
 * Check the signal strength of the connection - if too low, reconnect to the strongest AP;
 * good if the ESP is moving around and reconnection to the nearest AP makes sense
 */
void checkWiFi() {
  //--- If we are the router (AP mode) just return
  if (wifi_stations >= 0) {
    return;
  }
  WifiCfg* wifiCfg = xmlPiRail.getWifiCfg();
  if (wifiCfg == NULL) {
    return;
  }
  //--- If scanning for better WiFi check if finished
  if (wifiScanning) {
    int n = WiFi.scanComplete(); // -1 in Progress; -2 not started
    if (n < 0) {
      return;
    }
    debugStr( "scan done" );
    wifiScanning = false;
    if (n == 0) {
      debugStr( "no networks found" );
      return;
    }

    const char* ssid = wifiCfg->getSsid();
    if ((ssid == NULL) || (strlen(ssid) <= 0)) {
      return;
    }
    int bestIndex = -1;
    int bestRssi = -100;
    for (int i = 0; i < n; i++) {
      if (String(ssid) == String(WiFi.SSID(i))) {
        if (WiFi.RSSI(i) > bestRssi) {
          bestIndex = i;
        }
      }
    }
    if (bestIndex < 0) {
      debugStr( "No network with SSID=", ssid );
    }
    else if (WiFi.BSSIDstr(bestIndex).equals( apMAC )) {
      wifiBeginTime = millis();
      debugStr( "No better WiFi found" );
    }
    else {
      wifiBeginTime = millis();
      Udp.stop();
      udpConnected = false;
      WiFi.disconnect();
      WiFi.begin( ssid, wifiCfg->getPass(), 0, WiFi.BSSID(bestIndex) );
    }
  }
  //--- not scanning
  else {
    if ((millis() - wifiBeginTime) < 3000) {
      // allow 5 seconds for reconnect
      return;
    }
    if (WiFi.status() == WL_CONNECTED) {
      //--- Check if current WiFi dB is above configured limit
      int minDB = wifiCfg->getMinDB();
      if (minDB > -30) {
        minDB = -100;
      }
      if ((WiFi.RSSI() > minDB) && (WiFi.RSSI() < -25)) { // good connection
        return;
      }
      // debugVal( "Wifi bad: ", WiFi.RSSI(), "dB, min=", minDB );
    }
    else {
      if (udpConnected) {
        Udp.stop();
        udpConnected = false;
      }
    }

    //--- Scan for better AP on same channel
/*    WiFi.scanDelete();
    int channel = WiFi.channel();
    if (channel < 0) {
      channel = 0;
    }
    if (WiFi.scanNetworks(true, false, false, 300, channel) == WIFI_SCAN_RUNNING) {
      wifiScanning = true;
      debugStr( "scan started" );
    }*/
  }
}

boolean isValidNameChar( char ch ) {
  return ( ((ch == '-') || (ch == '_'))
           || ((ch >= '0') && (ch <= '9'))
           || ((ch >= 'A') && (ch <= 'Z'))
           || ((ch >= 'a') && (ch <= 'z'))
         );
}

void setUpHostname( const char * deviceId, boolean macPrefix ) {
  memset( myHostname, 0, HOSTNAME_SIZE + 1 );
  int pos = 0;
  int i = 0;
  if (macPrefix) {
    String mac = WiFi.macAddress();
    while (pos < mac.length()) {
      char ch = mac.charAt(pos);
      if (isValidNameChar( mac[pos] )) {
        myHostname[i++] = ch;
      }
      pos++;
    }
    myHostname[i++] = '-';
  }
  if (deviceId != NULL) {
    pos = 0;
    while ((i < HOSTNAME_SIZE - 1) && (deviceId[pos] > 0)) {
      char ch = deviceId[pos];
      if (isValidNameChar( ch )) {
        myHostname[i++] = ch;
      }
      else {
        myHostname[i++] = '-';
      }
      pos++;
    }
  }
}

void setUpConfigWifi() {
  digitalWrite( INFO_LED, HIGH );
  if (logSerial) {
    Serial.print(F( "Setup AP: " ));
    Serial.println( myHostname );
  }

  WiFi.softAP( myHostname );

  IPAddress wifiLocalIp = WiFi.softAPIP();
  if (logSerial) {
    Serial.print(F( "IP=" ));
    Serial.println( wifiLocalIp );
  }
}

bool usePin( uint8_t pin, uint8_t mode, iopin_usage_t usage ) {
  if ((pin < 0) || (pin > MAX_IO_PIN)) {
    return false;
  }
  iopin_usage_t pinUsage = ioPinUsage[pin];
  if (pinUsage == PIN_UNUSED) {
    if (mode > 0) {
      pinMode( pin, mode );
    }
    ioPinUsage[pin] = usage;
    return true;
  }
  else if (pinUsage == usage) {
    // allow use of same pin in multiple itemConn
    return true;
  }
  else {
    debugVal( " Pin already in use: ", pin, iopin_usage_names[pinUsage], pinUsage );
    return false;
  }
}

bool attachOutPin( OutCfg* outCfg, outMode_t outMode ) {
  ExtCfg *extCfg = outCfg->getExtCfg();
  if (extCfg == NULL) {
    if (outCfg->attached == OUTMODE_NONE) {
      int pin = outCfg->getPin();
      iopin_usage_t pinUsage;
      if (outMode == OUTMODE_PWM) {
        pinUsage = PIN_PWM;
      }
      else if (outMode == OUTMODE_SERVO) {
        pinUsage = PIN_SERVO;
      }
      else {
        pinUsage = PIN_OUTPUT;
      }
      if (!usePin( pin, OUTPUT, pinUsage )) {
        return false;
      }

      if (outMode == OUTMODE_PWM) {
        if (outCfg->pwmChannel == -1) {
          outCfg->pwmChannel = nextPwmChannel;
          ledcSetup( nextPwmChannel, 5000, 10 );
          ledcAttachPin( pin, nextPwmChannel );
          debugVal( "  PWM, ch=", nextPwmChannel );
          nextPwmChannel++;
        }
      }
      else if (outMode == OUTMODE_SERVO) {
        if (nextServo >= MAX_SERVOS) {
          debugVal( "ERROR too much servos: ", nextServo );
          return false;
        }
        servos[nextServo] = new Servo();
        servos[nextServo]->attach( pin, nextPwmChannel );
        outCfg->index = nextServo;
        outCfg->pwmChannel = nextPwmChannel;
        debugVal( "  Servo ", nextServo, "ch=", nextPwmChannel );

        nextPwmChannel++;
        nextServo++;
      }
      else {
        digitalWrite( pin, LOW );
      }
      outCfg->attached = outMode;
      return true;
    }
    else {
      return (outMode == outCfg->attached);
    }
  }
  else {
    PortCfg* extPort = xmlPiRail.getCfg()->getPortCfg( extCfg->getPort() );
    if (extPort == NULL) {
      return false;
    }
    else if (extPort->attached == PORTMODE_NONE) {
      portMode_t extPortMode = extCfg->getType();
      if (attachPort( extPort, extPortMode )) {
        outCfg->attached = outMode;
        return true;
      }
      else {
        return false;
      }
    }
    else {
      if (outCfg->attached == OUTMODE_NONE) {
        outCfg->attached = outMode;
        return true;
      }
      else {
        return (outCfg->attached == outMode);
      }
    }
  }
}

bool attachInPin( InCfg* inCfg, inModeType_t inMode, fireType_t fireType, bool useIterrupt ) {
  if (inCfg != NULL) {
    if (inCfg->attached == INMODETYPE_NONE) {
      int pin = inCfg->getPin();
      if (inCfg->getPullup()) {
        if (!usePin( pin, INPUT_PULLUP, PIN_INPUT )) {
          return false;
        }
      }
      else {
        if (!usePin( pin, INPUT, PIN_INPUT )) {
          return false;
        }
      }
      if (inMode == INMODETYPE_DIGITAL) {
        int pin = inCfg->getPin();
        if (pin > 0) {
          if (useIterrupt) {
            switch (fireType) {
              case FIRETYPE_ONFALL: {
                attachInterruptArg( pin, eventIsr, inCfg, FALLING );
                break;
              }
              case FIRETYPE_ONRISE: {
                attachInterruptArg( pin, eventIsr, inCfg, RISING );
                break;
              }
              default: {
                attachInterruptArg( pin, eventIsr, inCfg, CHANGE );
              }
            }
            debugVal( "Attached interrupt to pin=", pin );
          }
          else{
            debugVal( "Activated input polling for pin=", pin );
          }
        }
        else {
          debugIDVal( "Pin missing in inCfg ", inCfg->getId() );
        }
      }
      inCfg->attached = inMode;
      return true;
    }
    else {
      return (inCfg->attached == inMode);
    }
  }
}

bool attachPort( PortCfg* portCfg, portMode_t portMode ) {
  ExtCfg *extCfg = portCfg->getExtCfg();
  if (extCfg == NULL) {
    if (portCfg->attached == PORTMODE_NONE) {
      portType_t portType = portCfg->getType();
      if (portType == PORTTYPE_SERIALMODULATED) {
        int pwmPin = portCfg->getPinByType( PORTPINTYPE_CLOCKPIN );
        if (!usePin( pwmPin, OUTPUT, PIN_IR_SENDER )) {
          return false;
        }
        debugVal("  pwm=", pwmPin );
        int rxPin = portCfg->getPinByType( PORTPINTYPE_RX_PIN );
        if (rxPin >= 0) {
          if (!usePin( rxPin, 0, PIN_IR_SENDER )) {
            return false;
          }
        }
        debugVal("  rx=", rxPin );
        int txPin = portCfg->getPinByType( PORTPINTYPE_TX_PIN );
        if (!usePin( txPin, 0, PIN_IR_SENDER )) {
          return false;
        }
        debugVal("  tx=", txPin );
        int baudRate = portCfg->getParamIntVal( piRailFactory->ID_BAUD_RATE );
        debugVal("  baud=", baudRate );
        int index = portCfg->getParamIntVal( piRailFactory->ID_INDEX );
        if (index == 1) {
          HwSerial1.begin( baudRate, SERIAL_8N1, rxPin, txPin );
          debugStr( "  port 1 ok" );
        }
        else if (index == 2) {
          if (!hwSerial2Used) {
            HwSerial2.begin( baudRate, SERIAL_8N1, rxPin, txPin );
            debugStr( "  port 2 ok" );
            hwSerial2Used = true;
          }
          else {
            debugStr( " ERROR: HWSerial2 already used" );
            return false;
          }
        }
        else {
          debugVal( "Invalid msg index (1..2): ", index );
          return false;
        }
        int pwmChannel = nextPwmChannel++;
        ledcSetup( pwmChannel, 38000, 8 ); // 38 kHz PWM, 8-bit resolution
        ledcAttachPin( pwmPin, pwmChannel ); // assign a led pins to a channel
        ledcWrite( pwmChannel, 100 );
        portCfg->attached = portMode;
        return true;
      }
      else if ((portMode == PORTMODE_NFC_READER) || (portMode == PORTMODE_IDREADER)) {
        if (hwSerial2Used) {
          debugStr( " ERROR: HWSerial2 already used" );
          return false;
        }
        int txPin = portCfg->getPinByType( PORTPINTYPE_TX_PIN );
        int rxPin = portCfg->getPinByType( PORTPINTYPE_RX_PIN );
        debugVal( "  Setup NFC tx=" , txPin, "rx=", rxPin );
        if (txPin < 0) {
          debugStr( " - ERROR TX_Pin invalid" );
          return false;
        }
        if (rxPin < 0) {
          debugStr( " - ERROR RX_Pin invalid" );
          return false;
        }

        pn532hsu = new PN532_HSU( HwSerial2, txPin, rxPin );
        hwSerial2Used = true;
        nfc = new PN532( *pn532hsu );
        nfc->begin();
        uint32_t versiondata = nfc->getFirmwareVersion();
        if (!versiondata) {
          debugStr( "Didn't find PN53x board" );
          nfc = NULL;
        }
        else {
          debugStr( "Found chip PN5" );
          // Set the max number of retry attempts to read from a card
          // This prevents us from waiting forever for a card, which is
          // the default behaviour of the PN532.
          nfc->setPassiveActivationRetries( 1 );
          // configure board to read RFID tags
          nfc->SAMConfig();
        }
        portCfg->attached = portMode;
        return true;
      }
      else if (portMode == PORTMODE_DCCDECODER) {
        portType_t portType = portCfg->getType();
        if (portType == PORTTYPE_MOTORPORT) {
          int dirPin = portCfg->getPinByType( PORTPINTYPE_DIRPIN );
          int pwmPin = portCfg->getPinByType( PORTPINTYPE_PWM_PIN );
          debugVal( "  Setting up DCC pin=", dirPin, "pwmPin=", pwmPin );
          if (dirPin < 0) {
            debugStr( " - ERROR dirPin invalid" );
            return false;
          }
          if (!usePin( dirPin, OUTPUT, PIN_DCC )) {
            return false;
          }
          if (pwmPin < 0) {
            debugStr( " - ERROR pwmPin invalid" );
            return false;
          }
          if (!usePin( pwmPin, OUTPUT, PIN_DCC )) {
            return false;
          }
          digitalWrite( pwmPin, HIGH );

          dps.setup( dirPin, 0xFF, DCC128, ROCO, ON );
          dps.setSpeed128( 3, 0 );
          dpsActive = true;
          debugStr( " - DCC initialized" );
          portCfg->attached = portMode;
          return true;
        }
        else if (portType == PORTTYPE_BRIDGE) {
          int pwmPin1 = -1;
          int pwmPin2 = -1;
          PortPin *portPin = (PortPin *) portCfg->firstChild( piRailFactory->ID_PORTPIN );
          while (portPin != NULL) {
            if (portPin->getType() == PORTPINTYPE_PWM_PIN) {
              if (pwmPin1 == -1) {
                pwmPin1 = portPin->getPin();
              }
              else {
                pwmPin2 = portPin->getPin();
              }
            }
            portPin = (PortPin *) portPin->nextSibling( piRailFactory->ID_PORTPIN );
          }
          debugVal( "  Setting up DCC pin1=", pwmPin1, "pin2=", pwmPin2 );
          if (pwmPin1 < 0) {
            debugStr( " - ERROR pwmPin1 invalid" );
            return false;
          }
          if (pwmPin2 < 0) {
            debugStr( " - ERROR pwmPin2 invalid" );
            return false;
          }

          if (!usePin( pwmPin1, OUTPUT, PIN_DCC )) {
            return false;
          }
          digitalWrite( pwmPin1, LOW );

          if (!usePin( pwmPin2, OUTPUT, PIN_DCC )) {
            return false;
          }
          digitalWrite( pwmPin2, LOW );

          dps.setup( pwmPin1, pwmPin2, DCC128, ROCO, ON );
          dps.setSpeed128( 3, 0 );
          dpsActive = true;
          debugStr( " - DCC initialized" );
          portCfg->attached = portMode;
          return true;
        }
        else {
          debugStr( " ERROR: HWSerial2 already used" );
          return false;
        }
      }
      else {
        debugStr( "Unsupported port type ", piRailFactory->portType_names[portType] );
        return false;
      }
    }
    else {
      return (portCfg->attached == portMode);
    }
  }
  else {
    PortCfg* extPort = xmlPiRail.getCfg()->getPortCfg( extCfg->getPort() );
    if (extPort == NULL) {
      return false;
    }
    else if (extPort->attached == PORTMODE_NONE) {
      portMode_t extPortMode = extCfg->getType();
      if (attachPort( extPort, extPortMode )) {
        portCfg->attached = portMode;
        return true;
      }
      else {
        return false;
      }
    }
    else {
      if (portCfg->attached == PORTMODE_NONE) {
        portCfg->attached = portMode;
        return true;
      }
      else {
        return (portCfg->attached == portMode);
      }
    }
  }
}

//--------------------------
void setup() {
  sketchNameAndCompileDate = strrchr( sketchFileNameAndCompileDate, '/' );
  if (sketchNameAndCompileDate) {
    sketchNameAndCompileDate ++;
  }
  else {
    sketchNameAndCompileDate = sketchFileNameAndCompileDate;
  }

  piRailFactory->max_csv_count = 6;
  piRailFactory->max_ev_count = 4;
  piRailFactory->max_fileDef_count = 10;

  //-- Initialize logging
  Serial.begin( 115200 );
  logSerial = true;
  for (int i = 0; i < UDP_DATA_COUNT; i++) {
    udpData[i] = new Data();
  }
  logUDP = true;
  Serial.println();
  Serial.println( sketchNameAndCompileDate );
  Serial.println();
  printHeap( "Begin ", "setup" );

  //-- Initialize the INFO_LED pin as an output
  usePin( INFO_LED, OUTPUT, PIN_LED ); // TODO könnte false liefern
  digitalWrite(INFO_LED, HIGH);  // Turn the LED off by making the voltage HIGH
  lastErrorMsg[0] = 0;

  //--- From https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/peripherals/gpio.html :
  // Please do not use the interrupt of GPIO36 and GPIO39 when using ADC or Wi-Fi with sleep mode enabled.
  // Please refer to the comments of adc1_get_raw. Please refer to section 3.11 of 
  // ‘ECO_and_Workarounds_for_Bugs_in_ESP32’ for the description of this issue. 
  // As a workaround, call adc_power_acquire() in the app. This will result in higher power consumption
  // (by ~1mA), but will remove the glitches on GPIO36 and GPIO39.
  adc_power_acquire();

  xmlPiRail.setup( WiFi.macAddress().c_str(), DEVICETYPE_SWITCHBOX );

  // Ensure we have a hostname before connectToWifi()
  Cfg* cfg = xmlPiRail.getCfg();
  const char *deviceName;
  if ((cfg != NULL) && (cfg->getId() != NULL) && (strlen(cfg->getId()->getName()) > 0)) {
    deviceName = cfg->getId()->getName();
    setUpHostname( deviceName, false );
  }
  else {
    deviceName = xmlPiRail.getDeviceID();
    if (deviceName == NULL) {
      deviceName = "INIT";
    }
    setUpHostname( deviceName, true );
  }

  if (!initialConfigMode) {
    initialConfigMode = !connectToWifi();
  }

  if (!initialConfigMode && !xmlPiRail.isErrorMode()) {
    // if you get a connection, report back via serial:
    xmlPiRail.begin( udpOut );
  }

  if (initialConfigMode) {
    // discard the hostname and rebuild
    setUpHostname( deviceName, true );
  }

  debugStr( "Hostname=", myHostname );

  if (initialConfigMode) {
    setUpConfigWifi();
  }

  delay(250);

  NetCfg* netCfg = xmlPiRail.getNetCfg();
  piHttpServer.init( sketchNameAndCompileDate, myHostname, netCfg );
  piHttpServer.begin();

  piOta.begin( myHostname );

  // Use 1st timer of 4 (counted from zero).
/* TODO  - Funktioniert nicht (s.o.)
  timer = timerBegin(0, 80, true);

  // Attach onTimer function to our timer.
  timerAttachInterrupt(timer, &onTimer, true);

  // Set alarm to call onTimer function every 100 microseconds.
  timerAlarmWrite(timer, 100 * 1000, true);

  // Start an alarm
  timerAlarmEnable(timer);
*/
  printHeap( "Finished ", "setup" );
}

//------------------------------------------------------------------
// Hardware access functions
//------------------------------------------------------------------

void prepareMotorSensor( PortCfg* portCfg, char dir ) {
  // not supported
}

int readMotorSensor( PortCfg* portCfg, char dir, int currentSpeed ) {
  // not supported
  return 0;
}

void processMsgState( MsgState* newMsgState ) {
  // not supported
}

bool checkIDReader( SensorAction* sensorAction ) {
  if (nfc == NULL) {
    return NULL;;
  }

  // Wait for an ISO14443A type cards (Mifare, etc.).  When one is found
  // 'uid' will be populated with the UID, and uidLength will indicate
  // if the uid is 4 bytes (Mifare Classic) or 7 bytes (Mifare Ultralight)
  bool success = nfc->readPassiveTargetID( PN532_MIFARE_ISO14443A, &uid[0], &uidLength, 100 );

  if (success) {
    idReadMillis = millis();
    for (int i = 0; i < uidLength; i++) {
      String tmp = String(uid[i], HEX);
      if (tmp.length() == 1) {
        idReaderData[2*i] = '0';
        idReaderData[2*i+1] = tmp.charAt(0);
      }
      else {
        idReaderData[2*i] = tmp.charAt(0);
        idReaderData[2*i+1] = tmp.charAt(1);
      }
    }
    idReaderData[2*uidLength] = 0;

    const QName* msgID = piRailFactory->NS_PIRAILFACTORY->getQName( idReaderData, 0, 2*uidLength );
    return xmlPiRail.processTrackMsg( sensorAction, msgID, 0, '@', 0, idReadMillis );
  }
  else {
    return xmlPiRail.processTrackMsg( sensorAction, NULL, 0, '@', 0, idReadMillis );
  }
}

void doMotor( PortCfg* portCfg, char dir, int motorOutput ) {
  // not supported
}

void doOutputPin( OutCfg* outCfg, int pinValue, int duration ) {
  if (outCfg == NULL) {
    return;
  }
  ExtCfg* extCfg = outCfg->getExtCfg();
  if (extCfg == NULL) {
    int pin = outCfg->getPin();
    if (pin < 0) {
      return;
    }
    outMode_t outMode = outCfg->attached;
    switch (outMode) {
      case OUTMODE_SERVO: {
        int index = outCfg->index;
        debugVal( "  Set servo ", index, "pin=", pin );
        if (index <= MAX_SERVOS) {
          Servo* servo = servos[index];
          if (servo != NULL) {
            debugVal( "  pos=", pinValue );
            servo->write( pinValue );
          }
        }
        break;
      }
      case OUTMODE_PWM: {
        int pwmChannel = outCfg->pwmChannel;
        if (pwmChannel < 0) {
          return;
        }
        debugVal( "  Set PWM pin=", pin, "val=", pinValue );
        ledcWrite( pwmChannel, pinValue );
        break;
      }
      case OUTMODE_SWITCH: {
        debugVal( "  Set port=", pin, "val=", pinValue );
        if (pinValue == 0) {
          digitalWrite( pin, LOW );
        }
        else if (pinValue == -1) {
          digitalWrite( pin, HIGH );
        }
        break;
      }
      default: { // PULSE or NONE
        if ((duration <= 0) || (duration > 500)) {
          duration = 500;
        }
        debugVal( "  Pulse pin=",pin,"duration=", duration );
        if (pinValue < 0) {
          digitalWrite( pin, HIGH );
        }
        else {
          debugVal( "doPulse NOT YET IMPLEMENTED for value=", pinValue );
          //analogWrite( swPort, pinValue );
        }
        delay( duration );
        digitalWrite( pin, LOW );
      }
    }
  }
  else if (extCfg->getType() == PORTMODE_DCCDECODER) {
    int fn = outCfg->getPin();
    if (outCfg->attached == OUTMODE_SWITCH) {
      if (pinValue != 0) {
        pinValue = 1;
      }
    }
    else {
      pinValue = 2; // toggle
    }
    uint16_t locoAddr = extCfg->getBusAddr();
    debugVal( "Set DCC addr=", locoAddr );
    debugVal( "  Fn=", fn, "val=", pinValue );
    dps.setLocoFunc( locoAddr, pinValue, fn );
    dps.update();
  }

/*  else if (swPort < 0x1000) { // 8bit chip via I2C
    int chipNum = ((swPort & 0x0F00) >> 8) - 1;
    Serial.print("Switch chip num=");
    Serial.print( chipNum );
    int portNum = (swPort & 0x0F);
    Serial.print(",port=");
    Serial.println( portNum );
    if (chipNum < I2C_CHIP_COUNT) {
      Wire.beginTransmission( i2c_chip_addr[chipNum] );
      Wire.write( 0x01 << portNum );
      Wire.endTransmission();
      delay( switch_time );
      Wire.beginTransmission( i2c_chip_addr[chipNum] );
      Wire.write( 0x00 );
      Wire.endTransmission();
    }
  }
  else { // 16Bit MC23017 via I2C
    int chipNum = ((swPort & 0x0F00) >> 8) - 1;
    Serial.print("Switch mc23017 num=");
    Serial.print( chipNum );
    int portNum = (swPort & 0x0F);
    Serial.print(",port=");
    Serial.println( portNum );
    if (chipNum < I2C_MC23017_COUNT) {
      int portReg;
      if (portNum < 7) {
        portReg = MC23017_GPIOA;
      }
      else {
        portReg = MC23017_GPIOB;
        portNum = portNum - 8;
      }
      Wire.beginTransmission( i2c_mc23017_addr[chipNum] );
      Wire.write( portReg );
      Wire.write( 0x01 << portNum );
      Wire.endTransmission();
      delay( switch_time );
      Wire.beginTransmission( i2c_mc23017_addr[chipNum] );
      Wire.write( portReg );
      Wire.write( 0 );
      Wire.endTransmission();
    }
  }*/
}

void doSendMsg( PortCfg* portCfg, const char* msg ) {
  int index = portCfg->getParamIntVal( piRailFactory->ID_INDEX );
  if (index == 1) {
    HwSerial1.println( msg );
  }
  else if (index == 2) {
    HwSerial2.println( msg );
  }
}

//------------------------------------------------------------------
// Command Execution (doXXX, processXXX)
//------------------------------------------------------------------

void sendUdpMessage( const char *msg ) {
  if (!udpConnected) {
    return;
  }
  //IPAddress sendIP;
  //WiFi.hostByName(udpSendIP, sendIP);
  //Udp.beginPacketMulticast( WiFi.localIP(), sendIP, piRailSendPort ); // funzt nicht
  //Udp.beginPacketMulticast( sendIP, piRailSendPort, WiFi.localIP() ); // funzt nicht
  //Udp.beginPacket( "192.168.43.50", piRailSendPort ); // funzt
  //Udp.beginPacket( "255.255.255.255", piRailSendPort ); // funzt nicht
  //Udp.beginPacketMulticast(addr, port, WiFi.localIP())
  IPAddress sendIP = getMyIP();
  sendIP[3] = 255;
  Udp.beginPacket( sendIP, piRailSendPort ); // funzt
  Udp.print( msg );
  Udp.endPacket();
  udpOut[0] = 0; // clear udpOut buffer
}

//==========================
// Main Loop
//==========================
void loop() {
  if ( resetMillisStart != 0L && (unsigned long)( millis() - resetMillisStart ) >= resetAfterMillis ) {
    debugVal( "Performing reset at ", millis() );
    ESP.restart();
  }

  ArduinoOTA.handle();

  if (initialConfigMode && hasWifiCfg) {
    if (!httpServerWasAccessed && millis() >= 3 * 60 * 1000) {
      // 3 minutes = 1 minute trying to connect to existing WiFi plus 2 minutes configuration window
      debugStr( "Config timeout. Restarting…" );
      ESP.restart();
    }
    return; // return even if it's not yet time to restart
  }

  checkWiFi();
  if ((wifi_stations > 0) || (WiFi.status() == WL_CONNECTED)) {
    if (!udpConnected) {
      Udp.begin( piRailReceivePort );
      udpConnected = true;
      debugVal( "Start listen UDP ", piRailReceivePort );
    }
  }

  if (xmlPiRail.isErrorMode()) {
    unsigned long currentTime = millis();
    long nextSyncTime = xmlPiRail.getNextSyncTime();
    if (udpConnected && (nextSyncTime >= 0) && (currentTime >= nextSyncTime)) {
      IPAddress sendIP = getMyIP();
      sendIP[3] = 255;
      Udp.beginPacket( sendIP, piRailSendPort ); // funzt
      const char* deviceID = xmlPiRail.getDeviceID();
      if ((deviceID != NULL) && (strlen(deviceID) > 0)) {
        Udp.print( deviceID );
      }
      else {
        Udp.print( myHostname );
      }
      Udp.print( ":\n" );
      Udp.print( sketchNameAndCompileDate );
      Udp.print( "\nERROR - " );
      Udp.print( lastErrorMsg );
      Udp.endPacket();
      udpOut[0] = 0; // clear udpOut buffer
      xmlPiRail.setNextSyncTime( currentTime + 5000 );
    }
  }
  else {
    bool sendState = false;
    if (udpConnected) {
      int packetSize = Udp.parsePacket();
      if (packetSize > 0) {
        IPAddress remoteIp = Udp.remoteIP();
        sendState = xmlPiRail.processXml( getMyIP(), remoteIp, &Udp );
      }
    }
    if (dpsActive) {
      dps.update();
    }

    //---- Check if a input event has occurred
    Cfg* cfg = xmlPiRail.getCfg();
    IoCfg* ioCfg = xmlPiRail.getIoCfg();
    unsigned long evTime = millis();
    portENTER_CRITICAL( &mux );
    if (inputEventOccurred) {
      inputEventOccurred = false;
      SensorAction* sensorAction = (SensorAction*) cfg->firstChild( piRailFactory->ID_SENSOR );
      while (sensorAction != NULL) {
        InCfg* inCfg = sensorAction->getInCfg( cfg );
        if ((inCfg != NULL) && (inCfg->eventOccurred)) {
          sensorAction->eventOccurred = true;
          inCfg->eventOccurred = false;
        }
        sensorAction = (SensorAction*) sensorAction->nextSibling( piRailFactory->ID_SENSOR );
      }
    }
    portEXIT_CRITICAL( &mux );

    //--- Process input events and polling
    SensorAction* sensorAction = (SensorAction*) cfg->firstChild( piRailFactory->ID_SENSOR );
    while (sensorAction != NULL) {
      if (sensorAction->eventOccurred
          || ((sensorAction->nextPoll > 0) && (sensorAction->nextPoll < evTime))) {
        InCfg* inCfg = sensorAction->getInCfg( cfg );
        if (inCfg != NULL) {
          int pin = inCfg->getPin();
          if (pin > 0) {
            int value = digitalRead( pin );
            char charVal;
            if (value == HIGH) {
              charVal = '1';
            }
            else {
              charVal = '0';
            }
            if (sensorAction->processEvent( NULL, charVal, evTime, cfg, ioCfg )) {
              sendState = true;
            }
          }
        }
      }
      sensorAction = (SensorAction*) sensorAction->nextSibling( piRailFactory->ID_SENSOR );
    }

    //--- Check timers and execute if time out
    TimerAction* timerAction = (TimerAction*) cfg->firstChild( piRailFactory->ID_TIMER );
    while (timerAction != NULL) {
      if (timerAction->doLoop( cfg, ioCfg )) {
        sendState = true;
      }
      timerAction = (TimerAction*) timerAction->nextSibling( piRailFactory->ID_TIMER );
    }

    //--- Send sync message
    unsigned long currentTime = millis();
    unsigned long nextSyncTime = xmlPiRail.getNextSyncTime();
    if ((nextSyncTime >= 0) && (currentTime >= nextSyncTime)) {
      sendState = true;
      xmlPiRail.setNextSyncTime( -1 );
    }
    if (sendState) {
      //--- Save current BSSID to apMAC
      String bssidStr = WiFi.BSSIDstr();
      if (bssidStr == NULL) {
        apMAC[0] = 0;
      }
      else {
        for (int i = 0; i < bssidStr.length(); i++) {
          apMAC[i] = bssidStr.charAt(i);
          if (apMAC[i] == ':') {
            apMAC[i] = '-';
          }
        }
        apMAC[bssidStr.length()] = 0;
      }
      // print events and speed changes into udpOut
      xmlPiRail.sendDeviceState( currentTime, WiFi.RSSI(), apMAC );
    }

    //--- Check triggers
    xmlPiRail.processState( getMyIP(), getMyIP(), xmlPiRail.getDeviceState() );

    //--- Send motor tuning data if buffer is full
    if (udpData[currentDataIndex]->isFull() || ((udpData[currentDataIndex]->csvCount() > 0) && ((currentTime - lastDataMsg) > 3000))) {
      for (int i = 0; i <= currentDataIndex; i++) {
        sendUdpMessage( xmlPiRail.writeData( udpData[i] )); // prints csv data to udpOut
        udpData[i]->clear();
      }
      currentDataIndex = 0;
    }
  }
}
