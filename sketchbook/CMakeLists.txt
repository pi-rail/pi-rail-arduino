cmake_minimum_required(VERSION 3.14)
project(sketchbook)

set(CMAKE_CXX_STANDARD 14)

# esp32 includes
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/cores/esp32)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/cores/esp32/apps/sntp)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/cores/esp32/libb64)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/ArduinoOTA/src)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/AsyncUDP/src)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/AzureIoT/src)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/AzureIoT/src/az_iot)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/AzureIoT/src/az_iot/c-utility/inc/azure_c_shared_utility)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/AzureIoT/src/az_iot/c-utility/pal/generic)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/AzureIoT/src/az_iot/c-utility/pal/inc)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/AzureIoT/src/az_iot/c-utility/pal/lwip)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/AzureIoT/src/az_iot/iothub_client/inc)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/AzureIoT/src/az_iot/umqtt/inc/azure_umqtt_c)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/BLE/src)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/BluetoothSerial/src)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/DNSServer/src)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/EEPROM/src)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/ESP32/src)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/ESPmDNS/src)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/FFat/src)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/FS/src)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/HTTPClient/src)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/HTTPUpdateServer/src)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/HTTPUpdate/src)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/NetBIOS/src)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/Preferences/src)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/SD_MMC/src)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/SD/src)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/SimpleBLE/src)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/SPIFFS/src)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/SPI/src)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/Ticker/src)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/Update/src)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/WebServer/src)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/WebServer/src/detail)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/WebServer/src/uri)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/WiFiClientSecure/src)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/WiFiProv/src)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/WiFi/src)
include_directories(/home/pru/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/Wire/src)

# esp8266 includes
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/cores/esp8266)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/cores/esp8266/avr)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/cores/esp8266/libb64)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/cores/esp8266/spiffs)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/cores/esp8266/umm_malloc)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/cores/esp8266/umm_malloc/dbglog)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ArduinoOTA)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/DNSServer/src)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/EEPROM)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266AVRISP/src)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266HTTPClient/src)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266HTTPUpdateServer/src)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266httpUpdate/src)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266LLMNR)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266mDNS/src)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266NetBIOS)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266SdFat/examples.dontuse/AvrAdcLogger)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266SdFat/examples.dontuse/examplesV1/AnalogBinLogger)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266SdFat/examples.dontuse/examplesV1/LowLatencyLogger)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266SdFat/examples.dontuse/examplesV1/LowLatencyLoggerADXL345)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266SdFat/examples.dontuse/examplesV1/LowLatencyLoggerMPU6050)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266SdFat/examples.dontuse/ExFatLogger)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266SdFat/src)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266SdFat/src/common)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266SdFat/src/DigitalIO)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266SdFat/src/DigitalIO/boards)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266SdFat/src/ExFatLib)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266SdFat/src/FatLib)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266SdFat/src/FsLib)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266SdFat/src/iostream)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266SdFat/src/SdCard)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266SdFat/src/SpiDriver)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/esp8266/src)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266SSDP)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266WebServer/src)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266WebServer/src/detail)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266WebServer/src/uri)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266WiFiMesh/src)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266WiFi/src)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/ESP8266WiFi/src/include)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/Ethernet/src)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/Ethernet/src/utility)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/GDBStub/src)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/GDBStub/src/internal)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/GDBStub/src/xtensa)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/GDBStub/src/xtensa/config)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/Hash/src)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/I2S/src)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/LittleFS/lib/littlefs)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/LittleFS/lib/littlefs/bd)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/LittleFS/src)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/lwIP_enc28j60/src)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/lwIP_enc28j60/src/utility)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/lwIP_PPP/src)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/lwIP_w5100/src)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/lwIP_w5100/src/utility)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/lwIP_w5500/src)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/lwIP_w5500/src/utility)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/Netdump/src)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/SDFS/src)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/SD/src)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/Servo/src)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/SoftwareSerial/src)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/SoftwareSerial/src/circular_queue)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/SPI)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/SPISlave/src)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/TFT_Touch_Shield_V2)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/Ticker/src)
include_directories(/home/pru/.arduino15/packages/esp8266/hardware/esp8266/3.0.2/libraries/Wire)

# sketchbook includes
include_directories(libraries/ArduinoOTA/src)
include_directories(libraries/ArduinoOTA/src/utility)
include_directories(libraries/AsyncTCP/src)
include_directories(libraries/DCCInterfaceMaster)
include_directories(libraries/ESPAsyncTCP/src)
include_directories(libraries/ESPAsyncWebServer/src)
include_directories(libraries/LittleFS_esp32/src)
include_directories(libraries/NDEF)
include_directories(libraries/PiHttp/src)
include_directories(libraries/PiMobile/src)
include_directories(libraries/PiRail/src)
include_directories(libraries/PN532)
include_directories(libraries/PN532_HSU)
include_directories(libraries/PString)
include_directories(libraries/ServoESP32/src)
include_directories(libraries/Servo/src)
include_directories(libraries/Servo/src/avr)
include_directories(libraries/Servo/src/mbed)
include_directories(libraries/Servo/src/megaavr)
include_directories(libraries/Servo/src/nrf52)
include_directories(libraries/Servo/src/sam)
include_directories(libraries/Servo/src/samd)
include_directories(libraries/Servo/src/stm32f4)

# sketchbook executables
add_executable(sketchbook
        cmake-build-debug/.cmake
        cmake-build-debug/CMakeFiles/3.16.5/CMakeCCompiler.cmake
        cmake-build-debug/CMakeFiles/3.16.5/CMakeCXXCompiler.cmake
        cmake-build-debug/CMakeFiles/3.16.5/CMakeSystem.cmake
        cmake-build-debug/CMakeFiles/3.16.5/CompilerIdC/CMakeCCompilerId.c
        cmake-build-debug/CMakeFiles/3.16.5/CompilerIdCXX/CMakeCXXCompilerId.cpp
        cmake-build-debug/CMakeFiles/3.17.3/CMakeCCompiler.cmake
        cmake-build-debug/CMakeFiles/3.17.3/CMakeCXXCompiler.cmake
        cmake-build-debug/CMakeFiles/3.17.3/CMakeSystem.cmake
        cmake-build-debug/CMakeFiles/3.17.3/CompilerIdC/CMakeCCompilerId.c
        cmake-build-debug/CMakeFiles/3.17.3/CompilerIdCXX/CMakeCXXCompilerId.cpp
        cmake-build-debug/CMakeFiles/3.17.5/CMakeCCompiler.cmake
        cmake-build-debug/CMakeFiles/3.17.5/CMakeCXXCompiler.cmake
        cmake-build-debug/CMakeFiles/3.17.5/CMakeSystem.cmake
        cmake-build-debug/CMakeFiles/3.17.5/CompilerIdC/CMakeCCompilerId.c
        cmake-build-debug/CMakeFiles/3.17.5/CompilerIdCXX/CMakeCXXCompilerId.cpp
        cmake-build-debug/CMakeFiles/3.19.2/CMakeCCompiler.cmake
        cmake-build-debug/CMakeFiles/3.19.2/CMakeCXXCompiler.cmake
        cmake-build-debug/CMakeFiles/3.19.2/CMakeSystem.cmake
        cmake-build-debug/CMakeFiles/3.19.2/CompilerIdC/CMakeCCompilerId.c
        cmake-build-debug/CMakeFiles/3.19.2/CompilerIdCXX/CMakeCXXCompilerId.cpp
        cmake-build-debug/CMakeFiles/3.20.2/CMakeCCompiler.cmake
        cmake-build-debug/CMakeFiles/3.20.2/CMakeCXXCompiler.cmake
        cmake-build-debug/CMakeFiles/3.20.2/CMakeSystem.cmake
        cmake-build-debug/CMakeFiles/3.20.2/CompilerIdC/CMakeCCompilerId.c
        cmake-build-debug/CMakeFiles/3.20.2/CompilerIdCXX/CMakeCXXCompilerId.cpp
        cmake-build-debug/CMakeFiles/3.21.1/CMakeCCompiler.cmake
        cmake-build-debug/CMakeFiles/3.21.1/CMakeCXXCompiler.cmake
        cmake-build-debug/CMakeFiles/3.21.1/CMakeSystem.cmake
        cmake-build-debug/CMakeFiles/3.21.1/CompilerIdC/CMakeCCompilerId.c
        cmake-build-debug/CMakeFiles/3.21.1/CompilerIdCXX/CMakeCXXCompilerId.cpp
        cmake-build-debug/CMakeFiles/cmake.check_cache
        cmake-build-debug/CMakeFiles/CMakeDirectoryInformation.cmake
        cmake-build-debug/CMakeFiles/Makefile.cmake
        cmake-build-debug/CMakeFiles/sketchbook.dir/cmake_clean.cmake
        cmake-build-debug/CMakeFiles/sketchbook.dir/CMakeFiles/3.16.5/CompilerIdC/CMakeCCompilerId.c.o
        cmake-build-debug/CMakeFiles/sketchbook.dir/CMakeFiles/3.16.5/CompilerIdCXX/CMakeCXXCompilerId.cpp.o
        cmake-build-debug/CMakeFiles/sketchbook.dir/CMakeFiles/3.17.3/CompilerIdC/CMakeCCompilerId.c.o
        cmake-build-debug/CMakeFiles/sketchbook.dir/CMakeFiles/3.17.3/CompilerIdCXX/CMakeCXXCompilerId.cpp.o
        cmake-build-debug/CMakeFiles/sketchbook.dir/CMakeFiles/3.17.5/CompilerIdC/CMakeCCompilerId.c.o
        cmake-build-debug/CMakeFiles/sketchbook.dir/CMakeFiles/3.17.5/CompilerIdCXX/CMakeCXXCompilerId.cpp.o
        cmake-build-debug/CMakeFiles/sketchbook.dir/CMakeFiles/3.19.2/CompilerIdC/CMakeCCompilerId.c.o
        cmake-build-debug/CMakeFiles/sketchbook.dir/CMakeFiles/3.19.2/CompilerIdCXX/CMakeCXXCompilerId.cpp.o
        cmake-build-debug/CMakeFiles/sketchbook.dir/CMakeFiles/3.20.2/CompilerIdC/CMakeCCompilerId.c.o
        cmake-build-debug/CMakeFiles/sketchbook.dir/CMakeFiles/3.20.2/CompilerIdCXX/CMakeCXXCompilerId.cpp.o
        cmake-build-debug/CMakeFiles/sketchbook.dir/CMakeFiles/3.21.1/CompilerIdC/CMakeCCompilerId.c.o
        cmake-build-debug/CMakeFiles/sketchbook.dir/CMakeFiles/3.21.1/CompilerIdCXX/CMakeCXXCompilerId.cpp.o
        cmake-build-debug/CMakeFiles/sketchbook.dir/DependInfo.cmake
        cmake-build-debug/CMakeFiles/sketchbook.dir/libraries/ArduinoOTA/src/InternalStorage.cpp.o
        cmake-build-debug/CMakeFiles/sketchbook.dir/libraries/ArduinoOTA/src/InternalStorageESP.cpp.o
        cmake-build-debug/cmake_install.cmake
        cmake-build-debug/PI-Rail-Arduino.cbp
        cmake-build-debug/sketchbook.cbp
        i2c-test/i2c-test.ino
        IO_Board_ESP32/IO_Board_ESP32.ino
        libraries/ArduinoOTA/examples/Advanced/OTASketchDownload/OTASketchDownload.ino
        libraries/ArduinoOTA/examples/Advanced/OTASketchDownload_SD/OTASketchDownload_SD.ino
        libraries/ArduinoOTA/examples/Advanced/OTASketchDownloadWifi/OTASketchDownloadWifi.ino
        libraries/ArduinoOTA/examples/ATmega_SD/ATmega_SD.ino
        libraries/ArduinoOTA/examples/OTEthernet/OTEthernet.ino
        libraries/ArduinoOTA/examples/OTEthernet_SD/OTEthernet_SD.ino
        libraries/ArduinoOTA/examples/SerialWiFiOTA/SerialWiFiOTA.ino
        libraries/ArduinoOTA/examples/WiFi101_OTA/WiFi101_OTA.ino
        libraries/ArduinoOTA/examples/WiFi101_SD_OTA/WiFi101_SD_OTA.ino
        libraries/ArduinoOTA/src/InternalStorageAVR.cpp
        libraries/ArduinoOTA/src/InternalStorage.cpp
        libraries/ArduinoOTA/src/InternalStorageESP.cpp
        libraries/ArduinoOTA/src/OTAStorage.cpp
        libraries/ArduinoOTA/src/WiFiOTA.cpp
        libraries/AsyncTCP/src/AsyncTCP.cpp
        libraries/DCCInterfaceMaster/DCCHardware.cpp
        libraries/DCCInterfaceMaster/DCCPacket.cpp
        libraries/DCCInterfaceMaster/DCCPacketQueue.cpp
        libraries/DCCInterfaceMaster/DCCPacketScheduler.cpp
        libraries/DCCInterfaceMaster/examples/CmdrArduino_accessory/CmdrArduino_accessory.ino
        libraries/DCCInterfaceMaster/examples/CmdrArduino_accessory_Up-Down/CmdrArduino_accessory_Up-Down.ino
        libraries/DCCInterfaceMaster/examples/DCC_CmdrArduino_minimum/DCC_CmdrArduino_minimum.ino
        libraries/DCCInterfaceMaster/examples/DCC_CmdrArduino_minimum_POWER/DCC_CmdrArduino_minimum_POWER.ino
        libraries/DCCInterfaceMaster/examples/p50xDCC_Zentrale/p50xDCC_Zentrale.ino
        libraries/DCCInterfaceMaster/examples/SimpleDCC_without_Lib/SimpleDCC_without_Lib.ino
        libraries/DCCInterfaceMaster/examples/Z21_Ethernet_DCC_Zentrale_v483/Z21_Ethernet_DCC_Zentrale_v483.ino
        libraries/ESPAsyncTCP/examples/ClientServer/Client/Client.ino
        libraries/ESPAsyncTCP/examples/ClientServer/Server/Server.ino
        libraries/ESPAsyncTCP/examples/SyncClient/SyncClient.ino
        libraries/ESPAsyncTCP/src/AsyncPrinter.cpp
        libraries/ESPAsyncTCP/src/ESPAsyncTCPbuffer.cpp
        libraries/ESPAsyncTCP/src/ESPAsyncTCP.cpp
        libraries/ESPAsyncTCP/src/SyncClient.cpp
        libraries/ESPAsyncTCP/src/tcp_axtls.c
        libraries/ESPAsyncTCP/ssl/server.cer
        libraries/ESPAsyncWebServer/examples/CaptivePortal/CaptivePortal.ino
        libraries/ESPAsyncWebServer/examples/ESP_AsyncFSBrowser/ESP_AsyncFSBrowser.ino
        libraries/ESPAsyncWebServer/examples/simple_server/simple_server.ino
        libraries/ESPAsyncWebServer/src/AsyncEventSource.cpp
        libraries/ESPAsyncWebServer/src/AsyncWebSocket.cpp
        libraries/ESPAsyncWebServer/src/SPIFFSEditor.cpp
        libraries/ESPAsyncWebServer/src/WebAuthentication.cpp
        libraries/ESPAsyncWebServer/src/WebHandlers.cpp
        libraries/ESPAsyncWebServer/src/WebRequest.cpp
        libraries/ESPAsyncWebServer/src/WebResponses.cpp
        libraries/ESPAsyncWebServer/src/WebServer.cpp
        libraries/LittleFS_esp32/examples/LITTLEFS_PlatformIO/partitions_custom.csv
        libraries/LittleFS_esp32/examples/LITTLEFS_PlatformIO/src/main.cpp
        libraries/LittleFS_esp32/examples/LittleFS_test/LittleFS_test.ino
        libraries/LittleFS_esp32/examples/LITTLEFS_test/partitions.csv
        libraries/LittleFS_esp32/examples/LITTLEFS_time/LITTLEFS_time.ino
        libraries/LittleFS_esp32/src/esp_littlefs.c
        libraries/LittleFS_esp32/src/lfs.c
        libraries/LittleFS_esp32/src/lfs_util.c
        libraries/LittleFS_esp32/src/littlefs_api.c
        libraries/LittleFS_esp32/src/LITTLEFS.cpp
        libraries/NDEF/examples/CleanTag/CleanTag.ino
        libraries/NDEF/examples/EraseTag/EraseTag.ino
        libraries/NDEF/examples/FormatTag/FormatTag.ino
        libraries/NDEF/examples/P2P_Receive_LCD/P2P_Receive_LCD.ino
        libraries/NDEF/examples/P2P_Receive/P2P_Receive.ino
        libraries/NDEF/examples/P2P_Send/P2P_Send.ino
        libraries/NDEF/examples/ReadTagExtended/ReadTagExtended.ino
        libraries/NDEF/examples/ReadTag/ReadTag.ino
        libraries/NDEF/examples/WriteTagMultipleRecords/WriteTagMultipleRecords.ino
        libraries/NDEF/examples/WriteTag/WriteTag.ino
        libraries/NDEF/MifareClassic.cpp
        libraries/NDEF/MifareUltralight.cpp
        libraries/NDEF/Ndef.cpp
        libraries/NDEF/NdefMessage.cpp
        libraries/NDEF/NdefRecord.cpp
        libraries/NDEF/NfcAdapter.cpp
        libraries/NDEF/NfcTag.cpp
        libraries/NDEF/tests/NdefMemoryTest/NdefMemoryTest.ino
        libraries/NDEF/tests/NdefMessageTest/NdefMessageTest.ino
        libraries/NDEF/tests/NdefUnitTest/NdefUnitTest.ino
        libraries/NDEF/tests/NfcTagTest/NfcTagTest.ino
        libraries/PiHttp/src/PiHttpServer.cpp
        libraries/PiHttp/src/PiOTA.cpp
        libraries/PiMobile/src/Model.cpp
        libraries/PiMobile/src/Namespace.cpp
        libraries/PiMobile/src/openwall_md5.cpp
        libraries/PiMobile/src/QName.cpp
        libraries/PiMobile/src/StringHash.cpp
        libraries/PiMobile/src/XmlReader.cpp
        libraries/PiMobile/src/XmlWriter.cpp
        libraries/PiRail/src/Action.cpp
        libraries/PiRail/src/ActionState.cpp
        libraries/PiRail/src/CallCmd.cpp
        libraries/PiRail/src/Cfg.cpp
        libraries/PiRail/src/Cmd.cpp
        libraries/PiRail/src/Command.cpp
        libraries/PiRail/src/Csv.cpp
        libraries/PiRail/src/Data.cpp
        libraries/PiRail/src/DelayCmd.cpp
        libraries/PiRail/src/EnumAction.cpp
        libraries/PiRail/src/ExtCfg.cpp
        libraries/PiRail/src/FileCfg.cpp
        libraries/PiRail/src/FileDef.cpp
        libraries/PiRail/src/IfCmd.cpp
        libraries/PiRail/src/InCfg.cpp
        libraries/PiRail/src/InPin.cpp
        libraries/PiRail/src/IoCfg.cpp
        libraries/PiRail/src/IoConn.cpp
        libraries/PiRail/src/ItemConn.cpp
        libraries/PiRail/src/Ln.cpp
        libraries/PiRail/src/LockCmd.cpp
        libraries/PiRail/src/MoCmd.cpp
        libraries/PiRail/src/MotorAction.cpp
        libraries/PiRail/src/MotorMode.cpp
        libraries/PiRail/src/MotorState.cpp
        libraries/PiRail/src/MsgState.cpp
        libraries/PiRail/src/NetCfg.cpp
        libraries/PiRail/src/NetCfgOld.cpp
        libraries/PiRail/src/OutCfg.cpp
        libraries/PiRail/src/OutPin.cpp
        libraries/PiRail/src/Param.cpp
        libraries/PiRail/src/PiRailFactory.cpp
        libraries/PiRail/src/Point.cpp
        libraries/PiRail/src/PortCfg.cpp
        libraries/PiRail/src/Port.cpp
        libraries/PiRail/src/PortPin.cpp
        libraries/PiRail/src/RangeAction.cpp
        libraries/PiRail/src/SecurityCfg.cpp
        libraries/PiRail/src/SensorAction.cpp
        libraries/PiRail/src/SetCmd.cpp
        libraries/PiRail/src/SetIO.cpp
        libraries/PiRail/src/State.cpp
        libraries/PiRail/src/StateScript.cpp
        libraries/PiRail/src/SyncCmd.cpp
        libraries/PiRail/src/TimerAction.cpp
        libraries/PiRail/src/TrackMsg.cpp
        libraries/PiRail/src/TrackPos.cpp
        libraries/PiRail/src/TriggerAction.cpp
        libraries/PiRail/src/WifiCfg.cpp
        libraries/PiRail/src/WifiCfgOld.cpp
        libraries/PiRail/src/XmlPiRail.cpp
        libraries/PN532/examples/p2p_raw/p2p_raw.ino
        libraries/PN532/examples/p2p_with_ndef_library/p2p_with_ndef_library.ino
        libraries/PN532_HSU/PN532_HSU.cpp
        libraries/PN532/llcp.cpp
        libraries/PN532/mac_link.cpp
        libraries/PN532/PN532.cpp
        libraries/PN532/snep.cpp
        libraries/PString/PString.cpp
        libraries/ServoESP32/examples/01-SimpleServo/01-SimpleServo.ino
        libraries/ServoESP32/examples/02-ServoPotentiometer/02-ServoPotentiometer.ino
        libraries/ServoESP32/examples/03-MultipleServos/03-MultipleServos.ino
        libraries/ServoESP32/examples/04-SimpleServoAngles/04-SimpleServoAngles.ino
        libraries/ServoESP32/src/Servo.cpp
        libraries/Servo/examples/Knob/Knob.ino
        libraries/Servo/examples/Sweep/Sweep.ino
        libraries/Servo/src/avr/Servo.cpp
        libraries/Servo/src/mbed/Servo.cpp
        libraries/Servo/src/megaavr/Servo.cpp
        libraries/Servo/src/nrf52/Servo.cpp
        libraries/Servo/src/samd/Servo.cpp
        libraries/Servo/src/sam/Servo.cpp
        libraries/Servo/src/stm32f4/Servo.cpp
        Loko_21mtc_ESP32/Loko_21mtc_ESP32.ino
        Loko_G_ESP32/Loko_G_ESP32.ino
        Loko_H0_DCC_ESP8285/Loko_H0_DCC_ESP8285.ino
        Loko_H0_Ma_ESP32/Loko_H0_Ma_ESP32.ino
        Loko_H0_Ma_ESP8285/Loko_H0_Ma_ESP8285.ino
        Loko_plux22_ESP32/Loko_plux22_ESP32.ino
        test_DCC/test_DCC.ino
        test_DCC_without_Lib/test_DCC_without_Lib.ino
        test-io/test-io.ino
        TestMotorSensor/TestMotorSensor.ino
        test/test.ino
        Tower_H0_DCC_ESP8285/Tower_H0_DCC_ESP8285.ino
        Weiche_ESP32/Weiche_ESP32.ino
        Weiche_G_ESP32/Weiche_G_ESP32.ino
        IO_Board_ESP32/IO_Board_ESP32.ino
)
