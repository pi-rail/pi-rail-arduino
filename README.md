# PI-Rail Arduino Library and Firmware

Basic PI-Rail library for Arduino and Firmware for [CTC](https://www.ctc-system.ch) Modules like this one:

![Weichenplatine](images/Weiche_ESP32.png) 

The Library provides communication protocol and XML based config files. It is currently tested with ESP32 and ESP8285.

**CTC customers please contact [CTC](https://www.ctc-system.ch) for Software-Updates.**

## Licence

This project containt its own source and copies of source code from other libraries. These libraries are unchanged and provided only to make it easier for getting started.
- [ArduinoOTA](https://github.com/jandrassy/ArduinoOTA) - LGPL
- [AsyncTCP](https://github.com/me-no-dev/AsyncTCP) - LGPL v3
- [PN532 / PN532_HSU](https://github.com/elechouse/PN532) - BSD License
- [DCCInterfaceMaster](http://pgahtow.de/wiki/index.php?title=DCC) - GNU General Public License with Classpath exception
- [ESPAsyncTCP](https://github.com/me-no-dev/ESPAsyncTCP) - LGPL v3
- [ESPAsyncWebServer](https://github.com/me-no-dev/ESPAsyncWebServer) - LGPL v2.1
- PString - LGPL v2.1
- [ServoESP32](https://travis-ci.com/RoboticsBrno/ServoESP32) - MIT License

Inside PI-Rail Library folder (copied code):
- [OpenWall MD5](http://openwall.info/wiki/people/solar/software/public-domain-source-code/md5) - BSD variant

**Thanks a lot to all people contributing to open source in general and especially the listed projects!**

The PI-Rail library and module firmware are provided under **GPL v3**. For the other libraries see README file inside the library top folder.

## Documentation

- [Technical Documentation (english)](https://pi-rail.gitlab.io/pi-rail-doc)
- [User Documentation (german)](https://ctc-system.gitlab.io/ctc-doku)
- See [PI-Rail-FX](https://gitlab.com/pi-rail/pi-rail-fx) for documentation on desktop app.
- See also [PI-Rail.org (german wiki)](https://www.pi-rail.org). 

## Getting started

1. Download or clone PI-Rail-Arduino project from here.
1. Install Arduino boards ESP32 and ESP8266 in Arduino IDE's board management.
1. Change Arduino sketchbook directory (menu "File/Settings") to sketchbook folder of downloaded PI-Rail-Arduino.
1. Open PI-Rail project matching your board from Arduino menu "File/Sketchbook".

For editing and git operations we recommend using **JetBrains CLion**.

**Note**: For using DCC with ESP32 you need the upcoming version 1.05 of esp32 arduino board. 
Until it is released you have to apply this patch:
https://github.com/espressif/arduino-esp32/commit/be77bd4e27228e10cf7132668f0f1fd2172f4a37

## Online Software Update

You can either update via Arduino IDE or PI-Rail App

### Online Update via Arduiono IDE

1. Connect you PC and device to PI-Rail WiFi
1. Select device in Arduino IDE in Menu "Tools/Port" (instead of serial port)
1. Press upload button in Arduion IDE

### Online Update via PI-Rail App

1. Create binaray package in Arduino IDE via "Sketch/Export compiled Binary"
2. Copy binary (.bin) to PI-Rail App folder "firmware" (**do not rename the file!**)

## How to contribute?

There are many ways you can help this project. There is a lot of work to do:
- Documentation.
- Testing and Bug reporting.
- Optimize motor control, speed estimating and position calculation.
- Collect ideas for future boards and software features.
- ...

Interested? - Just write a mail to [Peter Rudolph](mailto:peter.rudolph@pi-data.de)

## Helpful Links

[Geschwindigkeit und Tücken gängiger MCUs](https://arduino-hannover.de/2020/09/07/mcu-performance/)
[Detailed discussion about Cache exception with Timer and SPIFFS](https://github.com/crankyoldgit/IRremoteESP8266/issues/1073)

## Debugging Tips

### ESP32 Stack Trace Analysis

[ESP Exception Decoder](https://github.com/me-no-dev/EspExceptionDecoder)

### Memory Map

[Create Memory map file](https://everythingesp.com/esp32-arduino-creating-a-memory-map-file/)

1. Locate ~/.arduino15/packages/esp32/hardware/esp32/1.0.6/platform.txt
2. Find "## Combine gc-sections, archives, and objects" inside platform.txt
3. In following line insert "-Wl,-Map=arduino.map" after "start-group"
4. Compile project and locate "arduino.map", in my case in my home directory.

Use [Map File Browser](http://www.sikorskiy.net/prj/amap/index.html) for analysis.

### Gereral ESP32

* [Overview of Libraries](https://docs.espressif.com/projects/arduino-esp32/en/latest/libraries.html)

### WiFi

* [ESP32 WiFi API Doc](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-guides/wifi.html)
* [Multi WiFi Example](https://randomnerdtutorials.com/esp32-wifimulti/)
* [Arduino WiFi API](https://espressif-docs.readthedocs-hosted.com/projects/arduino-esp32/en/latest/api/wifi.html)
* [Establish WiFi connection to AP with strongest radio signal](https://www.esp32.com/viewtopic.php?t=18979)
* [ESP32 WROOM-DA (Dual Antenna)](https://www.espressif.com/en/news/ESP32-WROOM-DA)
* [Reconnect ESP32 to WiFi after lost connection](https://randomnerdtutorials.com/solved-reconnect-esp32-to-wifi/)

Sources:
* [ESP32 WiFiScan](https://github.com/espressif/arduino-esp32/blob/master/libraries/WiFi/src/WiFiScan.cpp)
* [ESP32 WiFiSTA](https://github.com/espressif/arduino-esp32/blob/master/libraries/WiFi/src/WiFiSTA.cpp)